package com.dignara.lib.animation;

public class FrameMove {
	protected float mCurFrame;

	public float moveFrame() {
		return mCurFrame;
	}
	
	public void setFrame(float frame) {
		mCurFrame = frame;
	}
}


