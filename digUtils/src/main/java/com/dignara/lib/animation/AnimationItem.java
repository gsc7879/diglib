package com.dignara.lib.animation;

import android.view.View;

public class AnimationItem {
	public int	ORDER;
	public int 	ANIMATION_RESID;
	public int 	VISIBILITY;
	public View ANIMATION_VIEW;
	
	public AnimationItem(View animationView, int order, int animationResId, int visibility) {
		ORDER 			= order;
		ANIMATION_VIEW 	= animationView;
		ANIMATION_RESID = animationResId;
		VISIBILITY 		= visibility;
	}
}
