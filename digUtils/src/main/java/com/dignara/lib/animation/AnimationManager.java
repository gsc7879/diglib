package com.dignara.lib.animation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 애니메이션 매니저
 */
public class AnimationManager {
    private AnimatorListenerAdapter      mAnimatorListenerAdapter;
    private OnAnimationStepStateListener mAnimationStepStateListener;
    private ArrayList<AnimationItem>     mAnimationList;
    private HashMap<Integer, Integer>    mOrderInfo;
    private Animator                     mAnimator;

    /**
     * 애니메이션 매니저 리스너
     */
    public interface OnAnimationStateListener {
        /**
         * 애니메이션 전체 시작
         *
         * @param callBackId 애니메이션 아이디
         */
        public void onStartAnimation(int callBackId);

        /**
         * 애니메이션 전체 종료
         *
         * @param callBackId 애니메이션 아이디
         */
        public void onEndAnimation(int callBackId);
    }

    public interface OnAnimationStepStateListener {
        /**
         * 애니메이션 단위 시작
         *
         * @param callBackId 애니메이션 아이디
         * @param step       애니메이션 단계
         */
        public void onStartAnimation(int callBackId, int step);

        /**
         * 애니메이션 단위 종료
         *
         * @param callBackId 애니메이션 아이디
         * @param step       애니메이션 단계
         */
        public void onEndAnimation(int callBackId, int step);
    }

    /**
     * 생성자
     *
     */
    public AnimationManager() {
        mAnimationList  = new ArrayList<>();
        mOrderInfo      = new HashMap<>();
    }

    /**
     * 애니메이션 리스너 등록
     *
     * @param animatorListenerAdapter 애니메이션 리스너
     */
    public void setAnimationStateListener(AnimatorListenerAdapter animatorListenerAdapter) {
        mAnimatorListenerAdapter = animatorListenerAdapter;
    }

    /**
     * 애니메이션 리스너 등록
     *
     * @param animationStepStateListener 애니메이션 상태 리스너
     */
    public void setAnimationStepStateListener(OnAnimationStepStateListener animationStepStateListener) {
        mAnimationStepStateListener = animationStepStateListener;
    }

    /**
     * 애니메이션 추가
     *
     * @param animationItem
     */
    public void addAnimation(AnimationItem animationItem) {
        if (mAnimator != null) {
            return;
        }
        int beforeOrderCount = 0;
        if (mOrderInfo.containsKey(animationItem.ORDER)) {
            beforeOrderCount = mOrderInfo.get(animationItem.ORDER);
            mOrderInfo.remove(animationItem.ORDER);
        }
        mOrderInfo.put(animationItem.ORDER, beforeOrderCount + 1);
        mAnimationList.add(animationItem);
    }

    /**
     * 추가한 애니메이션 시작
     *
     * @param callBackId 애니메이션 아이디
     */
    public void startAnimation(Context context, int callBackId) {
        startAnimation(context, callBackId, 0, new View[]{});
    }

    /**
     * 추가한 애니메이션 시작
     *
     * @param callBackId    애니메이션 아이디
     * @param lockViewArray 애니메이션 동안 락시킬 뷰
     */
    public void startAnimation(Context context, int callBackId, View[] lockViewArray) {
        if (mAnimator != null) {
            return;
        }
        startAnimation(context, callBackId, 0, lockViewArray);
    }

    /**
     * 애니메이션 캔슬
     */
    public void cancelAnimation() {
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
    }

    private void startAnimation(final Context context, final int callBackId, final int order, final View[] lockViewArray) {
        if (context == null) {
            endAnimation(lockViewArray);
            return;
        }
        int cnt = 0;
        for (View lockView : lockViewArray) {
            lockView.setEnabled(false);
        }
        for (int i = 0; i < mAnimationList.size(); i++) {
            final AnimationItem animationItem = mAnimationList.get(i);
            if (animationItem.ORDER == order) {
                cnt++;
                mAnimator = AnimatorInflater.loadAnimator(context, animationItem.ANIMATION_RESID);
                mAnimator.setTarget(animationItem.ANIMATION_VIEW);
                mAnimator.start();

                animationItem.ANIMATION_VIEW.setVisibility(View.VISIBLE);
                if (cnt == mOrderInfo.get(order)) {
                    mAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (animationItem.ANIMATION_VIEW.getVisibility() != animationItem.VISIBILITY) {
                                animationItem.ANIMATION_VIEW.setVisibility(animationItem.VISIBILITY);
                            }

                            if (mOrderInfo.containsKey(order + 1)) {
                                startAnimation(context, callBackId, order + 1, lockViewArray);
                            } else {
                                mAnimationList.clear();
                                mOrderInfo.clear();
                                if (mAnimatorListenerAdapter != null) {
                                    mAnimatorListenerAdapter.onAnimationEnd(animation);
                                }
                                mAnimator = null;
                                for (View lockView : lockViewArray) {
                                    lockView.setEnabled(true);
                                }
                            }

                            if (mAnimationStepStateListener != null) {
                                mAnimationStepStateListener.onEndAnimation(callBackId, order);
                            }
                        }

                        @Override
                        public void onAnimationStart(Animator animation) {
                            if (mAnimatorListenerAdapter != null && order == 0) {
                                mAnimatorListenerAdapter.onAnimationStart(animation);
                            }

                            if (mAnimationStepStateListener != null) {
                                mAnimationStepStateListener.onStartAnimation(callBackId, order);
                            }
                        }
                    });
                } else {
                    mAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            if (animationItem.ANIMATION_VIEW.getVisibility() != animationItem.VISIBILITY) {
                                animationItem.ANIMATION_VIEW.setVisibility(animationItem.VISIBILITY);
                            }
                        }
                    });
                }
            }
        }
    }

    private void endAnimation(View[] lockViewArray) {
        mAnimationList.clear();
        mOrderInfo.clear();
        mAnimator = null;
        for (View lockView : lockViewArray) {
            lockView.setEnabled(true);
        }
    }
}
