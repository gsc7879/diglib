package com.dignara.lib.animation;

import android.view.animation.Interpolator;

public class FrameMoveAnimation extends FrameMove {
	private Interpolator 				 mInterploator;
	
	private long 						 mDuration;
	private long 						 mCurTime;
	private long 						 mStartTime;

	private float 						 mSetStartFrame;
	private float 						 mSetEndFrame;

	private OnFrameMoveAnimationListener mListener;

	public void setOnAnimationEndModeListener(OnFrameMoveAnimationListener listener) {
		mListener = listener;
	}

	public interface OnFrameMoveAnimationListener {
		void OnAnimationEndFrameMove(float endFrame);
	}

	@Override
	public float moveFrame() {
		boolean nMode = true;
		if (nMode) {
			mCurTime = System.currentTimeMillis();
			if (mCurTime - mStartTime > mDuration) {
				endFrame();
			} else {
				float f = (float) (mCurTime - mStartTime) / (float) mDuration;
				float finterploator = mInterploator.getInterpolation(f);
				mCurFrame = (Math.round((mSetEndFrame - mSetStartFrame) * finterploator) + mSetStartFrame);
			}
		} else {
			if (Math.abs(mSetEndFrame - mCurFrame) <= 1) {
				// mCurFrame = Math.round(d);
				endFrame();
			} else {
				if (mSetEndFrame >= mCurFrame) {
					if (Math.abs(mSetEndFrame - mCurFrame) > 10) {
						mCurFrame = mCurFrame + 6;
					} else if (Math.abs(mSetEndFrame - mCurFrame) > 3) {
						mCurFrame = mCurFrame + 4;
					} else {
						mCurFrame = mCurFrame + 1;
					}

				} else {
					if (Math.abs(mSetEndFrame - mCurFrame) > 10) {
						mCurFrame = mCurFrame - 6;
					} else if (Math.abs(mSetEndFrame - mCurFrame) > 3) {
						mCurFrame = mCurFrame - 4;
					} else {
						mCurFrame = mCurFrame - 1;
					}
				}

			}
		}
		return mCurFrame;
	}

	protected void endFrame() {
		if (mListener != null) {
			mListener.OnAnimationEndFrameMove(mSetEndFrame);
			mListener = null;
		}
		mCurFrame = mSetEndFrame;
	}

	public void bindInterpolator(Interpolator interploator) {
		this.mInterploator = interploator;
	}

	public void startAnimation(float s, float e, long milliseconds) {
		mStartTime = System.currentTimeMillis();
		this.mSetStartFrame = s;
		this.mCurFrame = s;
		this.mSetEndFrame = e;
		this.mDuration = milliseconds;
	}
}
