package com.dignara.lib.animation;

import android.os.AsyncTask;
import android.view.animation.Interpolator;

public class FrameMoveTask extends AsyncTask<Interpolator, Float, Void> {
	private int					mCallbackId;
	private long 				mDuration;
	private long 				mCurTime;
	private long 				mStartTime;
	private float 				mCurFrame;
	private float 				mStartFrame;
	private float 				mEndFrame;
	private OnFrameMoveListener mFrameMoveListener;
	
	public interface OnFrameMoveListener {
		public void onStartFrameMove(int callbackId, float startFrame);
		public void onFrameChanged(int callbackId, float frame);
		public void onEndFrameMove(int callbackId, float endFrame);
	}
	
	public FrameMoveTask(int callbackId, float startFrame, float endFrame, long duration, OnFrameMoveListener frameMoveListener) {
		mCallbackId		   = callbackId;
		mStartTime  	   = System.currentTimeMillis();
		mStartFrame 	   = startFrame;
		mCurFrame   	   = startFrame;
		mEndFrame   	   = endFrame;
		mDuration   	   = duration;
		mFrameMoveListener = frameMoveListener;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		if (mFrameMoveListener != null) {
			mFrameMoveListener.onStartFrameMove(mCallbackId, mStartFrame);
		}
	}

	@Override
	protected void onPreExecute() {
		if (mFrameMoveListener != null) {
			mFrameMoveListener.onEndFrameMove(mCallbackId, mEndFrame);
		}
	}

	@Override
	protected void onProgressUpdate(Float... curFrame) {
		if (mFrameMoveListener != null) {
			mFrameMoveListener.onFrameChanged(mCallbackId, curFrame[0]);
		}
	}

	@Override
	protected Void doInBackground(Interpolator... interpolator) {
		do {
			publishProgress(moveFrame(interpolator[0]));
		} while (mCurFrame != mEndFrame);
		return null;
	}
	
	public float moveFrame(Interpolator interpolator) {
		mCurTime = System.currentTimeMillis();
		if (mCurTime - mStartTime > mDuration) {
			mCurFrame = mEndFrame;
		} else {
			float f = (float) (mCurTime - mStartTime) / (float) mDuration;
			float finterploator = interpolator.getInterpolation(f);
			mCurFrame = (Math.round((mEndFrame - mStartFrame) * finterploator) + mStartFrame);
		}
		return mCurFrame;
	}
}