package com.dignara.lib.popup;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.*;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.PopupWindow;

public abstract class QuickPopupMenu {
    public static final int ARROW_UP   = 0;
    public static final int ARROW_DOWN = 1;

    protected Context        mContext;
    protected LayoutInflater mInflater;
    protected PopupWindow    mWindow;
    protected View           mRootView;
    protected Drawable       mBackground;
    protected WindowManager  mWindowManager;

    private ImageView        mArrowUp;
    private ImageView        mArrowDown;
    private View             mSelectedView;

    private int              mAniStyleResId;

    private int              mOffsetX;
    private int              mOffsetY;
    private int              mPosition;


    public QuickPopupMenu(Context context, int rootLayoutResId) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext  = context;
        mWindow   = new PopupWindow(context);
        mPosition = -1;

        mWindow.setTouchInterceptor(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    mWindow.dismiss();
                    return true;
                }
                return false;
            }
        });
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        setRootViewId(rootLayoutResId);
    }

    public void setOffset(int offsetX, int offsetY) {
        mOffsetX = offsetX;
        mOffsetY = offsetY;
    }

    public View getRootView() {
        return mRootView;
    }

    public void setRootViewId(int rootLayoutResId) {
        mRootView = mInflater.inflate(rootLayoutResId, null);
        mRootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        setContentView(mRootView);
    }

    public void setSelectedView(View selectedView) {
        mSelectedView = selectedView;
    }

    public void setArrow(int downArrowResId, int upArrowResId) {
        mArrowDown = (ImageView) mRootView.findViewById(downArrowResId);
        mArrowUp   = (ImageView) mRootView.findViewById(upArrowResId);
    }

    public void setAnimStyle(int aniStyleResId) {
        mAniStyleResId = aniStyleResId;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public void show(View anchorView) {
        preShow();
        int xPos;
        int yPos;
        int arrowPos;
        int[] location = new int[2];
        anchorView.getLocationOnScreen(location);
        Rect anchorRect = new Rect(location[0], location[1], location[0] + anchorView.getWidth(), location[1] + anchorView.getHeight());
        mRootView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        ViewTreeObserver viewTreeObserver = mRootView.getViewTreeObserver();
//        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
//            public boolean onPreDraw() {
//                return true;
//            }
//        });

        int rootHeight   = mRootView.getMeasuredHeight();
        int rootWidth    = mRootView.getMeasuredWidth();
        int screenWidth  = mWindowManager.getDefaultDisplay().getWidth();
        int screenHeight = mWindowManager.getDefaultDisplay().getHeight();
        if ((anchorRect.left + rootWidth) > screenWidth) {
            xPos = anchorRect.left - (rootWidth - anchorView.getWidth());
            xPos = (xPos < 0) ? 0 : xPos;
            arrowPos = anchorRect.centerX() - xPos;
        } else {
            if (anchorView.getWidth() > rootWidth) {
                xPos = anchorRect.centerX() - (rootWidth / 2);
            } else {
                xPos = anchorRect.left;
            }
            arrowPos = anchorRect.centerX() - xPos;
        }
        int dyTop     = anchorRect.top;
        int dyBottom  = screenHeight - anchorRect.bottom;
        boolean onTop = (dyTop > dyBottom) ? true : false;
        if (mPosition > 0) {
            switch (mPosition) {
                case ARROW_UP:
                    onTop = true;
                    break;
                case ARROW_DOWN:
                    onTop = false;
                    break;
            }
        }
        if (onTop) {
            if (rootHeight > dyTop) {
                yPos = 15;
                ViewGroup.LayoutParams l = mRootView.getLayoutParams();
                l.height = dyTop - anchorView.getHeight();
            } else {
                yPos = anchorRect.top - rootHeight;
            }
        } else {
            yPos = anchorRect.bottom;
            if (rootHeight > dyBottom) {
                ViewGroup.LayoutParams l = mRootView.getLayoutParams();
                l.height = dyBottom;
            }
        }
        showArrow(((onTop) ? ARROW_DOWN : ARROW_UP), arrowPos);
//        setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);
        mWindow.showAtLocation(anchorView, Gravity.NO_GRAVITY, xPos + mOffsetX, yPos + mOffsetY);
        if (mSelectedView != null) {
            mSelectedView.setSelected(true);
        }
    }

    private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
        int arrowPos = requestedX - getArrowWidth();
        if (arrowPos <= screenWidth / 4) {
            mWindow.setAnimationStyle(mAniStyleResId);
        } else if (arrowPos > screenWidth / 4 && arrowPos < 3 * (screenWidth / 4)) {
            mWindow.setAnimationStyle(mAniStyleResId);
        } else {
            mWindow.setAnimationStyle(mAniStyleResId);
        }
    }

    private void showArrow(int arrowStatus, int requestedX) {
        if (mArrowUp == null || mArrowDown == null) {
            return;
        }
        final View showArrow = (arrowStatus == ARROW_UP) ? mArrowUp : mArrowDown;
        final View hideArrow = (arrowStatus == ARROW_UP) ? mArrowDown : mArrowUp;
        showArrow.setVisibility(View.VISIBLE);
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) showArrow.getLayoutParams();
        param.leftMargin = requestedX - getArrowWidth();
        hideArrow.setVisibility(View.INVISIBLE);
    }

    private final int getArrowWidth() {
        if (mArrowUp != null && mArrowDown != null) {
            return mArrowUp.getMeasuredWidth() / 2;
        }
        return 0;
    }

    protected void preShow() {
        if (mRootView == null) {
            throw new IllegalStateException("setContentView was not called with a view to display.");
        }
        if (mBackground == null) {
            mWindow.setBackgroundDrawable(new BitmapDrawable());
        } else {
            mWindow.setBackgroundDrawable(mBackground);
        }
        mWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        mWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        mWindow.setTouchable(true);
        mWindow.setFocusable(true);
        mWindow.setOutsideTouchable(true);
        mWindow.setContentView(mRootView);
    }

    public void setContentView(View root) {
        mRootView = root;
        mWindow.setContentView(root);
    }

    public void setContentView(int layoutResID) {
        LayoutInflater inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setContentView(inflator.inflate(layoutResID, null));
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
        mWindow.setOnDismissListener(listener);
    }

    public void dismiss() {
        mWindow.dismiss();
        if (mSelectedView != null) {
            mSelectedView.setSelected(false);
        }
    }
}