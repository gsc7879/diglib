package com.dignara.lib.popup;

import android.content.Context;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

public class LayoutQuickPopupMenu extends QuickPopupMenu implements View.OnClickListener {
    private OnClickListener mClickListener;
    private Map<Integer, View> mMenuViewMap;

    public interface OnClickListener {
        public void onClick(int id);
    }

    public LayoutQuickPopupMenu(Context context, int rootLayoutResId) {
        super(context, rootLayoutResId);
        mMenuViewMap = new HashMap<>();
    }

    public void setOnClickListener(OnClickListener clickListener) {
        this.mClickListener = clickListener;
    }

    public void setMenuView(int id, int menuResId) {
        View menuView = getRootView().findViewById(menuResId);
        menuView.setTag(id);
        menuView.setOnClickListener(this);
        mMenuViewMap.put(id, menuView);
    }

    public View getMenuView(int id) {
        return mMenuViewMap.get(id);
    }

    @Override
    public void onClick(View view) {
        if (mClickListener != null) {
            mClickListener.onClick((int) view.getTag());
        }
    }
}