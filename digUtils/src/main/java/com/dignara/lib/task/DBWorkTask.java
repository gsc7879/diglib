package com.dignara.lib.task;

import android.content.Context;
import com.dignara.lib.db.DBHelper;

public abstract class DBWorkTask<T> extends WorkTask<T> {
    private OnDBExecutionListener mOnDBExecutionListener;
    private OnDBProgressListener  mDBProgressListener;

	protected Context  mContext;
    protected DBHelper mDBHelper;
    protected int      mErrorCode;

    public interface OnDBExecutionListener<T>{
        void onDBReady(int callbackId);
        void onDBResult(int callbackId, T result);
        void onDBFailed(int callbackId, int errorCode);
    }

    public interface OnDBProgressListener {
        void onDBProgress(int callbackId, Object... object);
    }

	public DBWorkTask(int callBackId, Context context, DBHelper dbHelper, OnDBExecutionListener dbExecutionListener) {
		super(callBackId);
        mContext               = context;
        mDBHelper              = dbHelper;
        mOnDBExecutionListener = dbExecutionListener;
	}

    public void setDBProgressListener(OnDBProgressListener dbProgressListener) {
        mDBProgressListener = dbProgressListener;
    }

    @Override
    protected void onPreExecute() {
        if (mOnDBExecutionListener != null) {
            mOnDBExecutionListener.onDBReady(mCallBackId);
        }
    }

    @Override
    protected void onPostExecute(T result) {
        cleanup();
        if (mOnDBExecutionListener != null) {
            mOnDBExecutionListener.onDBResult(mCallBackId, result);
        }
    }

    @Override
    protected void onProgressUpdate(Object... params) {
        if (mDBProgressListener != null) {
            mDBProgressListener.onDBProgress(mCallBackId, params);
        }
    }

    @Override
    protected void onCancelled() {
        cleanup();
        if (mOnDBExecutionListener != null) {
            mOnDBExecutionListener.onDBFailed(mCallBackId, mErrorCode);
        }
    }

    private void cleanup() {
        if (mDBHelper != null) {
            mDBHelper.close();
            mDBHelper = null;
        }
        mContext = null;
    }
}
