package com.dignara.lib.task;

import android.graphics.drawable.Drawable;

public abstract class TaskListener {
	/**
	 * 로딩한 이미지를 반환함.
	 * @param callbackId 	테스크 아이디
	 * @param drawable	 	반환된 이미지
	 */
	public void onTaskDrawable(int callbackId, Drawable drawable) {
	}

	/**
	 * 테스크 진행 프로그래스
	 * 0-100까지 표시됨
	 * 
	 * @param callBackId
	 * @param progress
	 */
	public void onTaskProgress(int callBackId, int progress, long currentProgress, long totalProgress) {
	}

	/**
	 * 요청이 성공했을 때 호출됨
	 * 
	 * @param callBackId	테스크 아이디이터
	 */
	public void onTaskData(int callBackId, String returnData) {
	}

	/**
	 * 요청이 실패 했을 때 호출됨
	 * 
	 * @param callBackId	테스크 아이디
	 * @param failCode		요청 실패값
	 */
	public void onTaskFail(int callBackId, int failCode) {
	}

	/**
	 * 테스크가 취소 되었을 때 호출됨.
	 * 
	 * @param callBackId	테스크 아이디
	 */
	public void onCancelTask(int callBackId) {
	}

	/**
	 * 테스크를 시작 할 때 호출 됨
	 * 
	 * @param callBackId	테스크 아이디
	 */
	public void onStartTask(int callBackId) {
	}

	/**
	 * 테스크를 종료 되었을 때 호출 됨
	 * 
	 * @param callBackId	테스크 아이디
	 */
	public void onEndTask(int callBackId, int resultCode) {
	}
}
