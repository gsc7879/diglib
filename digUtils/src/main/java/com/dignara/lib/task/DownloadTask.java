package com.dignara.lib.task;

import android.os.AsyncTask;
import android.os.Environment;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ReadableByteChannel;

/**
 * 서버 파일 다운로드 클래스.<br/>
 * 서버의 파일을 지정한 위치롤 다운로드 한다.<br/>
 * execute(String serverURL	    파일 서버 주소 		 ex)http://xxx.xxx.xxx.xxx,
 *   	   String fileName	    파일을 저장할 이름,
 *	   	   String fileSize		파일의 크기<br/>
 *
 * 에러코드
 * RESULT_ERR_URL    서버 URL 에러
 * RESULT_ERR_PATH   저장경로 에러러 * @author Seong-Cheol Gwon (seongcheol.gwon@konantech.com)
 */
public class DownloadTask extends AsyncTask<String, Long, Integer> {
    private static final int BUFFER_COUNT = 1000;
    private static final int BUFFER_SIZE  = 4096;

    public static final int RESULT_SUCCESS    = 0;
    public static final int RESULT_ERR_URL    = 1;
    public static final int RESULT_ERR_PATH   = 2;
    public static final int RESULT_ERR_CANCEL = 3;

	private TaskListener  mTaskListener;
	private File mDownloadPath;
	private int			  mCallbackId;

	/**
	 * DownloadTask 생성자
	 * @param callbackId	테스크 아이디
	 * @param taskListener	테스크 리스너
	 */
	public DownloadTask(int callbackId, TaskListener taskListener) {
		mCallbackId	  = callbackId;
		mTaskListener = taskListener;
        mDownloadPath = Environment.getExternalStorageDirectory();
	}
	
	/**
	 * 파일을 저장할 위치 설정.
	 * 
	 * @param downloadFile  파일을 다운받을 위치
	 */
	public void setDownloadPath(File downloadFile) {
        if (!downloadFile.exists()) {
            downloadFile.mkdirs();
        }
		mDownloadPath = downloadFile;
	}

	@Override
	protected void onProgressUpdate(Long... progress) {
		if (mTaskListener != null) {
            long totalProgress   = progress[0];
            long currentProgress = progress[1];
            int percent = (int) (((float) totalProgress / (float) currentProgress) * 100);
			mTaskListener.onTaskProgress(mCallbackId, percent, totalProgress, currentProgress);
		}
	}

	@Override
	protected Integer doInBackground(String... path) {
		String serverURL = path[0];
        String fileName  = path[1];
        long   fileSize  = NumberUtils.toLong(path[2]);
        return downloadFromServer(serverURL, fileName, fileSize);
	}
	
	@Override
	protected void onPreExecute() {
		if (mTaskListener != null) {
			mTaskListener.onStartTask(mCallbackId);
		}
	}

	@Override
	protected void onCancelled() {
		if (mTaskListener != null) {
			mTaskListener.onEndTask(mCallbackId, RESULT_ERR_CANCEL);
		}
	}
	
	@Override
	protected void onPostExecute(Integer resultStatus) {
		if (mTaskListener != null) {
            if (resultStatus != RESULT_SUCCESS) {
                mTaskListener.onTaskFail(mCallbackId, resultStatus);
            }
			mTaskListener.onEndTask(mCallbackId, resultStatus);
		}
	}
	
	private int downloadFromServer(String serverURL, String fileName, long fileSize) {
        InputStream fis      = null;
        FileOutputStream fos = null;
        long downloaded      = 0;
        try {
            URL website = new URL(serverURL);
            fis = website.openStream();
            fos = new FileOutputStream(new File(mDownloadPath, fileName));
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            GatheringByteChannel gbc = fos.getChannel();
            ByteBuffer bb = ByteBuffer.allocateDirect(BUFFER_COUNT * BUFFER_SIZE);
            int read;
            while ((read = rbc.read(bb)) != -1) {
                bb.flip();
                gbc.write(bb);
                bb.clear();
                publishProgress(downloaded += read, fileSize);
            }
            return RESULT_SUCCESS;
        } catch (MalformedURLException e) {
            return RESULT_ERR_URL;
        } catch (FileNotFoundException e) {
            return RESULT_ERR_PATH;
        } catch (IOException e) {
            return RESULT_ERR_PATH;
        } finally {
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(fis);
        }
	}
}
