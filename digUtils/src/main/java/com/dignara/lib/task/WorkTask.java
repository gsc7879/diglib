package com.dignara.lib.task;

import android.os.AsyncTask;

public abstract class WorkTask<T> extends AsyncTask<Object, Object, T> {
	protected int 	 mCallBackId;
    protected int    mCancelCode;
    protected Object mData;

	private OnTaskListener mTaskListener;

	public interface OnTaskListener<T> {
		void onStartTask(int callBackId);
        void onEndTask(int callBackId, T object);
        void onCancelTask(int callBackId, int cancelCode, Object object);
        void onTaskProgress(int callBackId, Object... params);
	}

	public WorkTask(int callBackId) {
		this(callBackId, null);
	}

    public WorkTask(int callBackId, OnTaskListener taskListener) {
        mCallBackId   = callBackId;
        mTaskListener = taskListener;
    }

    public void setOnTaskListener(OnTaskListener taskListener) {
        mTaskListener = taskListener;
    }
	
	@Override
	protected void onPreExecute() {
        if (mTaskListener != null) {
            mTaskListener.onStartTask(mCallBackId);
        }
	}
	
	@Override
	protected void onProgressUpdate(Object... params) {
		if (mTaskListener != null) {
			mTaskListener.onTaskProgress(mCallBackId, params);
		}
	}
	
	@Override
	protected void onPostExecute(T object) {
        if (mTaskListener != null) {
            mTaskListener.onEndTask(mCallBackId, object);
        }
	}
	
	@Override
	protected void onCancelled() {
        if (mTaskListener != null) {
            mTaskListener.onCancelTask(mCallBackId, mCancelCode, mData);
        }
    }
}
