package com.dignara.lib.exception;

public class MediaPlayerException extends Exception {
    public static final int CODE_FILE_NOT_EXIST     = 0;
    public static final int CODE_FILE_NOT_SUPPORT   = 1;
    public static final int CODE_SECTION_NOT_EXIST  = 2;
    public static final int CODE_PLAYLIST_NOT_EXIST = 3;

    private int mediaId;
    private int errorCode;

    public MediaPlayerException(int mediaId, int errorCode) {
        this(mediaId, errorCode, null);
    }

    public MediaPlayerException(int mediaId, int errorCode, String detailMessage) {
        super(detailMessage);
        this.mediaId   = mediaId;
        this.errorCode = errorCode;
    }

    public int getMediaId() {
        return mediaId;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
