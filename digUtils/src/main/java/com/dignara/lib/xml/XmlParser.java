package com.dignara.lib.xml;

import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XmlParser {
	private static XMLReader initializeReader() throws ParserConfigurationException, SAXException {
		SAXParser parser 	= SAXParserFactory.newInstance().newSAXParser();
		XMLReader xmlreader = parser.getXMLReader();
		return xmlreader;
	}

	public static XmlHandler parseXml(String xml, XmlHandler xmlHandler) {
		try {
			XMLReader xmlreader = initializeReader();
			xmlreader.setContentHandler(xmlHandler);
			xmlreader.parse(new InputSource(new StringReader(xml)));
			return xmlHandler;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
