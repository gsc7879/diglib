package com.dignara.lib.xml;

import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlHandler extends DefaultHandler {
    private StringBuilder mXmlText;

    protected String 	  mContent;

    public XmlHandler() {
        mXmlText = new StringBuilder();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        mXmlText.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        mContent = StringUtils.trim(StringUtils.defaultString(mXmlText.toString()));
        mXmlText = new StringBuilder();
    }
}
