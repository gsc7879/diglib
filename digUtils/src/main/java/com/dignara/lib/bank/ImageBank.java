package com.dignara.lib.bank;

import android.content.Context;
import com.squareup.picasso.Picasso;

public class ImageBank {
    private static ImageBank uniqueImageBank;

    private Picasso picasso;

    public synchronized static ImageBank getInstance() {
        if (uniqueImageBank == null) {
            uniqueImageBank = new ImageBank();
        }
        return uniqueImageBank;
    }

    public void init(Context context) {
        picasso = Picasso.with(context);
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
