package com.dignara.lib.media;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class PlayFolder {
    protected File   file;
    protected String name;

    public PlayFolder(File file) {
        this(file, null);
    }

    public PlayFolder(File file, String name) {
        this.file = file;
        this.name = name == null ? file.getName() : name;
    }

    public boolean isSubFolder(PlayFolder playFolder) {
        return file != null && playFolder != null && StringUtils.contains(playFolder.getFile().getPath(), file.getPath());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        return file;
    }
}
