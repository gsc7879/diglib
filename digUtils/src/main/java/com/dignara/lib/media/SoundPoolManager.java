package com.dignara.lib.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPoolManager {
	private Context 	 mContext;
	private SoundPool 	 mSoundPool;
	private AudioManager mAudioManager;
	private int			 mStreamId;
	private boolean 	 mIsReadyPlay;
	
	public SoundPoolManager(Context context) {
		mContext 	  = context;
		mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
	}
	
	public void playSound(String path, final float rate) {
		if (mIsReadyPlay) {
			return;
		}
		mIsReadyPlay = true;
		stopSound();
		mSoundPool = new SoundPool(1, AudioManager.STREAM_RING, 0);
		mStreamId  = mSoundPool.load(path, 1);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Froyo
//		mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
//			@Override
//			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				int streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
				mSoundPool.play(mStreamId, streamVolume, streamVolume, 0, 0, rate);
				mIsReadyPlay = false;
//			}
//		});
	}
	
	public void stopSound() {
		if (mSoundPool == null) {
			return;
		}
		mSoundPool.stop(mStreamId);  
		mSoundPool.release();  
		mSoundPool = null; 
	}
	
	public void cleanup() {
		stopSound();
		mContext = null;
		mAudioManager = null;
	}
}