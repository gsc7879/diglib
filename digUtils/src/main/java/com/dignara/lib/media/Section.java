package com.dignara.lib.media;

public class Section {
    private int     id;
    private String  title;
    private int     startPos;
    private int     endPos;
    private boolean isSkip;

    public Section(int id, String title, int startPos, int endPos) {
        this.id       = id;
        this.title    = title;
        this.startPos = startPos;
        this.endPos   = endPos;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getStartPos() {
        return startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public boolean isSkip() {
        return isSkip;
    }

    public void setSkip(boolean skip) {
        isSkip = skip;
    }
}
