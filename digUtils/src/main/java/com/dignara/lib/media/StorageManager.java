package com.dignara.lib.media;

import android.os.Environment;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;

public class StorageManager {
    private File            mCurrentFolder;
    private List<PlayDrive> mDriveList;
    private String          mInternalName;
    private String          mExternalName;

    public StorageManager(String internalName, String externalName) {
        mInternalName = internalName;
        mExternalName = externalName;
        initDrive();
    }

    private void initDrive() {
        mDriveList     = getStorageList();
        mCurrentFolder = null;
    }

    public List<? extends PlayFolder> getFiles(String[] extensions) {
        if (mCurrentFolder == null) {
            return mDriveList;
        }
        File[] folderFiles = mCurrentFolder.listFiles();
        Collection<File> fileList = FileUtils.listFiles(mCurrentFolder, extensions, false);
        List<PlayFolder> playFileList = new ArrayList<>();
        for (File file : folderFiles) {
            if (file.isDirectory() && !StringUtils.startsWith(file.getName(), ".")) {
                playFileList.add(new PlayFolder(file));
            }
        }
        for (File file : fileList) {
            playFileList.add(file.isDirectory() ? new PlayFolder(file) : new PlayFile(file));
        }
        return playFileList;
    }

    public List<? extends PlayFolder> goParents(String[] extensions) {
        if (mCurrentFolder != null) {
            if (getMatchDrive(mCurrentFolder.getPath()) != null) {
                mCurrentFolder = null;
            } else {
                mCurrentFolder = mCurrentFolder.getParentFile();
            }
        }
        return getFiles(extensions);
    }

    public String getCurrentFolderName() {
        if (mCurrentFolder == null) {
            return "";
        }
        return getFileName();
    }

    private PlayDrive getMatchDrive(String path) {
        for (PlayDrive playDrive : mDriveList) {
            if (StringUtils.equals(playDrive.getFile().getPath(), path)) {
                return playDrive;
            }
        }
        return null;
    }

    private String getFileName() {
        for (PlayDrive playDrive : mDriveList) {
            if (StringUtils.contains(mCurrentFolder.getPath(), playDrive.getFile().getPath())) {
                return StringUtils.replaceOnce(mCurrentFolder.getPath(), playDrive.getFile().getPath(), playDrive.getName());
            }
        }
        return mCurrentFolder.getPath();
    }

    public void setCurrentFolder(File currentFolder) {
        mCurrentFolder = currentFolder;
    }

    public void setCurrentFolder(PlayFolder playFolder) {
        if (playFolder instanceof PlayFile) {
            mCurrentFolder = playFolder.getFile().getParentFile();
        } else {
            mCurrentFolder = playFolder.getFile();
        }
    }

    public File getCurrentFolder() {
        return mCurrentFolder;
    }

    public boolean isRoot() {
        return mCurrentFolder == null;
    }

    public String byteToSize(long size) {
        if (size / 1073741824L > 0L) {
            return size / 1073741824L + " GB";
        } else if (size / 1048576L > 0L) {
            return size / 1048576L + " MB";
        } else if (size / 1024L > 0L) {
            return size / 1024L + " KB";
        } else {
            return size + " bytes";
        }
    }

    private List<PlayDrive> getStorageList() {
        ArrayList driveList = new ArrayList();
        File storagePath    = Environment.getExternalStorageDirectory();
        String storageState = Environment.getExternalStorageState();
        boolean isRemovable = Environment.isExternalStorageRemovable();
        boolean isAvailable = storageState.equals("mounted") || storageState.equals("mounted_ro");
        boolean isReadOnly  = Environment.getExternalStorageState().equals("mounted_ro");
        HashSet paths = new HashSet();
        int removableNumber = 1;
        if (isAvailable) {
            paths.add(storagePath);
            driveList.add(0, new PlayDrive(storagePath, isReadOnly, isRemovable, isRemovable ? removableNumber++ : -1, mInternalName, mExternalName));
        }
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader("/proc/mounts"));
            while (true) {
                String ex;
                do {
                    if ((ex = bufferedReader.readLine()) == null) {
                        return driveList;
                    }
                } while (!ex.contains("vfat") && !ex.contains("/mnt"));
                StringTokenizer tokens = new StringTokenizer(ex, " ");
                String unused = tokens.nextToken();
                String mount_point = tokens.nextToken();
                if (!paths.contains(mount_point)) {
                    unused = tokens.nextToken();
                    List flags = Arrays.asList(tokens.nextToken().split(","));
                    boolean readonly = flags.contains("ro");
                    if (ex.contains("/dev/block/vold") && !ex.contains("/mnt/secure") && !ex.contains("/mnt/asec") && !ex.contains("/mnt/obb") && !ex.contains("/dev/mapper") && !ex.contains("tmpfs")) {
                        mount_point = StringUtils.replace(mount_point, "/mnt/media_rw/", "/storage/");
                        if (FileUtils.getFile(new String[]{mount_point}).exists()) {
                            paths.add(mount_point);
                            driveList.add(new PlayDrive(FileUtils.getFile(mount_point), readonly, true, removableNumber++, mInternalName, mExternalName));
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException var24) {
                }
            }
        }
        return driveList;
    }
}
