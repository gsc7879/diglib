package com.dignara.lib.media;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Handler;
import android.os.Message;
import com.dignara.lib.exception.MediaPlayerException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class MediaPlayerManager implements OnErrorListener, OnCompletionListener, MediaPlayer.OnInfoListener, PlayFile.SectionListListener, PlayList.PlayListListener, MediaPlayer.OnSeekCompleteListener {
    public static final int STATUS_PLAY             = 0;
    public static final int STATUS_PLAY_PAUSE       = 1;
    public static final int STATUS_PLAY_SEARCH      = 2;
    public static final int STATUS_SECTION_PLAY     = 3;
    public static final int STATUS_SECTION_SEARCH   = 4;
    public static final int STATUS_BLANK_START      = 5;
    public static final int STATUS_BLANK_END        = 6;
    public static final int STATUS_SECTION_PAUSE    = 7;
    public static final int STATUS_PREPARED         = 9;
    public static final int STATUS_ERROR            = 10;
    public static final int STATUS_STOP             = 11;
    public static final int STATUS_END              = 12;

    public static final int ID_PLAY_NORMAL          = 0;
    public static final int ID_PLAY_RECENT          = 1;
    public static final int ID_PLAY_STOP            = 2;
    public static final int ID_PLAY_FILE            = 3;

    private MediaPlayer 		mPlayer;
    private PlayList            mPlayList;
    private MediaPlayerListener mMediaPlayerListener;

    private int					mCallbackId;
    private int 		 		mSearchWeight;
    private boolean             mIsBlank;

    public interface MediaPlayerListener {
        void onPlayProgress(int callbackId, int progress);
        void onPlayerStatus(int callbackId, int playerStatus);
        void onChangeSection(int startPos, int endPos);
        void onChangePlayMode(int playMode);
        void onChangePlayShuffle(boolean isShuffle);
        void onChangePlayFile(PlayFile[] playFiles);
        void onChangeBlankMode(boolean isBlank);
        void onChangeBlankTime(int blankTime);
    }

    private Handler mBlankHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int remainBlankTime = msg.arg1;
            mMediaPlayerListener.onChangeBlankTime(remainBlankTime);
            if (!mIsBlank || remainBlankTime <= 0) {
                sendStatus(STATUS_BLANK_END);
                mPlayer.seekTo(getPlayFile().getStartPos());
            } else {
                remainBlankTime -= 1000;
                int delayTime = remainBlankTime < 0 ? 1000 - remainBlankTime : 1000;
                Message sendMsg = new Message();
                sendMsg.arg1 = remainBlankTime;
                mBlankHandler.sendMessageDelayed(sendMsg, delayTime);
            }
        }
    };

    public void toggleBlankMode() {
        setBlankMode(!mIsBlank);
    }

    public void setBlankMode(boolean isBlank) {
        mIsBlank = isBlank;
        mMediaPlayerListener.onChangeBlankMode(mIsBlank);
    }

    private Handler mProgressHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isPlaying()) {
                int curPosition = mPlayer.getCurrentPosition();
                mMediaPlayerListener.onPlayProgress(mCallbackId, curPosition);
                if (isResetSection(curPosition)) {
                    if (isRepeatMode() && mIsBlank) {
                        startBlank();
                    } else {
                        mPlayer.seekTo(getPlayFile().getStartPos());
                    }
                }
                mProgressHandler.sendEmptyMessageDelayed(0, 50);
            }
        }
    };

    private boolean isResetSection(int curPosition) {
        return getPlayFile().getEndPos() > 0 && (curPosition >= getPlayFile().getEndPos() || curPosition < getPlayFile().getStartPos());
    }

    private Handler mSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mSearchWeight != 0) {
                int nextPos = mPlayer.getCurrentPosition() + (500 * mSearchWeight);
                if (nextPos < 1) {
                    stopPlayer();
                    return;
                }
                mPlayer.seekTo(nextPos);
                mSearchHandler.sendEmptyMessageDelayed(0, 500);
            }
        }
    };

    public MediaPlayerManager(MediaPlayerListener mediaPlayerListener) {
        mMediaPlayerListener = mediaPlayerListener;
        mPlayList            = new PlayList(this);
        mPlayer              = new MediaPlayer();
        mPlayer.setOnSeekCompleteListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnInfoListener(this);
    }

    private void startBlank() {
        mPlayer.pause();
        Message msg = new Message();
        msg.arg1 = getPlayFile().getEndPos() - getPlayFile().getStartPos();
        sendStatus(STATUS_BLANK_START);
        mBlankHandler.removeMessages(0);
        mBlankHandler.sendMessage(msg);
    }

    @Override
    public void onSeekComplete(MediaPlayer mediaPlayer) {
        startPlayer();
    }

    private void sendRepeatStatus() {
        if (isRepeatMode()) {
            sendStatus(STATUS_SECTION_PLAY);
        }
    }

    public PlayFile getPlayFile() {
        return mPlayList.getPlayFile();
    }

    public void stopRepeat() {
        getPlayFile().clearSection();
        mBlankHandler.removeMessages(0);
        startPlayer();
        sendStatus(STATUS_PLAY);
    }

    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    public void search(int speed) {
        if (mPlayList == null || mSearchWeight == speed) {
            return;
        }

        if (mSearchWeight == 0) {
            if (isRepeatMode()) {
                sendStatus(STATUS_SECTION_SEARCH);
            } else {
                sendStatus(STATUS_PLAY_SEARCH);
            }
        }

        mSearchWeight = speed;
        mSearchHandler.removeMessages(0);
        mSearchHandler.sendEmptyMessage(0);
    }

    public boolean isRepeatMode() {
        return getPlayFile().isSection();
    }

    public int getDuration() {
        return mPlayer.getDuration();
    }

    public void initPlayerInfo() {
        mBlankHandler.removeMessages(0);
        mSearchHandler.removeMessages(0);
        mSearchWeight = 0;
        setBlankMode(false);
    }

    public void setNextPlayMode() {
        mPlayList.setNextPlayMode();
    }

    public void setPlayFileList(List<PlayFile> playFileList) {
        mPlayList.clearList();
        mPlayList.setPlayList(playFileList);
    }

    public void setPlayList(PlayFile playFile) {
        mPlayList.clearList();
        mPlayList.addPlayFile(playFile);
    }

    public void playNextFile(int callbackId) throws MediaPlayerException {
        if (mPlayList.setNextPlayFile() != null) {
            prepareMediaPlayer(callbackId, mPlayList.getPlayIndex());
        }
    }

    public void playPrevFile(int callbackId) throws MediaPlayerException {
        if (mPlayList.setPrevPlayFile() != null) {
            prepareMediaPlayer(callbackId, mPlayList.getPlayIndex());
        }
    }

    public void prepareMediaPlayer(int callbackId, PlayFile playFile) throws MediaPlayerException {
        setPlayList(playFile);
        prepareMediaPlayer(callbackId, 0);
    }

    public void prepareMediaPlayer(int callbackId, List<PlayFile> playFileList, int playIndex) throws MediaPlayerException {
        setPlayFileList(playFileList);
        prepareMediaPlayer(callbackId, playIndex);
    }

    public void prepareMediaPlayer(int callbackId, int playIndex) throws MediaPlayerException {
        resetPlayer();
        try {
            if (mPlayList.getCount() < 1) {
                throw new MediaPlayerException(mCallbackId, MediaPlayerException.CODE_PLAYLIST_NOT_EXIST);
            }
            PlayFile playFile = mPlayList.setPlayFile(playIndex);
            mPlayer.setDataSource(playFile.getFile().getAbsolutePath());
            try {
                mPlayer.prepare();
            } catch (Exception e) {
                throw new MediaPlayerException(mCallbackId, MediaPlayerException.CODE_FILE_NOT_SUPPORT);
            }
            playFile.setSectionListListener(this);
            mCallbackId = callbackId;
            sendStatus(STATUS_PREPARED);
            if (playFile.isSection()) {
                mPlayer.seekTo(playFile.getStartPos());
            }
        } catch (IllegalArgumentException e) {
            throw new MediaPlayerException(mCallbackId, MediaPlayerException.CODE_FILE_NOT_EXIST);
        } catch (FileNotFoundException e) {
            throw new MediaPlayerException(mCallbackId, MediaPlayerException.CODE_FILE_NOT_EXIST);
        } catch (IOException e) {
            throw new MediaPlayerException(mCallbackId, MediaPlayerException.CODE_FILE_NOT_EXIST);
        }
    }

    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    public void pausePlayer() {
        if (isPlaying()) {
            mSearchWeight = 0;
            mPlayer.pause();
            if (isRepeatMode()) {
                sendStatus(STATUS_SECTION_PAUSE);
            } else {
                sendStatus(STATUS_PLAY_PAUSE);
            }
        }
    }

    public void stopPlayer() {
        if (isRepeatMode()) {
            stopRepeat();
        }
        if (!isPlaying()) {
            return;
        }
        mPlayer.stop();
        initPlayerInfo();
        sendStatus(STATUS_STOP);

        mCallbackId = ID_PLAY_STOP;
        try {
            mPlayer.prepare();
        } catch (Exception e) {
            sendStatus(STATUS_ERROR);
        }
    }

    public void seekTo(int position) {
        mPlayer.seekTo(position);
    }

    public boolean isPause() {
        return !isPlaying();
    }

    public void startPlayer() {
        mSearchWeight = 0;
        if (!isPlaying() && mPlayList.getCount() > 0) {
            mPlayer.start();
            if (isRepeatMode()) {
                sendStatus(STATUS_SECTION_PLAY);
            } else {
                sendStatus(STATUS_PLAY);
            }
            mProgressHandler.removeMessages(0);
            mProgressHandler.sendEmptyMessageDelayed(0, 0);
        }
    }

    public void toggleShuffle() {
        mPlayList.toggleShuffle();
    }

    private void resetPlayer() {
        if (mCallbackId == -1) {
            return;
        }
        mPlayer.reset();
        initPlayerInfo();

    }

    public void cleanup() {
        resetPlayer();
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        mMediaPlayerListener = null;
    }

    public int setRepeatStartPos() {
        if (mPlayList.getCount() == 0) {
            return 0;
        }
        int currentPosition = mPlayer.getCurrentPosition();
        getPlayFile().setStartSection(currentPosition);
        sendRepeatStatus();
        return getPlayFile().getStartPos();
    }

    public int setRepeatEndPos() {
        if (mPlayList.getCount() == 0) {
            return 0;
        }
        int currentPosition = mPlayer.getCurrentPosition();
        getPlayFile().setEndSection(currentPosition);
        sendRepeatStatus();
        return getPlayFile().getEndPos();
    }

    public void setPrevSection() {
        if (mPlayList.getCount() == 0) {
            return;
        }
        getPlayFile().setPrevSection();
    }

    public void setRandomSection() {
        if (mPlayList.getCount() == 0) {
            return;
        }
        getPlayFile().setRandomSection();
    }

    public void setNextSection() {
        if (mPlayList.getCount() == 0) {
            return;
        }
        getPlayFile().setNextSection();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        try {
            sendStatus(STATUS_END);
            playNextFile(mCallbackId);
        } catch (MediaPlayerException e) {
            resetPlayer();
            sendStatus(STATUS_ERROR);
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int what, int extra) {
        return false;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        resetPlayer();
        sendStatus(STATUS_ERROR);
        return false;
    }

    @Override
    public void onChangeSection(int startPos, int endPos) {
        if (isRepeatMode()) {
            mMediaPlayerListener.onChangeSection(startPos, endPos);
        }
        sendRepeatStatus();
        if (!isPlaying()) {
            startPlayer();
        }
    }

    @Override
    public void onChangePlayMode(int playMode) {
        mMediaPlayerListener.onChangePlayMode(playMode);
    }

    @Override
    public void onChangePlayShuffle(boolean isShuffle) {
        mMediaPlayerListener.onChangePlayShuffle(isShuffle);
    }

    @Override
    public void onChangePlayFile(PlayFile[] playFiles) {
        mMediaPlayerListener.onChangePlayFile(playFiles);
    }

    private void sendStatus(int status) {
        mMediaPlayerListener.onPlayerStatus(mCallbackId, status);
    }
}
