package com.dignara.lib.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Parcel;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SpeedControl {
	private static final String CLASS_AUDIO_MANAGER = "android.media.AudioManager";
	private static final String FIELD_SA_SET_SPEED = "SA_SET_SPEED";
	private static final String MP_NEW_REQUEST_METHOD = "newRequest";
	private static final String MP_SET_SOUND_ALIVE_METHOD = "setSoundAlive";

	private static final int ERROR = -1;
	private static int SA_SET_SPEED = ERROR;

	private static int setSpeedConstant(Context context) {
		if (context == null) {
			return ERROR;
		}

		AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		if (am == null) {
			return ERROR;
		}

		try {
			Class<?> audioMgr = Class.forName(CLASS_AUDIO_MANAGER);
			Field field = audioMgr.getField(FIELD_SA_SET_SPEED);
			SA_SET_SPEED = field.getInt(am);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SA_SET_SPEED;
	}

	private static Parcel newRequest(MediaPlayer mp) {
		Object obj = null;

		try {
			Class<?> c = mp.getClass();
			String methodName = SpeedControl.MP_NEW_REQUEST_METHOD;
			Method m = c.getMethod(methodName);
			obj = m.invoke(mp);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (Parcel) obj;
	}

	private static void setSoundAlive(MediaPlayer mp, Parcel playSpeed, Parcel reply) {
		try {
			Class<?> c = mp.getClass();
			String methodName = SpeedControl.MP_SET_SOUND_ALIVE_METHOD;
			Method m = c.getMethod(methodName, Parcel.class, Parcel.class);
			m.invoke(mp, playSpeed, reply);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isPlaySpeedSupport(Context context) {

		if (SA_SET_SPEED == ERROR) {
			if (setSpeedConstant(context) == ERROR) {
				return false;
			}
		}

		return true;
	}

	public static void setPlaySpeed(Context context, MediaPlayer mp, float speed) {
		if (mp == null) {
			return;
		}

		if (!isPlaySpeedSupport(context)) {
			return;
		}

		try {
			Parcel playSpeed = newRequest(mp);
			Parcel playSpeed_Reply = Parcel.obtain();

			if (playSpeed != null) {
				playSpeed.writeInt(SA_SET_SPEED);
				playSpeed.writeFloat(speed);
			}

			setSoundAlive(mp, playSpeed, playSpeed_Reply);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}

}
