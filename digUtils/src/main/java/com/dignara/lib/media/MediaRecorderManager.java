package com.dignara.lib.media;

import android.os.Handler;
import android.os.Message;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class MediaRecorderManager {
    public static final int UPDATE_INTERVAL = 100;

    public static final int REC_STATUS_STOP  = 0;
    public static final int REC_STATUS_START = 1;
    public static final int REC_STATUS_ERR   = 2;

    private ExtAudioRecorder      mExtAudioRecorder;
    private MediaRecorderListener mMediaRecorderListener;

    private int mCallbackId;
    private int mRecStatus;
    private int mRecordTime;

    public interface MediaRecorderListener {
        public void onRecordProgress(int callbackId, int progress);
        public void onRecordStatus(int callbackId, int recorderStatus);
    }

    private Handler mProgressHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mRecStatus == REC_STATUS_START) {
                mRecordTime += UPDATE_INTERVAL;
                mMediaRecorderListener.onRecordProgress(mCallbackId, mRecordTime);
                mProgressHandler.sendEmptyMessageDelayed(0, UPDATE_INTERVAL);
            }
        }
    };

    public MediaRecorderManager(MediaRecorderListener mediaRecorderListener) {
        mExtAudioRecorder      = new ExtAudioRecorder();
        mMediaRecorderListener = mediaRecorderListener;
        mRecStatus 	           = REC_STATUS_STOP;
    }

    public boolean isRec() {
        return mRecStatus == REC_STATUS_START ? true : false;
    }

    public File getRecFile() {
        return mExtAudioRecorder.getRecFile();
    }

    public boolean startRec(int callbackId, File recFile) {
        File tempFile = FileUtils.getFile(recFile.getParent(), "temp.raw");
        mExtAudioRecorder.startRecording(tempFile, recFile);

        mCallbackId = callbackId;
        mRecStatus 	= REC_STATUS_START;
        mRecordTime = 0;
        mMediaRecorderListener.onRecordStatus(callbackId, mRecStatus);
        mProgressHandler.sendEmptyMessageDelayed(0, UPDATE_INTERVAL);
        return true;
    }

    public int progress() {
        return mRecordTime;
    }

    public void stopRec() {
        if (mRecStatus == REC_STATUS_START) {
            mExtAudioRecorder.stopRecording();
            mRecStatus = REC_STATUS_STOP;
            mRecordTime = 0;
            mMediaRecorderListener.onRecordStatus(mCallbackId, mRecStatus);
        }
    }

    public int getStatus() {
        return mRecStatus;
    }

    public void cleanup() {
        stopRec();
        mMediaRecorderListener = null;
    }

//    @Override
//    public void onError(MediaRecorder mr, int what, int extra) {
//        mMediaRecorderListener.onRecordStatus(mCallbackId, REC_STATUS_ERR);
//        stopRec();
//    }
//
//    @Override
//    public void onInfo(MediaRecorder mr, int what, int extra) {
//        if (MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED == what) {
//            mMediaRecorderListener.onRecordStatus(mCallbackId, REC_STATUS_ERR);
//            stopRec();
//        }
//
//    }
}