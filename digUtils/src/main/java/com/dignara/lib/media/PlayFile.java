package com.dignara.lib.media;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class PlayFile extends PlayFolder {
    private SectionListListener mSectionListListener;

    private List<Section> mSectionList;
    private int           mStartPos;
    private int           mEndPos;
    private int           mSectionIndex;

    public interface SectionListListener {
        void onChangeSection(int startPos, int endPos);
    }

    public PlayFile(File file) {
        this(file, new LinkedList<Section>());
    }

    public PlayFile(File file, List<Section> sectionList) {
        super(file);
        mSectionList = sectionList;
    }

    public boolean isSection() {
        return mStartPos != 0 && mEndPos > mStartPos;
    }

    public void setStartSection(int start) {
        mStartPos = start;
        if (mSectionListListener != null) {
            mSectionListListener.onChangeSection(mStartPos, mEndPos);
        }
    }

    public void setEndSection(int end) {
        if (mStartPos >= end) {
            return;
        }
        mEndPos = end;
        if (mSectionListListener != null) {
            mSectionListListener.onChangeSection(mStartPos, mEndPos);
        }
    }

    public int getStartPos() {
        return mStartPos;
    }

    public int getEndPos() {
        return mEndPos;
    }

    public void setSectionListListener(SectionListListener sectionListListener) {
        mSectionListListener = sectionListListener;
    }

    public List<Section> getSectionList() {
        return mSectionList;
    }

    public void setSectionList(List<Section> sectionList) {
        mSectionList = sectionList;
    }

    public void clearSection() {
        mSectionList.clear();
        mSectionIndex = 0;
        mStartPos     = 0;
        mEndPos       = 0;
    }

    public int getSectionCount() {
        return mSectionList.size();
    }

    public boolean setPrevSection() {
        if (mSectionIndex-- < 0) {
            mSectionIndex = mSectionList.size() - 1;
        }
        return setSection();
    }

    public boolean setNextSection() {
        if (mSectionIndex++ > mSectionList.size() - 1) {
            mSectionIndex = 0;
        }
        return setSection();
    }

    public boolean setRandomSection() {
        Random random = new Random();
        if (mSectionList.size() < 2) {
            return false;
        }
        mSectionIndex = random.nextInt(mSectionList.size());
        return setSection();
    }

    private boolean setSection() {
        try {
            Section section = mSectionList.get(mSectionIndex);
            setStartSection(section.getStartPos());
            setEndSection(section.getEndPos());
            return true;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public void addSection(int id, String title, int startPos, int endPos) {
        if (!isDuplicationSection(startPos, endPos)) {
            mSectionList.add(new Section(id, title, startPos, endPos));
        }
    }

    public boolean isDuplicationSection(int startPos, int endPos) {
        for (Section section : mSectionList) {
            if (section.getStartPos() == startPos && section.getEndPos() == endPos) {
                return true;
            }
        }
        return false;
    }

    public boolean removeSection(int startPos, int endPos) {
        for (Section section : mSectionList) {
            if (section.getStartPos() == startPos && section.getEndPos() == endPos) {
                mSectionList.remove(section);
                return true;
            }
        }
        return false;
    }

    public boolean isEqualFile(PlayFile playFile) {
        return file != null && playFile != null && StringUtils.equals(file.getPath(), playFile.getFile().getPath());
    }

    public boolean isEqualSection(PlayFile playFile) {
        return isEqualSection(playFile.getStartPos(), playFile.getEndPos());
    }

    public boolean isEqualSection(int startPos, int endPos) {
        return mStartPos == startPos && mEndPos == endPos;
    }
}
