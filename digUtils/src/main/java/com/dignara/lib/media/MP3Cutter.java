package com.dignara.lib.media;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class MP3Cutter {
	private final int MPEG_VERSION1 = 3;
	private final int MPEG_VERSION2 = 2;
	private final int MPEG_VERSION2_5 = 0;

	private final int[][] BITRATES = { { 0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, -1 }, { 0, 32, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, -1 }, { 0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, -1 }, { 0, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256, -1 }, { 0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160, -1 }, };

	private final int[][] SAMPLERATES = { { 11025, 12000, 8000, -1 }, // Version
																		// 2.5
			{ -1, -1, -1, -1 }, // Unknown version
			{ 22050, 24000, 16000, -1 }, // Version 2
			{ 44100, 44800, 32000, -1 }, // Version 1
	};

	private RandomAccessFile out = null;

	public MP3Cutter(String subFolder, String saveFile) {
		File file = new File(subFolder);
		file.mkdirs();
		try {
			out = new RandomAccessFile(subFolder + saveFile, "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// startPoint�� endPoint������ mp3����Ÿ�� ���ο� ���Ͽ� ����
	public void cut(String sourcePath, int startPoint, int endPoint) throws IOException {
		RandomAccessFile in = new RandomAccessFile(sourcePath, "r");
		
		byte[] buf = null;
		long mp3Frame = 0x00;
		int currentTimestamp = 0;
		long start = 0L;
		long size = 0L;

		// �߶󳻱� ������ �������� ���� �κ��̸� ��ŵ�Ѵ�.
		while (currentTimestamp < startPoint) {
			mp3Frame = readUnsignedInt(in);
			in.skipBytes(rawDataSize(mp3Frame));
			currentTimestamp += 26;
		}

		// ������� endPoint���� �߶� �����̴�.
		start = in.getFilePointer();
		while (currentTimestamp < endPoint) {
			mp3Frame = readUnsignedInt(in);
			in.skipBytes(rawDataSize(mp3Frame));
			
			currentTimestamp += 26;
		}
		size = in.getFilePointer() - start;
		buf = new byte[(int) size];
		in.readFully(buf);
		out.write(buf);

		// endPoint���� ����Ÿ �ۼ��Ϸ�. ��Ʈ���� �ݴ´�.
		if (in != null) {
			in.close();
			in = null;
		}
		if (out != null) {
			out.close();
			out = null;
		}
	}

	/*
	 * MP3 ������ ����� �������� ���ؼ� �ۼ����� �޼ҵ� ��ũ ��Ʈ�� '1111 1111 111' ���� �����ϴµ� �̴� ù �ڸ��� 1��
	 * ���� ���� ���� �ȴ�. ������ ������ �� ���ɼ��� �����Ƿ� longŸ���� ���� 4����Ʈ�� ����ϴ� ������� ������ �߻��� ���ɼ���
	 * �����ߴ�.
	 */
	private long readUnsignedInt(RandomAccessFile in) throws IOException {
		long rv = 0L;
		rv = rv | in.readInt();
		return rv;
	}

	// MP3�� �����ӿ� �ο쵥��Ÿ�� ũ�⸦ ����ϴ� �޼ҵ�
	private int rawDataSize(long mp3Frame) {
		int rawDataSize = 0;
		int mpegVersion = 0; // 2
		int layer = 0; // 2
		int bitrate = 0; // 4
		int samplingRate = 0; // 2
		int paddingBit = 0; // 1
		int col1 = 0, col2 = 0;
		int row1 = 0, row2 = 0;

		// ������ʹ� MP3 Frame�� �� ������ ����
		mpegVersion = (int) (((mp3Frame << 11) & 0x00000000FFFFFFFFL) >> 30);
		layer = (int) (((mp3Frame << 13) & 0x00000000FFFFFFFFL) >> 30);
		bitrate = (int) (((mp3Frame << 16) & 0x00000000FFFFFFFFL) >> 28);
		samplingRate = (int) (((mp3Frame << 20) & 0x00000000FFFFFFFFL) >> 30);
		paddingBit = (int) (((mp3Frame << 22) & 0x00000000FFFFFFFFL) >> 31);

		// �� �κ��� Bitrate �ε��� ���� �κ�
		if (mpegVersion == 3 && layer == 3)
			row1 = 0;
		else if (mpegVersion == 3 && layer == 2)
			row1 = 1;
		else if (mpegVersion == 3 && layer == 1)
			row1 = 2;
		else if (mpegVersion == 2 && layer == 3)
			row1 = 3;
		else if (mpegVersion == 2 && (layer == 2 || layer == 1))
			row1 = 4;
		col1 = (int) bitrate;

		// �� �κ��� Sampling rate ���� �κ�
		if (mpegVersion == MPEG_VERSION1)
			row2 = 3;
		else if (mpegVersion == MPEG_VERSION2)
			row2 = 2;
		else if (mpegVersion == MPEG_VERSION2_5)
			row2 = 0;

		if (samplingRate == 0)
			col2 = 0;
		if (samplingRate == 1)
			col2 = 1;
		if (samplingRate == 2)
			col2 = 2;

		/**
		 * Raw Data ũ�� ��� ��� The size of the sample data is calculated like this
		 * (using integer arithmetic): Size = (((MpegVersion == MPEG1 ? 144 :
		 * 72) * Bitrate) / SamplingRate) + PaddingBit - 4 For example: The size
		 * of the sample data for an MPEG1 frame with a Bitrate of 128000, a
		 * SamplingRate of 44100, and PaddingBit of 1 is: Size = (144 * 128000)
		 * / 44100 + 1 ? 4 = 414 bytes
		 */
		rawDataSize = (int) ((((mpegVersion == MPEG_VERSION1 ? 144 : 72) * BITRATES[row1][col1] * 1000) / SAMPLERATES[row2][col2]) + paddingBit - 4);

		return rawDataSize;
	}
}
