package com.dignara.lib.media;

import com.dignara.lib.utils.ShuffleUtils;

import java.util.LinkedList;
import java.util.List;

public class PlayList {
    public static final int PLAY_MODE_NORMAL        = 0;
    public static final int PLAY_MODE_RECURSIVE_ONE = 1;
    public static final int PLAY_MODE_RECURSIVE_ALL = 2;

    public static final int PLAY_MODE_COUNT         = 3;

    private PlayListListener mPlayListListener;
    private List<PlayFile>   mPlayFileList;
    private int              mPlayIndex;
    private int              mPlayMode;
    private int[]            mIndexList;
    private boolean          mIsShuffle;

    public interface PlayListListener {
        void onChangePlayMode(int playMode);
        void onChangePlayShuffle(boolean isShuffle);
        void onChangePlayFile(PlayFile[] playFiles);
    }

    public PlayList(PlayListListener playListListener) {
        mPlayFileList     = new LinkedList<>();
        mPlayListListener = playListListener;
    }

    public void setPlayList(List<PlayFile> playFileList) {
        mPlayFileList = playFileList;
        setShuffle(mIsShuffle);
    }

    public void setNextPlayMode() {
        mPlayMode = ShuffleUtils.getNextIndex(mPlayMode, PLAY_MODE_COUNT, true);
        mPlayListListener.onChangePlayMode(mPlayMode);
        PlayFile prevPlayFile = getPrevPlayFile();
        PlayFile playFile     = getPlayFile();
        PlayFile nextPlayFile = getNextPlayFile();
        if (playFile != null) {
            mPlayListListener.onChangePlayFile(new PlayFile[] {prevPlayFile, playFile, nextPlayFile});
        }
    }

    public void clearList() {
        mPlayFileList.clear();
        mPlayIndex = 0;
        mIndexList = null;
    }

    public int getCount() {
        return mPlayFileList.size();
    }

    public PlayFile getPrevPlayFile() {
        int prevPlayIndex = getPrevPlayIndex();
        if (mPlayFileList.size() < 1 || prevPlayIndex < 0) {
            return null;
        }
        return mPlayFileList.get(mIndexList[prevPlayIndex]);
    }

    public PlayFile getNextPlayFile() {
        int nextPlayIndex = getNextPlayIndex();
        if (mPlayFileList.size() < 1 || nextPlayIndex < 0) {
            return null;
        }
        return mPlayFileList.get(mIndexList[nextPlayIndex]);
    }

    public void toggleShuffle() {
        setShuffle(!mIsShuffle);
    }

    public void setShuffle(boolean isShuffle) {
        if (isShuffle) {
            mIndexList = ShuffleUtils.shuffle(mPlayIndex, mPlayFileList.size());
        } else {
            mIndexList = new int[mPlayFileList.size()];
            for (int i = 0; i < mPlayFileList.size(); i++) {
                mIndexList[i] = i;
            }
        }
        mIsShuffle = isShuffle;
        mPlayListListener.onChangePlayShuffle(mIsShuffle);
        mPlayListListener.onChangePlayFile(new PlayFile[] {getPrevPlayFile(), getPlayFile(), getNextPlayFile()});
    }

    public PlayFile getPlayFile() {
        if (mPlayFileList.size() < 1) {
            return null;
        }
        return mPlayFileList.get(mIndexList[mPlayIndex]);
    }

    public PlayFile setNextPlayFile() {
        if (setNextPlayIndex()) {
            mPlayListListener.onChangePlayFile(new PlayFile[] {getPrevPlayFile(), getPlayFile(), getNextPlayFile()});
            return getPlayFile();
        }
        return null;
    }

    private boolean setNextPlayIndex() {
        int nextPlayIndex = getNextPlayIndex();
        if (nextPlayIndex < 0) {
            return false;
        }
        mPlayIndex = nextPlayIndex;
        return true;
    }

    private boolean setPrevPlayIndex() {
        int prevPlayIndex = getPrevPlayIndex();
        if (prevPlayIndex < 0) {
            return false;
        }
        mPlayIndex = prevPlayIndex;
        return true;
    }

    private int getIndexNumber(int index) {
        for (int i = 0; i < mIndexList.length; i++) {
            if (mIndexList[i] == index) {
                return i;
            }
        }
        return ShuffleUtils.getSafeIndex(index, mPlayFileList.size());
    }

    public PlayFile setPlayFile(int index) {
        mPlayIndex = getIndexNumber(index);
        mPlayListListener.onChangePlayFile(new PlayFile[] {getPrevPlayFile(), getPlayFile(), getNextPlayFile()});
        return getPlayFile();
    }

    public PlayFile setPrevPlayFile() {
        if (setPrevPlayIndex()) {
            mPlayListListener.onChangePlayFile(new PlayFile[] {getPrevPlayFile(), getPlayFile(), getNextPlayFile()});
            return getPlayFile();
        }
        return null;
    }

    public void addPlayFile(PlayFile playFile) {
        addPlayFile(mPlayFileList.size(), playFile);
        setShuffle(mIsShuffle);
    }

    public void addPlayFile(int index, PlayFile playFile) {
        index = ShuffleUtils.getSafeIndex(index, mPlayFileList.size() + 1);
        mPlayFileList.add(index, playFile);
        setShuffle(mIsShuffle);
    }

    public int getPlayIndex() {
        return mPlayIndex;
    }

    private int getNextPlayIndex() {
        int nextIndex = mPlayIndex;
        if (mIndexList == null) {
            return nextIndex;
        }
        switch (mPlayMode) {
            case PLAY_MODE_NORMAL:
                nextIndex = ShuffleUtils.getNextIndex(mPlayIndex, mIndexList.length, false);
                break;
            case PLAY_MODE_RECURSIVE_ALL:
                nextIndex = ShuffleUtils.getNextIndex(mPlayIndex, mIndexList.length, true);
                break;
        }
        return nextIndex;
    }

    private int getPrevPlayIndex() {
        int prevIndex = mPlayIndex;
        if (mIndexList == null) {
            return prevIndex;
        }
        switch (mPlayMode) {
            case PLAY_MODE_NORMAL:
                prevIndex = ShuffleUtils.getPrevIndex(mPlayIndex, mIndexList.length, false);
                break;
            case PLAY_MODE_RECURSIVE_ALL:
                prevIndex = ShuffleUtils.getPrevIndex(mPlayIndex, mIndexList.length, true);
                break;
        }
        return prevIndex;
    }
}
