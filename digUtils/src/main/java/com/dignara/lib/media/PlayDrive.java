package com.dignara.lib.media;

import java.io.File;

public class PlayDrive extends PlayFolder {
    private boolean readonly;
    private boolean removable;
    private int     number;

    public PlayDrive(File drive, boolean readonly, boolean removable, int number, String internalName, String externalName) {
        super(drive, null);
        this.name      = getName(internalName, externalName);
        this.readonly  = readonly;
        this.removable = removable;
        this.number    = number;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public boolean isRemovable() {
        return removable;
    }

    public int getNumber() {
        return number;
    }

    private String getName(String internalName, String externalName) {
        StringBuilder res = new StringBuilder();
        if (!this.removable) {
            res.append(internalName);
        } else if (this.number > 1) {
            res.append(externalName);
            res.append(" " + this.number);
        } else {
            res.append(externalName);
        }
        if (this.readonly) {
            res.append("(R)");
        }
        return res.toString();
    }
}
