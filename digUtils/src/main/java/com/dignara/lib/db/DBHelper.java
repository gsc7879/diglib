package com.dignara.lib.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;

public class DBHelper extends SQLiteOpenHelper {
	private Context mContext;
	
	private int 	mCreateQueryResId;
	private int 	mUpgradeQueryResId;
	
	public DBHelper(Context context, int createQueryResId, int upgradeQueryResId, String dbName, CursorFactory cursorFactory, int version) {
		super(context, dbName, cursorFactory, version);
		mContext 		   = context;
		mCreateQueryResId  = createQueryResId;
		mUpgradeQueryResId = upgradeQueryResId;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String[] sql = mContext.getString(mCreateQueryResId).split("\n");
		db.beginTransaction();
		try {
			execMultipleSQL(db, sql);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

    private void execMultipleSQL(SQLiteDatabase db, String[] sql) {
        for (String query : sql) {
            if (query.trim().length() > 0) {
                db.execSQL(query);
            }
        }
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String[] sql = mContext.getString(mUpgradeQueryResId).split("\n");
		db.beginTransaction();
		try {
			execMultipleSQL(db, sql);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		onCreate(db);
	}
	
	public void execSQL(int sqlResId, Object... args) throws SQLException {
		String sql = String.format(mContext.getString(sqlResId), args);
		writeSQL(sql);
	}
	
	public Cursor selectDb(int sqlResId, Object... args) {
		String sql = String.format(mContext.getString(sqlResId), args);
		Cursor cursor = getReadableDatabase().rawQueryWithFactory(new CursorFactory() {
			@Override
			public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery, String editTable, SQLiteQuery query) {
				return new SQLiteCursor(db, masterQuery, editTable, query);
			}
		}, sql, null, null);
		cursor.moveToFirst();
		return cursor;
	}
	
	private void writeSQL(String sql) throws SQLException {
		try {
			getWritableDatabase().execSQL(sql);
		} catch (SQLException e) {
			throw e;
		} finally {
			this.close();
		}
	}
}
