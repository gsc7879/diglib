package com.dignara.lib.db;

import android.content.Context;
import com.dignara.lib.task.DBWorkTask;

public class DBManager {
    protected Context mContext;

    private int      mCreateDbResId;
    private int      mDropDbResId;
    private int      mDBVersion;

    private String   mDBName;

    public DBManager(Context context, int createDbResId, int dropDbResId, String dbName, int dbVersion) {
        mContext       = context;
        mCreateDbResId = createDbResId;
        mDropDbResId   = dropDbResId;
        mDBName        = dbName;
        mDBVersion     = dbVersion;
    }

    protected DBHelper getDBHelper() {
        return new DBHelper(mContext, mCreateDbResId, mDropDbResId, mDBName, null, mDBVersion);
    }

	protected void execute(DBWorkTask dbTask, Object... object) {
        dbTask.execute(object);
	}
}
