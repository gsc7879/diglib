package com.dignara.lib.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ToastManager {
    private static ToastManager uniqueToastManager;

    private Toast    mToast;
    private View     mLayout;
    private TextView mTextView;

    public synchronized static ToastManager getInstance() {
        if (uniqueToastManager == null) {
            uniqueToastManager = new ToastManager();
        }
        return uniqueToastManager;
    }

    private ToastManager() {}

    public void setLayout(View layout, int textViewResId) {
        mLayout   = layout;
        mTextView = (TextView) layout.findViewById(textViewResId);
    }

    private void show(Context context, int duration) {
        mToast = new Toast(context);
        mToast.setView(mLayout);
        mToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, UnitUtils.convertDpToPixel(context, 20f));
        mToast.setDuration(duration);
        mToast.show();
    }

    public void show(Context context, int textResId, int duration) {
        if (context == null) {
            return;
        }

        if (mLayout == null || mTextView == null) {
            Toast.makeText(context, textResId, duration).show();
        } else {
            mTextView.setText(textResId);
            show(context, duration);
        }
    }

    public void show(Context context, CharSequence text, int duration) {
        if (context == null) {
            return;
        }

        if (mLayout == null || mTextView == null) {
            Toast.makeText(context, text, duration).show();
        } else {
            mTextView.setText(text);
            show(context, duration);
        }
    }
}