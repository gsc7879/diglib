package com.dignara.lib.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.util.Log;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.*;
import java.util.*;

public class StorageUtils {
    private static String internalName;
    private static String externalName;

    public static void setName(String internalName, String externalName) {
        StorageUtils.internalName = internalName;
        StorageUtils.externalName = externalName;
    }

    public static File uniqueFile(String filePath, String type) {
        File newFile = new File(filePath);
        while (newFile.exists()) {
            filePath = increment(newFile);
            newFile = new File(filePath);
        }
        return newFile;
    }

    private static String increment(File newFile) {
        String filePath = newFile.getPath();
        int index = -1;
        for (int i = filePath.length() - 1; i >= 0; i--) {
            if (!NumberUtils.isNumber(filePath.substring(i))) {
                index = i + 1;
                break;
            }
        }
        String incrementedId;
        if (index < 0) {
            incrementedId = incrementNumber(filePath);
        } else if (index == filePath.length()) {
            incrementedId = filePath + "2";
        } else {
            incrementedId = filePath.substring(0, index) + incrementNumber(filePath.substring(index));
        }
        return incrementedId;
    }

    private static String incrementNumber(final String pString) {
        String num;
        try {
            int id = Integer.parseInt(pString);
            num = String.valueOf(id + 1);
        } catch (Exception e) {
            double id = Double.parseDouble(pString);
            num = String.valueOf(id + 1);
        }
        return num;
    }

    public static void saveBitmapFile(Bitmap bitmap, String downloadPath) {
        if (bitmap != null) {
            try {
                bitmap.compress(CompressFormat.JPEG, 100, new FileOutputStream(downloadPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static String[] getMediaList(File path) {
        Collection<File> mediaFiles = FileUtils.listFiles(path, new String[]{"mp3", "3gp", "wma", "wma", "wav", "flac"}, false);
        Object[] mediaList = IteratorUtils.toList(mediaFiles.iterator()).toArray();
        Arrays.sort(mediaList);
        String[] returnList = path.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String fileName) {
                File files = new File(dir.getAbsolutePath() + File.separator + fileName);
                if (files.isFile() && (fileName.toLowerCase().endsWith("mp3") ||
                        fileName.toLowerCase().endsWith("3gp") ||
                        fileName.toLowerCase().endsWith("wma") ||
                        fileName.toLowerCase().endsWith("wav"))) {
                    return true;
                } else {
                    return false;
                }
            }
        });
        if (returnList != null) {
            Arrays.sort(returnList);
        }
        return returnList;
    }

    public static String byteCountToDisplaySize(long size) {
        String displaySize;
        if (size / FileUtils.ONE_GB > 0) {
            displaySize = String.valueOf(size / FileUtils.ONE_GB) + " GB";
        } else if (size / FileUtils.ONE_MB > 0) {
            displaySize = String.valueOf(size / FileUtils.ONE_MB) + " MB";
        } else if (size / FileUtils.ONE_KB > 0) {
            displaySize = String.valueOf(size / FileUtils.ONE_KB) + " KB";
        } else {
            displaySize = String.valueOf(size) + " bytes";
        }
        return displaySize;
    }

    public static class StorageInfo {
        public final String path;
        public final boolean readonly;
        public final boolean removable;
        public final int number;

        StorageInfo(String path, boolean readonly, boolean removable, int number) {
            this.path = path;
            this.readonly = readonly;
            this.removable = removable;
            this.number = number;
        }

        public String getDisplayName() {
            StringBuilder res = new StringBuilder();
            if (!removable) {
                res.append(internalName);
            } else if (number > 1) {
                res.append(String.format(externalName, number));
            } else {
                res.append(StringUtils.remove(externalName, "%d").trim());
            }
            if (readonly) {
                res.append(" (Read only)");
            }
            return res.toString();
        }
    }

    public static boolean isRootFolder(File folder) {
        return isRootFolder(getStorageList(), folder);
    }

    public static boolean isRootFolder(List<StorageInfo> storageList, File folder) {
        if (!folder.exists()) {
            return true;
        }
        for (StorageUtils.StorageInfo storageInfo : storageList) {
            if (StringUtils.contains(FileUtils.getFile(storageInfo.path).getParent(), folder.getPath())) {
                return true;
            }
        }
        return false;
    }

    public static List<StorageInfo> getStorageList() {
        List<StorageInfo> list = new ArrayList<>();
        String def_path = Environment.getExternalStorageDirectory().getPath();
        boolean def_path_removable = Environment.isExternalStorageRemovable();
        String def_path_state = Environment.getExternalStorageState();
        boolean def_path_available = def_path_state.equals(Environment.MEDIA_MOUNTED) || def_path_state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        boolean def_path_readonly = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);

        HashSet<String> paths = new HashSet<>();
        int cur_removable_number = 1;

        if (def_path_available) {
            paths.add(def_path);
            list.add(0, new StorageInfo(def_path, def_path_readonly, def_path_removable, def_path_removable ? cur_removable_number++ : -1));
        }

        BufferedReader buf_reader = null;
        try {
            buf_reader = new BufferedReader(new FileReader("/proc/mounts"));
            String line;
            while ((line = buf_reader.readLine()) != null) {
                if (line.contains("vfat") || line.contains("/mnt")) {
                    StringTokenizer tokens = new StringTokenizer(line, " ");
                    String unused = tokens.nextToken(); //device
                    String mount_point = tokens.nextToken(); //mount point
                    if (paths.contains(mount_point)) {
                        continue;
                    }
                    unused = tokens.nextToken(); //file system
                    List<String> flags = Arrays.asList(tokens.nextToken().split(",")); //flags
                    boolean readonly = flags.contains("ro");

                    if (line.contains("/dev/block/vold")) {
                        if (!line.contains("/mnt/secure")
                                && !line.contains("/mnt/asec")
                                && !line.contains("/mnt/obb")
                                && !line.contains("/dev/mapper")
                                && !line.contains("tmpfs")) {

                            mount_point = StringUtils.replace(mount_point, "/mnt/media_rw/", "/storage/");
                            if (!FileUtils.getFile(mount_point).exists()) {
                                continue;
                            }
                            paths.add(mount_point);
                            list.add(new StorageInfo(mount_point, readonly, true, cur_removable_number++));
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (buf_reader != null) {
                try {
                    buf_reader.close();
                } catch (IOException ex) {
                }
            }
        }
        return list;
    }
}
