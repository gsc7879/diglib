package com.dignara.lib.utils;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;

/**
 * 뷰를 여러가지 형태로 조작하는 컨트롤러.
 * 
 * @author Seong-Cheol Gwon (seongcheol.gwon@konantech.com)
 */
public class ViewUtils {	
	/**
	 * 폭을 기준으로 이미지의 비율에 맞춰 뷰의 사이즈를 변경한다.
	 * @param imageView 	사이즈를 변경 할 이미지 뷰
	 * @param imageWidth 	이미지의 폭
	 * @param imageHeight 	이미지의 높이
	 * @param resizeWidth 	변경할 폭
	 */
	public static void resizeByWidth(ImageView imageView, int imageWidth, int imageHeight, int resizeWidth) {
		int resizeHeight = (int) ((float) imageHeight * ((float) resizeWidth / (float) imageWidth));
		LayoutParams lp  = imageView.getLayoutParams();
		lp.width 		 = resizeWidth;
		lp.height 		 = resizeHeight;
		imageView.setLayoutParams(lp);
	}
	
	/**
	 * 높이를 기준으로 이미지의 비율에 맞춰 뷰의 사이즈를 변경한다.
	 * @param imageView 	사이즈를 변경 할 이미지 뷰
	 * @param imageWidth 	이미지의 폭
	 * @param imageHeight 	이미지의 높이
	 * @param resizeHeight 	변경할 높이
	 */
	public static void resizeByHeight(ImageView imageView, int imageWidth, int imageHeight, int resizeHeight) {
		int resizeWidth = (int) ((float) imageWidth * ((float) resizeHeight / (float) imageHeight));
		LayoutParams lp = imageView.getLayoutParams();
		lp.width 		= resizeWidth;
		lp.height 		= resizeHeight;
		imageView.setLayoutParams(lp);
	}
	
	/**
	 * 뷰의 사이즈를 변경한다.
	 * @param view			사이즈를 변경 할 뷰
	 * @param resizeWidth	변경할 폭
	 * @param resizeHeight	변경할 높이
	 */
	public static void resizeView(View view, int resizeWidth, int resizeHeight) {
		LayoutParams lp = view.getLayoutParams();
		lp.width 		= resizeWidth;
		lp.height 		= resizeHeight;
		view.setLayoutParams(lp);
	}
	
	/**
	 * 뷰의 마진값을 변경한다.
	 * @param view			마진값을 변경 할 뷰
	 * @param leftMargin	왼쪽 마진
	 * @param topMargin		상단 마진
	 * @param rightMargin	오른쪽 마진
	 * @param bottomMargin	하단 마진
	 */
	public static void marginView(View view, int leftMargin, int topMargin, int rightMargin, int bottomMargin) {
		LayoutParams layout 	  	   = view.getLayoutParams();
		MarginLayoutParams marginParam = (MarginLayoutParams) layout;
		marginParam.leftMargin 		   = leftMargin;
		marginParam.topMargin 		   = topMargin;
		marginParam.rightMargin 	   = rightMargin;
		marginParam.bottomMargin 	   = bottomMargin;
		view.setLayoutParams(marginParam);
	}
	
	public static void toViewRawXY(View view) {
		View parentView = view.getRootView();
		int sumX = 0;
		int sumY = 0;

		boolean chk = false;
		while (!chk) {
			sumX = sumX + view.getLeft();
			sumY = sumY + view.getTop();

			view = (View) view.getParent();
			if (parentView == view) {
				chk = true;
			}
		}
	}
}
