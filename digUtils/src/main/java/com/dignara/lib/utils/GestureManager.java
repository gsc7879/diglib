package com.dignara.lib.utils;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class GestureManager implements GestureDetector.OnGestureListener {
    private GestureDetector mDetector;
    private OnFlingListener mFlingListener;
    private int             mSwipeMinDistance;
    private int             mSwipeMinVelocity;

    public interface OnFlingListener {
        public void onFlingLeft();
        public void onFlingRight();
    }

    public GestureManager(View eventView, OnFlingListener flingListener) {
        mDetector      = new GestureDetector(this);
        mFlingListener = flingListener;
        setSensitivity(eventView.getContext());
        eventView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return mDetector.onTouchEvent(motionEvent);
            }
        });
    }

    private void setSensitivity(Context context) {
        ViewConfiguration vc  = ViewConfiguration.get(context);
        mSwipeMinDistance     = vc.getScaledTouchSlop();
        mSwipeMinVelocity     = vc.getScaledMinimumFlingVelocity();
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override
    public boolean onFling(MotionEvent start, MotionEvent finish, float xVelocity, float yVelocity) {
        if (start == null || finish == null) {
            return false;
        }
        if (Math.abs(start.getRawX() - finish.getRawX()) > mSwipeMinDistance && Math.abs(xVelocity) > mSwipeMinVelocity) {
            if (start.getRawY() < finish.getRawY()) {
                mFlingListener.onFlingRight();
            } else {
                mFlingListener.onFlingLeft();
            }
            return true;
        }
        return false;
    }
}
