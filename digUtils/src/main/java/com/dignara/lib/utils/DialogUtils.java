package com.dignara.lib.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class DialogUtils {
    private static AlertDialog mAlertDialog;
    private static AlertDialog mAlertDialogTemp;

    public interface OnDialogEtcSet {
        public void onDialogBuilder(AlertDialog.Builder dialog);
    }

    public static final void convDialog(Context context, int msgResId, int positiveButtonTextResId, int NegativeButtonTextResId,  DialogInterface.OnClickListener positiveOnClickListener, DialogInterface.OnClickListener negativeOnClickListener, int iconResId, int titleResId) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(msgResId).setCancelable(false).setPositiveButton(context.getText(positiveButtonTextResId), positiveOnClickListener).setNegativeButton(context.getText(NegativeButtonTextResId), negativeOnClickListener);
        dialog.setCancelable(true);
        dialog.setIcon(iconResId);
        dialog.setTitle(titleResId);
        showDialog(dialog);
    }

    public static final void convDialog(Context context, String msg, String positiveButtonText, String NegativeButtonText, DialogInterface.OnClickListener positiveOnClickListener, DialogInterface.OnClickListener negativeOnClickListener, int iconResId, String title) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(msg).setCancelable(false).setPositiveButton(positiveButtonText, positiveOnClickListener).setNegativeButton(NegativeButtonText, negativeOnClickListener);
        dialog.setCancelable(true);
        dialog.setIcon(iconResId);
        dialog.setTitle(title);
        showDialog(dialog);
    }

    public static final void listDialog(Context context, int titleResId, ArrayList<String> arrayList, DialogInterface.OnClickListener onClickListener) {
        listDialog(context, titleResId, arrayList, onClickListener, null);
    }

    public static final void listDialog(Context context, int titleResId, ArrayList<String> arrayList, DialogInterface.OnClickListener onClickListener, OnDialogEtcSet onDialogEtcSet) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setAdapter(new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, arrayList), onClickListener);
        dialog.setCancelable(true);
        dialog.setTitle(titleResId);
        if (onDialogEtcSet != null) {
            onDialogEtcSet.onDialogBuilder(dialog);
        }
        showDialog(dialog);
    }

    public static final void listDialog(Context context, String title, ArrayList<String> arrayList, DialogInterface.OnClickListener onClickListener) {
        listDialog(context, title, arrayList, onClickListener, null);
    }

    public static final void listDialog(Context context, String title, ArrayList<String> arrayList, DialogInterface.OnClickListener onClickListener, OnDialogEtcSet onDialogEtcSet) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setAdapter(new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, arrayList), onClickListener);
        dialog.setCancelable(true);
        dialog.setTitle(title);
        if (onDialogEtcSet != null) {
            onDialogEtcSet.onDialogBuilder(dialog);
        }
        showDialog(dialog);
    }

    public static final void customDialog(Context context, View customView, int iconResId, int titleResId, int cancelTextResId, String btnTitle, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LinearLayout dialogLayout = new LinearLayout(context);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        dialogLayout.addView(customView);
        dialog.setView(dialogLayout);
        dialog.setIcon(iconResId);
        dialog.setTitle(titleResId);
        dialog.setCancelable(true);
        dialog.setPositiveButton(btnTitle, onClickListener);
        dialog.setNegativeButton(context.getText(cancelTextResId), null);
        showDialog(dialog);
    }

    public static final void customDialog(Context context, View[] view, int titleResId, int btnTitleResId, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        LinearLayout dialogLayout = new LinearLayout(context);
        dialogLayout.setOrientation(LinearLayout.VERTICAL);
        for (int i = 0; i < view.length; i++) {
            dialogLayout.addView(view[i]);
        }
        dialog.setView(dialogLayout);
        dialog.setTitle(titleResId);
        dialog.setPositiveButton(context.getText(btnTitleResId), onClickListener);
        showDialog(dialog);
    }

    public static final void setDialogMessage(String msg) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.setMessage(msg);
        }
    }

    public static final void setProgress(int progress, int max) {
        if (mAlertDialog != null && mAlertDialog instanceof ProgressDialog) {
            ((ProgressDialog) mAlertDialog).setMax(max);
            ((ProgressDialog) mAlertDialog).setProgress(progress);

        }
    }

    public static final void progressDialog(Context context, int msgResId, int dialogStyle) {
        progressDialog(context, -1, msgResId, dialogStyle);
    }

    public static final void progressDialog(Context context, String msg, int dialogStyle) {
        progressDialog(context, null, msg, dialogStyle);
    }

    public static final void progressDialog(Context context, int titleResId, int msgResId, int dialogStyle) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setProgressStyle(dialogStyle);
        if (titleResId > 0) {
            dialog.setTitle(context.getText(titleResId));
        }
        dialog.setMessage(context.getText(msgResId));
        showDialog(dialog);
    }

    public static final void progressDialog(Context context, String title, String msg, int dialogStyle) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setProgressStyle(dialogStyle);
        if (title != null) {
            dialog.setTitle(title);
        }
        dialog.setMessage(msg);
        showDialog(dialog);
    }

    public static void alertDialog(Context context, int msgResId, int btnTextResId) {
        alertDialog(context, msgResId, btnTextResId, null);
    }

    public static void alertDialog(Context context, int msgResId, int btnTextResId, DialogInterface.OnClickListener positiveOnClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(msgResId);
        dialog.setPositiveButton(btnTextResId, positiveOnClickListener);
        showDialog(dialog);
    }

    public static void alertDialog(Context context, String msg, String btnText) {
        alertDialog(context, msg, btnText, null);
    }

    public static void alertDialog(Context context, String msg, String btnText, DialogInterface.OnClickListener positiveOnClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(msg);
        dialog.setPositiveButton(btnText, positiveOnClickListener);
        showDialog(dialog);
    }

    public static boolean closeDialog() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
            return true;
        }
        return false;
    }

    public static boolean isShowDialog() {
        return mAlertDialog == null ? false : true;
    }

    private static void showDialog(AlertDialog.Builder dialog) {
        AlertDialog alertDialog = dialog.create();
        showDialog(alertDialog);
    }

    private static void showDialog(AlertDialog alertDialog) {
        if (closeDialog()) {
            mAlertDialogTemp = alertDialog;
            return;
        }
        mAlertDialog = alertDialog;
        mAlertDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mAlertDialog = null;
                if (mAlertDialogTemp != null) {
                    showDialog(mAlertDialogTemp);
                    mAlertDialogTemp = null;
                }
            }
        });

        mAlertDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mAlertDialog = null;
                if (mAlertDialogTemp != null) {
                    showDialog(mAlertDialogTemp);
                    mAlertDialogTemp = null;
                }
            }
        });
        mAlertDialog.show();
        mAlertDialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
