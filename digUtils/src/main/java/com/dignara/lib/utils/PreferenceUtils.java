package com.dignara.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.Map;

public class PreferenceUtils {
    private static String NAME = "preferences";

    public static String getEncryptedPreference(Context context, String name, String defaultString) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        String encryptString = SecurityUtils.decrypt(preferences.getString(name, ""), DeviceInfo.getDeviceId(context));
        return StringUtils.isBlank(encryptString) ? defaultString : encryptString;
    }

    public static void putEncryptedPreference(Context context, String name, String value) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(name, SecurityUtils.encrypt(value, DeviceInfo.getDeviceId(context)));
        editor.apply();
    }

    public static SharedPreferences getPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        return preferences;
    }

    public static void putPreference(Context context, String name, String value) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void putPreference(Context context, String name, int value) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(name, value);
        editor.apply();
    }

    public static void putPreference(Context context, String name, long value) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(name, value);
        editor.apply();
    }

    public static void putPreference(Context context, String name, boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    public static void removePreference(Context context, String name) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(name);
        editor.apply();
    }

    public static void clearPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void putPreferences(Context context, Map<String, Object> preferenceMap) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Iterator it = preferenceMap.keySet().iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            Object value = preferenceMap.get(it.next());
            if (value instanceof String) {
                editor.putString(name, (String) value);
            } else if (value instanceof Boolean) {
                editor.putBoolean(name, (Boolean) value);
            } else if (value instanceof Float) {
                editor.putFloat(name, (Float) value);
            } else if (value instanceof Integer) {
                editor.putInt(name, (Integer) value);
            } else if (value instanceof Long) {
                editor.putLong(name, (Long) value);
            }
        }
        editor.apply();
    }
}
