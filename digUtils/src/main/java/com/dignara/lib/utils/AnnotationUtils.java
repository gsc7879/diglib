package com.dignara.lib.utils;

import android.app.Activity;
import android.view.View;
import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AnnotationUtils {
    public static void setInjectView(View parentView, Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            InjectView injectView = field.getAnnotation(InjectView.class);
            if (injectView == null) {
                continue;
            }
            int[] ids = injectView.ids();
            Object viewObject = ids[0] > -1 ? getViews(field, ids, parentView) : getView(injectView.id(), parentView);
            if (viewObject == null) {
                continue;
            }
            try {
                field.setAccessible(true);
                field.set(object, viewObject);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static View[] getViews(Field field, int[] ids, View parentView) {
        View viewArray[] = (View[]) Array.newInstance(field.getType().getComponentType(), ids.length);
        for (int i = 0; i < viewArray.length; i++) {
            viewArray[i] = parentView.findViewById(ids[i]);
        }
        return viewArray;
    }

    private static View getView(int identifier, View parentView) {
        return parentView.findViewById(identifier);
    }

    public static void setInjectView(Activity activity, Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            InjectView injectView = field.getAnnotation(InjectView.class);
            if (injectView == null) {
                continue;
            }
            int[] ids = injectView.ids();
            Object viewObject = ids[0] > -1 ? getViews(field, ids, activity) : getView(injectView.id(), activity);
            if (viewObject == null) {
                continue;
            }
            try {
                field.setAccessible(true);
                field.set(object, viewObject);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static View[] getViews(Field field, int[] ids, Activity activity) {
        View viewArray[] = (View[]) Array.newInstance(field.getType().getComponentType(), ids.length);
        for (int i = 0; i < viewArray.length; i++) {
            viewArray[i] = activity.findViewById(ids[i]);
        }
        return viewArray;
    }

    private static View getView(int identifier, Activity activity) {
        return activity.findViewById(identifier);
    }

    public static void setClickEvent(Activity activity, final Object receiver) {
        Method[] methods = receiver.getClass().getDeclaredMethods();
        for (final Method method : methods) {
            Click click = method.getAnnotation(Click.class);
            if (click == null) {
                continue;
            }

            Class<?>[] params = method.getParameterTypes();
            final boolean hasViewParameter = (params.length > 0);

            int[] ids = click.value();
            for (int id : ids) {
                View clickView = activity.findViewById(id);
                if (clickView != null) {
                    clickView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (hasViewParameter) {
                                    method.invoke(receiver, view);
                                } else {
                                    method.invoke(receiver);
                                }
                            } catch (IllegalAccessException e) {
                                throw new RuntimeException(e);
                            } catch (InvocationTargetException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            }
        }
    }

    public static void setClickEvent(View parentView, final Object receiver) {
        Method[] methods = receiver.getClass().getDeclaredMethods();
        for (final Method method : methods) {
            Click click = method.getAnnotation(Click.class);
            if (click == null) {
                continue;
            }

            Class<?>[] params = method.getParameterTypes();
            final boolean hasViewParameter = (params.length > 0);

            int[] ids = click.value();
            for (int id : ids) {
                View clickView = parentView.findViewById(id);
                if (clickView != null) {
                    clickView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (hasViewParameter) {
                                    method.invoke(receiver, view);
                                } else {
                                    method.invoke(receiver);
                                }
                            } catch (IllegalAccessException e) {
                                throw new RuntimeException(e);
                            } catch (InvocationTargetException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            }
        }
    }
}
