package com.dignara.lib.utils;

import java.util.Calendar;

public class DateUtils {
    /**
     * 현재 설정되어 있는 요일을 한글 형식으로 반환 한다.
     * @return String
     */
    public static String getDayStringOfWeek(Calendar calendar) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return "일";
            case 2:
                return "월";
            case 3:
                return "화";
            case 4:
                return "수";
            case 5:
                return "목";
            case 6:
                return "금";
            case 7:
                return "토";
        }
        return "";
    }

    /**
     * 현재 설정되어 있는 달을 영문 형식으로 반환 한다.
     * @return String
     */
    public String getMonthString(Calendar calendar) {
        switch (calendar.get(Calendar.MONTH) + 1) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
        }
        return null;
    }
}
