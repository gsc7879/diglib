package com.dignara.lib.utils;

import java.util.Random;

public class ShuffleUtils {
    public static int[] shuffle(int startCount, int count) {
        if (count < 1) {
            return new int[0];
        }
        int shuffleCount[] = shuffle(count);
        if (shuffleCount.length < startCount) {
            startCount = 0;
        }
        int index = shuffleCount[startCount];

        for (int i = 0; i < shuffleCount.length; i++) {
            if (shuffleCount[i] == startCount) {
                shuffleCount[i] = index;
                break;
            }
        }
        shuffleCount[startCount] = startCount;
        return shuffleCount;
    }

    public static int[] shuffle(int count) {
        int var[] = new int[count];
        int idx[] = new int[count];
        int tmp;

        Random r = new Random();
        for (int i = 0; i < count; i++) {
            var[i] = i;
            idx[i] = r.nextInt(count);
        }

        for (int i = 0; i < count; i++) {
            tmp = var[i];
            var[i] = var[idx[i]];
            var[idx[i]] = tmp;
        }
        return var;
    }

    public static int getNextIndex(int curIndex, int size, boolean isRecursive) {
        if (curIndex < size - 1) {
            return curIndex + 1;
        } else {
            if (isRecursive) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    public static int getPrevIndex(int curIndex, int size, boolean isRecursive) {
        if (curIndex > 0) {
            return curIndex - 1;
        } else {
            if (isRecursive) {
                return size - 1;
            } else {
                return -1;
            }
        }
    }

    public static int getSafeIndex(int index, int size) {
        if (index < 0) {
            return 0;
        } else if (index >= size - 1) {
            return size - 1;
        } else {
            return index;
        }
    }
}
