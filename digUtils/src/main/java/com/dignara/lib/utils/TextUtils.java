package com.dignara.lib.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {
    static final Pattern SCRIPTS     = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>", Pattern.DOTALL);
    static final Pattern STYLE       = Pattern.compile("<style[^>]*>.*</style>", Pattern.DOTALL);
    static final Pattern TAGS        = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");
    static final Pattern nTAGS       = Pattern.compile("<\\w+\\s+[^<]*\\s*>");
    static final Pattern ENTITY_REFS = Pattern.compile("&[^;]+;");
    static final Pattern WHITESPACE  = Pattern.compile("\\s\\s+");

    public static String safeFileName(String fileName) {
        return fileName.replaceAll("[^a-zA-Z0-9\\._]+", "_");
    }

    //특수문자 제거 하기
    public static String stringReplace(String str) {
        String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
        str = str.replaceAll(match, " ");
        return str;
    }

    //이메일 유효성
    public static boolean isEmailPattern(String email) {
        Pattern pattern = Pattern.compile("\\w+[@]\\w+\\.\\w+");
        Matcher match = pattern.matcher(email);
        return match.find();
    }

    //연속 스페이스 제거
    public static String continueSpaceRemove(String str) {
        String match2 = "\\s{2,}";
        str = str.replaceAll(match2, " ");
        return str;
    }

    /**
     * 용량 형식으로 변환
     * @param bytes	변환하고자 하는 용량
     * @return String
     */
    public static String toNumInUnits(long bytes) {
        int u = 0;
        for (;bytes > 1024*1024; bytes >>= 10) {
            u++;
        }
        if (bytes > 1024)
            u++;
        return String.format("%.1f %cB", bytes/1024f, " KMGTPE".charAt(u));
    }

	/**
	 * 금액 형식으로 변환
	 * @param price	변환하고자 하는 가격
	 * @return String
	 */
	public static String priceFormat(long price) {
		DecimalFormat form = new DecimalFormat("#,###,###");
		return form.format(price);
	}
	
	/**
     * 금액 형식으로 변환
     * @param price	변환하고자 하는 가격
     * @return String
     */
    public static String priceFormat(String price) {
        DecimalFormat form = new DecimalFormat("#,###,###");
        return form.format((long) Long.parseLong(price));
    }

    /**
     * 시간 형식으로 변환
     * @param millisec	변환하고자 하는 시간의 밀리초
     * @return String
     */
    public static String msToMin(long millisec) {
        String format = String.format("%%0%dd", 2);
        millisec = millisec / 1000;
        String seconds = String.format(format, millisec % 60);
        String minutes = String.format(format, (millisec % 3600) / 60);
        String time    = minutes + ":" + seconds;
        return time;
    }

    /**
     * 시간 형식으로 변환
     * @param millisec	변환하고자 하는 시간의 밀리초
     * @return String
     */
    public static String msToTime(long millisec) {
        String format = String.format("%%0%dd", 2);
        millisec = millisec / 1000;
        String seconds = String.format(format, millisec % 60);
        String minutes = String.format(format, (millisec % 3600) / 60);
        String hours   = String.format(format, millisec / 3600);
        String time    =  hours + ":" + minutes + ":" + seconds;
        return time;
    }

    /**
     * 시간 형식으로 변환
     * @param millisec	변환하고자 하는 시간의 밀리초
     * @return String
     */
    public static String[] msToMsTime(long millisec) {
        String format  = String.format("%%0%dd", 2);
        long sec       = millisec / 1000;
        String seconds = String.format(format, sec % 60);
        String minutes = String.format(format, (sec % 3600) / 60);
        String hours   = String.format(format, sec / 3600);
        String[] time  = new String[]{hours + ":" + minutes + ":" + seconds, StringUtils.substring(String.format(String.format("%%0%dd", 3), millisec % 1000), 0, 2)};
        return time;
    }
	
	/**
	 * 빈 문자열인지 여부
	 * @param checkStr	체크하고자 하는 텍스트
	 * @return
	 */
	public static boolean isNull(String checkStr) {
		if (checkStr == null || checkStr.trim().length() < 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * 숫자인지 여부
	 * @param checkStr	체크하고자 하는 텍스트
	 * @return	boolean
	 */
	public static boolean isNumber(String checkStr) {
		if (isNull(checkStr)) {
			return false;
		}
		for (int i = 0; i < checkStr.length(); i++) {
			char ch = checkStr.charAt(i);
			if (((ch >= '0' && ch <= '9') && ch != ' ')) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 한글인지 여부
	 * @param checkStr	체크하고자 하는 텍스트
	 * @return	boolean
	 */
	public static boolean isKorean(String checkStr) {
		if (isNull(checkStr)) {
			return false;
		}
		for (int i = 0; i < checkStr.length(); i++) {
			char ch = checkStr.charAt(i);
			if (!(ch >= 0xAC00 && ch <= 0xD7AF || // unicode hangul range
				  ch >= 0x3130 && ch <= 0x318F || // hangul compatibility jamo
				  ch >= 0x1100 && ch <= 0x11FF || // unicode hangul jamo
				  ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch >= '0' && ch <= '9' || ch == ' ')) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 시간형식의 텍스트를 초로 바꾸어 준다.
	 * @param time **:**:**
	 * @return int 초단위
	 */
	public static int timeToSec(String time) {
		String[] timeArray = time.split(":");
		int hour = Integer.parseInt(timeArray[0]) * 3600;
		int min  = Integer.parseInt(timeArray[1]) * 60; 
		int sec  = Integer.parseInt(timeArray[2]);
		return hour + min + sec;
	}
	
	/**
	 * 원하는 자릿수 형태로 반환한다.
	 * @param str		
	 * @param position	자릿수
	 * @return String
	 */
	public static String changePosition(String str, int position) {
		int cnt = position - str.length();
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<cnt; i++) {
			sb.append("0");
		}
		sb.append(str);
		return sb.toString();
	}
	
	public static String changePosition(int number, int position) {
		return changePosition(String.valueOf(number), position);
	}

    /**
     * 유니코드를 한글로 변환해 준다.
     * @param unicode
     * @return String
     */
    public static String convertUnicode(String unicode) {
        char[] temp = unicode.toCharArray();
        StringBuffer result = new StringBuffer(temp.length);
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] == '&' && temp.length > i + 7 && temp[i + 1] == '#' && temp[i + 7] == ';') {
                try {
                    result.append((char) Integer.parseInt(unicode.substring(i + 2, i + 7)));
                    i = i + 7;
                } catch (NumberFormatException e) {
                    result.append(temp[i]);
                }
            } else
                result.append(temp[i]);
        }
        result.trimToSize();
        return result.toString();
    }

    public static String listToString(ArrayList<?> list, String separator) {
        StringBuffer strTemp = new StringBuffer();
        for (int i=0; i<list.size(); i++) {
            strTemp.append(list.get(i));
            if (i < list.size() - 1) {
                strTemp.append(separator);
            }
        }
        return strTemp.toString();
    }

    public static File createUniqueFile(String filePath, String numFormat) {
        String extension = String.format(".%s", FilenameUtils.getExtension(filePath));
        String baseName  = FilenameUtils.getBaseName(filePath);
        File file        = org.apache.commons.io.FileUtils.getFile(filePath);
        File folder      = file.getParentFile();
        int i = 1;
        while (file.exists()) {
            file = org.apache.commons.io.FileUtils.getFile(folder, String.format(String.format("%%s%s%%s", numFormat), baseName, i++, extension));
        }
        return file;
    }

    public static ArrayList<String> stringToStringList(String list, String separator) {

        if (list == null || list.trim().equalsIgnoreCase("")) {
            return null;
        }
        String[] listArray = list.trim().split(separator);
        if (listArray == null || listArray.length < 1) {
            return null;
        }
        ArrayList<String> listTemp = new ArrayList<>();
        for (int i=0; i<listArray.length; i++) {
            listTemp.add(listArray[i]);
        }
        return listTemp;
    }

    public static ArrayList<Integer> stringToIntegerList(String list, String separator) {
        if (list == null || list.trim().equalsIgnoreCase("")) {
            return null;
        }
        String[] listArray = list.trim().split(separator);
        if (listArray == null || listArray.length < 1) {
            return null;
        }

        ArrayList<Integer> listTemp = new ArrayList<>();
        for (int i=0; i<listArray.length; i++) {
            listTemp.add(Integer.parseInt(listArray[i]));
        }
        return listTemp;
    }

    public static String removeTag(String s) {
        if (StringUtils.isBlank(s)) {
            return "";
        }
        Matcher m = SCRIPTS.matcher(s);
        s = m.replaceAll("");
        m = STYLE.matcher(s);
        s = m.replaceAll("");
        m = TAGS.matcher(s);
        s = m.replaceAll("");
        m = nTAGS.matcher(s);
        s = m.replaceAll("");
        m = ENTITY_REFS.matcher(s);
        s = m.replaceAll("");
        m = WHITESPACE.matcher(s);
        s = m.replaceAll(" ");
        return s;
    }
}
