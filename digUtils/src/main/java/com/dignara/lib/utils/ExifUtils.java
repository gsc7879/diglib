package com.dignara.lib.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.jpeg.JpegDirectory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ExifUtils {
    public static Metadata getMetadata(File file) throws ImageProcessingException, IOException {
        return ImageMetadataReader.readMetadata(file);
    }

    public static Metadata getMetadata(InputStream inputStream) throws ImageProcessingException, IOException {
        return ImageMetadataReader.readMetadata(inputStream);
    }

    public static String getGeoLocation(InputStream inputStream) throws ImageProcessingException, IOException {
        return getGeoLocation(getMetadata(inputStream));
    }

    public static String getGeoLocation(File file) throws ImageProcessingException, IOException {
        return getGeoLocation(getMetadata(file));
    }

    public static String getGeoLocation(Metadata metadata) {
        if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
            GeoLocation geoLocation = metadata.getFirstDirectoryOfType(GpsDirectory.class).getGeoLocation();
            return geoLocation == null ? "" : geoLocation.toString();
        }
        return "";
    }

    public static int[] getImageSize(File file) {
        try {
            Metadata metadata = getMetadata(file);
            JpegDirectory jpegDirectory = metadata.getFirstDirectoryOfType(JpegDirectory.class);
            if (jpegDirectory != null) {
                return new int[] {jpegDirectory.getImageWidth(), jpegDirectory.getImageHeight()};
            }
        } catch (ImageProcessingException | IOException | MetadataException ignored) {
        }
        return getImageSize(file.getPath());
    }

    private static int[] getImageSize(String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        if (bitmap == null) {
            return null;
        }
        int[] size = {bitmap.getWidth(), bitmap.getHeight()};
        bitmap.recycle();
        return size;
    }
}
