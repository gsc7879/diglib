package com.dignara.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class ImageUtils {
	public static Bitmap viewToBitmap(View view) {
	    Bitmap imageBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888); 
	    Canvas canvas = new Canvas(imageBitmap); 
	    view.draw(canvas); 
	    return imageBitmap;
	}
	
	public static Bitmap textToBitmap(Context context, String text, float fontSize, int width, int height, int color, boolean isSingleLine) {
		TextView textView = new TextView(context); 
	    LayoutParams layoutParams = new LayoutParams(width, height); 
	    textView.setLayoutParams(layoutParams);
	    textView.setTextSize(fontSize);
	    textView.setText(text); 
	    textView.setTextColor(color);
	    if (isSingleLine) {
	    	textView.setSingleLine();
	    }
	 
	    Bitmap textBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444); 
	    Canvas canvas = new Canvas(textBitmap); 
	    textView.layout(0, 0, width, height); 
	    textView.draw(canvas); 
	    return textBitmap;
	}

    public static Bitmap getBitmap(Context context, Uri uri, int maxKByte) {
        try {
            long maxByte = maxKByte * 1000;
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, options);
            inputStream.close();

            int scale = 1;
            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) > maxByte) {
                scale++;
            }
            Bitmap bitmap;
            inputStream = context.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                options = new BitmapFactory.Options();
                options.inSampleSize = scale;
                bitmap = BitmapFactory.decodeStream(inputStream, null, options);

                int height = bitmap.getHeight();
                int width = bitmap.getWidth();

                double y = Math.sqrt(maxByte / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) x, (int) y, true);
                bitmap.recycle();
                bitmap = scaledBitmap;
                System.gc();
            } else {
                bitmap = BitmapFactory.decodeStream(inputStream);
            }
            inputStream.close();
            return bitmap;
        } catch (IOException e) {
            return null;
        }
    }

    public static boolean saveBitmapToFile(Bitmap bitmap, File saveFile) {
        OutputStream outputStream = null;
        try {
            FileUtils.forceMkdir(saveFile.getParentFile());
            outputStream = new FileOutputStream(saveFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            if (saveFile.exists()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    public static String getImagePath(Activity activity, Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }
}
