package com.dignara.lib.utils;

import android.view.View;

import java.util.Hashtable;

public class ViewManager {
    private Hashtable<Integer, View>                     viewTable;
    private Hashtable<Integer, Hashtable<Integer, View>> groupViewTable;

    public ViewManager() {
        this.viewTable      = new Hashtable<>();
        this.groupViewTable = new Hashtable<>();
    }

    public View getGroupView(int groupId, View parent, int resId) {
        if (groupViewTable.contains(groupId)) {
            if (!groupViewTable.get(groupId).contains(resId)) {
                groupViewTable.get(groupId).put(resId, parent.findViewById(resId));
            }
        } else {
            groupViewTable.put(groupId, new Hashtable<Integer, View>());
            groupViewTable.get(groupId).put(resId, parent.findViewById(resId));
        }
        return groupViewTable.get(groupId).get(resId);
    }

    public View getView(View parent, int resId) {
        if (!viewTable.contains(resId)) {
            viewTable.put(resId, parent.findViewById(resId));
        }
        return viewTable.get(resId);
    }

    public void cleanup() {
        viewTable.clear();
    }

    public void cleanupGroup(int groupId) {
        groupViewTable.get(groupId).clear();
    }

    public void cleanupAll() {
        viewTable.clear();
        groupViewTable.clear();
    }
}
