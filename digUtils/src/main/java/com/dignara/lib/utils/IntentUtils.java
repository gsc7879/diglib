package com.dignara.lib.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class IntentUtils {
    public static boolean openingCamera(Fragment fragment, Uri saveUri, int requestCode) {
        if (fragment.getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, saveUri);
            fragment.startActivityForResult(intent, requestCode);
            return true;
        } else {
            return false;
        }
    }

    public static void openingGallery(Fragment fragment, int requestCode) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestCode);
    }

    public static void searchingMarket(Context context, String keyword) {
        IntentUtils.openingUrl(context, String.format("market://search?q=%s", keyword));
    }

    public static void openingMarket(Context context, String packageName) {
        IntentUtils.openingUrl(context, String.format("market://details?id=%s", packageName));
    }

    public static void openingMap(Context context, String latitude, String longitude, String label, int zoom) {
        IntentUtils.openingUrl(context, String.format("geo:%s,%s?q=%s,%s(%s)&z=%d", latitude, longitude, latitude, longitude, label, zoom));
    }

    public static void openingGoogleMap(Context context, String latitude, String longitude, String label, int zoom) {
        String uri = String.format("geo:%s,%s?q=%s,%s(%s)&z=%d", latitude, longitude, latitude, longitude, label, zoom);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        context.startActivity(intent);
    }

    public static void openAppSettings(Activity activity, int requestCode) {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.parse(String.format("package:%s", activity.getPackageName())));
            activity.startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public static void phoneCalling(Context context, String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:%s", phoneNumber)));
        context.startActivity(callIntent);
    }

    public static void openingUrl(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);;
    }

    public static void sendingEmail(Context context, String email, String subject, String contents, File file) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, email);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, contents);

        if (file != null && file.exists()) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType(getMimeType(file));
        } else {
            intent.setType("text/html");
        }
        context.startActivity(intent);
    }

    public static void openingIntent(Context context, String intentUri, String webUrl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(intentUri));

        PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() == 0) {
            intent.setData(Uri.parse(webUrl));
        }
        context.startActivity(intent);
    }

    public static void sendingSMS(Context context, String phoneNumber, String content){
        Uri uri = Uri.parse(String.format("smsto:%s", phoneNumber));
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", content);
        context.startActivity(intent);
    }

    public static void openingFile(Context context, File file) throws FileNotFoundException, ActivityNotFoundException {
        Intent fileOpenIntent = getOpenFileIntent(file);
        try {
            context.startActivity(fileOpenIntent);
        } catch (ActivityNotFoundException e) {
            throw new ActivityNotFoundException();
        }
    }

    public static Intent getOpenFileIntent(File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException();
        }
        Intent fileOpenIntent = new Intent(Intent.ACTION_VIEW);
        fileOpenIntent.setDataAndType(Uri.fromFile(file), getMimeType(file));
        return fileOpenIntent;
    }

    public static void sharingFile(Context context, File file) throws FileNotFoundException, ActivityNotFoundException {
        Intent shareIntent = getSharingFileIntent(file);
        try {
            context.startActivity(shareIntent);
        } catch (ActivityNotFoundException e) {
            throw new ActivityNotFoundException();
        }
    }

    public static void sharing(Context context, String text) throws ActivityNotFoundException {
        Intent shareIntent = getSharingIntent(text);
        try {
            context.startActivity(shareIntent);
        } catch (ActivityNotFoundException e) {
            throw new ActivityNotFoundException();
        }
    }

    public static Intent getSharingFileIntent(File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException();
        }
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        shareIntent.setType(getMimeType(file));
        return shareIntent;
    }

    public static Intent getSharingIntent(String text) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
        return sharingIntent;
    }

    private static String getMimeType(File file) {
        String extension        = FilenameUtils.getExtension(file.getName());
        ContentInfo contentInfo = new ContentInfoUtil().findExtensionMatch(extension);
        if (contentInfo == null) {
            if (StringUtils.equalsIgnoreCase(extension, "hwp")) {
                return "application/hwp";
            } else if (StringUtils.startsWithIgnoreCase(extension, "doc")) {
                return "application/msword";
            } else if (StringUtils.startsWithIgnoreCase(extension, "xls")) {
                return "application/vnd.ms-excel";
            } else if (StringUtils.startsWithIgnoreCase(extension, "ppt")) {
                return "application/vnd.ms-powerpoint";
            } else {
                return extension;
            }
        }
        return contentInfo.getMimeType();
    }
}
