package com.dignara.lib.utils;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class CalendarUtils {
    private static final String CONTENT_URI = "content://com.android.calendar";

    private Context 	mContext;
    private Calendar 	mCalendar;

    public CalendarUtils(Context context) {
        mContext  = context;
        mCalendar = Calendar.getInstance();
    }

    /**
     * 현재 날자를 dateFormat에 맞추어 반환한다.
     * @return	String
     */
    public String getDate(String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        return format.format(mCalendar.getTime());
    }

    /**
     * 캘린더를 반환한다.
     * @return	Calendar
     */
    public Calendar getCalendar() {
        return mCalendar;
    }

    /**
     * 현재 설정되어 있는 년도를 반환한다.
     * @return int
     */
    public int getYear() {
        return mCalendar.get(Calendar.YEAR);
    }

    /**
     * 현재 설정되어 있는 달 을 반환한다.
     * @return int
     */
    public int getMonth() {
        return mCalendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 현재 설정되어 있는 일 을 반환한다.
     * @return int
     */
    public int getDay() {
        return mCalendar.get(Calendar.DATE);
    }

    /**
     * 현재 설정되어 있는 시 을 반환한다.
     * @return int
     */
    public int getHour() {
        return mCalendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 현재 설정되어 있는 분 을 반환한다.
     * @return int
     */
    public int getMinute() {
        return mCalendar.get(Calendar.MINUTE);
    }

    /**
     * 현재 설정되어 있는 초 을 반환한다.
     * @return int
     */
    public int getSecond() {
        return mCalendar.get(Calendar.SECOND);
    }



    /**
     * 현재 설정되어 있는 달을 영문 형식으로 반환 한다.
     * @return String
     */
    public String getMonthString() {
        switch (getMonth()) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
        }
        return null;
    }

    /**
     * 다음 년도로 설정한다.
     */
    public void setNextYear() {
        setYear(1);
    }

    /**
     * 이전 년도로 설정한다.
     */
    public void setPrevYear() {
        setYear(-1);
    }

    /**
     * 다음 달로 설정한다.
     */
    public void setNextMonth() {
        setMonth(1);
    }

    /**
     * 이전 달로 설정한다.
     */
    public void setPrevMonth() {
        setMonth(-1);
    }

    public void setToday() {
        mCalendar = Calendar.getInstance();
    }

    /**
     * 원하는 달로 설정한다.
     * @param gap 현재 달과의 차이
     */
    public void setYear(int gap) {
        mCalendar.add(Calendar.YEAR, gap);
    }

    /**
     * 원하는 달로 설정한다.
     * @param gap 현재 달과의 차이
     */
    public void setMonth(int gap) {
        mCalendar.add(Calendar.MONTH, gap);
    }

    /**
     * 원하는 날자로 설정한다.
     * @param gap 일자
     */
    public void setDay(int gap) {
        mCalendar.add(Calendar.DATE, gap);
    }

    /**
     * 이번 년도인지 여부
     * @return boolean
     */
    public boolean isThisYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR) == getYear() ? true : false;
    }

    /**
     * 이번 달인지 여부
     * @return boolean
     */
    public boolean isThisMonth() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.MONTH) == getMonth() - 1 ? true : false;
    }

    /**
     * 오늘 인지 여부
     * @return boolean
     */
    public boolean isToday() {
        Calendar cal  = Calendar.getInstance();
        int thisYear  = cal.get(Calendar.YEAR);
        int thisMonth = cal.get(Calendar.MONTH) + 1;
        int toDay     = cal.get(Calendar.DATE);

        if (thisYear == getYear() && thisMonth == getMonth() && toDay == getDay()) {
            return true;
        }
        return false;
    }

    /**
     * 달의 첫번째 날자를 반환한다.
     * @return int
     */
    public int getFirstDay() {
        int dayOfWeek 	  = mCalendar.get(Calendar.DAY_OF_WEEK);
        int startCount 	  = dayOfWeek - (2 * (dayOfWeek - 1));
        int beforeLastDay = getLastDayOfMonth(-1);
        return beforeLastDay + startCount;
    }

    /**
     * 달의 마지막 날자를 반환한다.
     * @return int
     */
    public int getLastDay() {
        return mCalendar.getActualMaximum(Calendar.DATE);
    }

    /**
     * 달의 시간 구간
     * @return long[] 시작, 끝
     */
    public long[] getMonthMillisecond() {
        Calendar calendar = (Calendar) mCalendar.clone();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        String startDate = new SimpleDateFormat("yyyyMMdd").format(new Date(calendar.getTimeInMillis())) + "000000";
        calendar.set(Calendar.DATE, getLastDay());
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        String endDate   = new SimpleDateFormat("yyyyMMdd").format(new Date(calendar.getTimeInMillis())) + "000000";

        long[] timeInterval = new long[2];
        try {
            timeInterval[0] = new SimpleDateFormat("yyyyMMddHHmmss").parse(startDate).getTime();
            timeInterval[1] = new SimpleDateFormat("yyyyMMddHHmmss").parse(endDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInterval;
    }

    /**
     * 설정 한 달의 일자 리스트를 반환한다.
     * @return int[month][day]	month 0:전달 1:현재달 2:다음달
     */
    public int[][] getDayList() {
        int firstDay = getFirstDay();
        int lastDay  = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int startGap = mCalendar.get(Calendar.DAY_OF_WEEK) - 1;

        int[] beforeMonth = new int[startGap];
        for (int i=0; i<beforeMonth.length; i++) {
            beforeMonth[i] = firstDay + i;
        }

        int[] thisMonth = new int[lastDay];
        for (int i=0; i<thisMonth.length; i++) {
            thisMonth[i] = i + 1;
        }

        int size = (beforeMonth.length + thisMonth.length) % 7;
        int[] nextMonth = new int[size > 0 ? (7 - size) : 0];
        for (int i=0; i<nextMonth.length; i++) {
            nextMonth[i] = i + 1;
        }
        int[][] dayList = {beforeMonth, thisMonth, nextMonth};
        return dayList;
    }

    /**
     * 현재 날자를 음력으로 반환한다.
     * @param day	XXXXXXXX 형태의 현재날자
     * @param type	ex)M월d일
     * @return String
     */
    public String getLunarCalendar(String day, String type) {
        LunarCalendar lunarCalendar = new LunarCalendar();
        return lunarCalendar.solToLun(day, type);
    }

    /**
     * 주어진 날자의 전후 달의 날자를 반환한다.
     * @param year			년도
     * @param month			달
     * @param monthGap		일자
     * @return String
     */
    public int[] getDate(int year, int month, int monthGap) {
        month += monthGap;
        if (month < 1) {
            year --;
            month = 12;
        } else if (month > 12) {
            year ++;
            month = 1;
        }
        int[] date = new int[2];
        date[0] = year;
        date[1] = month;
        return date;
    }

    public HashMap<String, ArrayList<String>> getEvent() {
        Calendar calendar = (Calendar) mCalendar.clone();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        String startDate = new SimpleDateFormat("yyyyMMdd").format(new Date(calendar.getTimeInMillis())) + "000000";
        calendar.add(Calendar.MONTH, 1);
        String endDate   = new SimpleDateFormat("yyyyMMdd").format(new Date(calendar.getTimeInMillis())) + "000000";
        calendar.add(Calendar.MONTH, -1);

        HashMap<String, ArrayList<String>> eventOfMonth = new HashMap<String, ArrayList<String>>();
        ArrayList<String> calendarIdList = getCalendarIdList();
        String[] projection = new String[] { "calendar_id", "htmlUri", "title", "eventLocation", "description", "eventStatus", "selfAttendeeStatus", "commentsUri", "dtstart", "dtend", "eventTimezone", "duration", "allDay", "visibility", "transparency", "hasAlarm", "hasExtendedProperties", "rrule", "rdate", "exrule", "exdate", "originalEvent", "originalInstanceTime", "originalAllDay", "lastDate", "hasAttendeeData", "guestsCanModify", "guestsCanInviteOthers", "guestsCanSeeGuests", "organizer", "deleted" };

        for (String id : calendarIdList) {
            Uri.Builder builder = Uri.parse(CONTENT_URI + "/instances/when").buildUpon();
            try {
                ContentUris.appendId(builder, new SimpleDateFormat("yyyyMMddHHmmss").parse(startDate).getTime());
                ContentUris.appendId(builder, new SimpleDateFormat("yyyyMMddHHmmss").parse(endDate).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Cursor managedCursor = mContext.getContentResolver().query(builder.build(), projection, "Calendars._id=" + id, null, "startDay ASC, startMinute ASC");
            if (managedCursor.moveToFirst()) {
                for (int i = 0; i < managedCursor.getCount(); i++) {
                    String title  = managedCursor.getString(managedCursor.getColumnIndex("title"));
                    Date ddtstart = new Date(managedCursor.getLong(managedCursor.getColumnIndex("dtstart")));
//					Date ddtend   = new Date(managedCursor.getLong(managedCursor.getColumnIndex("dtend")));
//					Log.e("Calendar", "=================================================================================================================");
//					Log.e("Calendar", "calendar_id : " + managedCursor.getInt(managedCursor.getColumnIndex("calendar_id")));
//					Log.e("Calendar", "htmlUri : " + managedCursor.getString(managedCursor.getColumnIndex("htmlUri")));
//					Log.e("Calendar", "title : " + managedCursor.getString(managedCursor.getColumnIndex("title")));
//					Log.e("Calendar", "eventLocation : " + managedCursor.getString(managedCursor.getColumnIndex("eventLocation")));
//					Log.e("Calendar", "description : " + managedCursor.getString(managedCursor.getColumnIndex("description")));
//					Log.e("Calendar", "eventStatus : " + managedCursor.getString(managedCursor.getColumnIndex("eventStatus")));
//					Log.e("Calendar", "selfAttendeeStatus : " + managedCursor.getString(managedCursor.getColumnIndex("selfAttendeeStatus")));
//					Log.e("Calendar", "commentsUri : " + managedCursor.getString(managedCursor.getColumnIndex("commentsUri")));
//					Log.e("Calendar", "dtstart : " + managedCursor.getString(managedCursor.getColumnIndex("dtstart")));
//					Log.e("Calendar", "dtend : " + managedCursor.getString(managedCursor.getColumnIndex("dtend")));
//					Log.e("Calendar", "eventTimezone : " + managedCursor.getString(managedCursor.getColumnIndex("eventTimezone")));
//					Log.e("Calendar", "duration : " + managedCursor.getString(managedCursor.getColumnIndex("duration")));
//					Log.e("Calendar", "allDay : " + managedCursor.getString(managedCursor.getColumnIndex("allDay")));
//					Log.e("Calendar", "visibility : " + managedCursor.getString(managedCursor.getColumnIndex("visibility")));
//					Log.e("Calendar", "transparency : " + managedCursor.getString(managedCursor.getColumnIndex("transparency")));
//					Log.e("Calendar", "hasAlarm : " + managedCursor.getString(managedCursor.getColumnIndex("hasAlarm")));
//					Log.e("Calendar", "hasExtendedProperties : " + managedCursor.getString(managedCursor.getColumnIndex("hasExtendedProperties")));
//					Log.e("Calendar", "rrule : " + managedCursor.getString(managedCursor.getColumnIndex("rrule")));
//					Log.e("Calendar", "rdate : " + managedCursor.getString(managedCursor.getColumnIndex("rdate")));
//					Log.e("Calendar", "exrule : " + managedCursor.getString(managedCursor.getColumnIndex("exrule")));
//					Log.e("Calendar", "exdate : " + managedCursor.getString(managedCursor.getColumnIndex("exdate")));
//					Log.e("Calendar", "originalEvent : " + managedCursor.getString(managedCursor.getColumnIndex("originalEvent")));
//					Log.e("Calendar", "originalInstanceTime : " + managedCursor.getString(managedCursor.getColumnIndex("originalInstanceTime")));
//					Log.e("Calendar", "originalAllDay : " + managedCursor.getString(managedCursor.getColumnIndex("originalAllDay")));
//					Log.e("Calendar", "lastDate : " + managedCursor.getString(managedCursor.getColumnIndex("lastDate")));
//					Log.e("Calendar", "hasAttendeeData : " + managedCursor.getString(managedCursor.getColumnIndex("hasAttendeeData")));
//					Log.e("Calendar", "guestsCanModify : " + managedCursor.getString(managedCursor.getColumnIndex("guestsCanModify")));
//					Log.e("Calendar", "guestsCanInviteOthers : " + managedCursor.getString(managedCursor.getColumnIndex("guestsCanInviteOthers")));
//					Log.e("Calendar", "guestsCanSeeGuests : " + managedCursor.getString(managedCursor.getColumnIndex("guestsCanSeeGuests")));
//					Log.e("Calendar", "organizer : " + managedCursor.getString(managedCursor.getColumnIndex("organizer")));
//					Log.e("Calendar", "deleted : " + managedCursor.getString(managedCursor.getColumnIndex("deleted")));
                    if (title != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat("d");
                        String eventDay = sdf.format(ddtstart);

                        ArrayList<String> eventOfDay = null;
                        if (eventOfMonth.containsKey(eventDay)) {
                            eventOfDay = eventOfMonth.get(eventDay);
                        } else {
                            eventOfDay = new ArrayList<String>();
                            eventOfMonth.put(eventDay, eventOfDay);
                        }
                        eventOfDay.add(title);
                    }
                    managedCursor.moveToNext();
                }
                managedCursor.close();
            }
        }
        return eventOfMonth;
    }

    /**
     * 캘린더 아이디를 반환한다.
     * @return ArrayList<String>
     */
    public ArrayList<String> getCalendarIdList() {
        final Cursor cursor = mContext.getContentResolver().query(Uri.parse(CONTENT_URI + "/calendars"), new String[] {"_id"}, null, null, null);
        ArrayList<String> calendarIdList = new ArrayList<String>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                calendarIdList.add(cursor.getString(0));
            }
        }
        return calendarIdList;
    }

    public int getLastDayOfMonth(int monthGap) {
        Calendar calendar = (Calendar) mCalendar.clone();
        calendar.add(Calendar.MONTH, monthGap);
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.add(Calendar.MONTH, -monthGap);
        return lastDay;
    }

    //http://kanais.tistory.com/257
    // http://zeph1e.tistory.com/34
    // http://tinyurl.com/yfbg76w
}
