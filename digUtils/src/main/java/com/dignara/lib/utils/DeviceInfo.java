package com.dignara.lib.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

public class DeviceInfo {
    public static final String VERSION_NAME = "VERSION_NAME";
    public static final String VERSION_CODE = "VERSION_CODE";

    private static Map<String, Object> infoMap;

    public static void setDeviceInfo(Context context) {
        infoMap = new Hashtable<>();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            infoMap.put(VERSION_NAME, packageInfo.versionName);
            infoMap.put(VERSION_CODE, packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setInfo(String key, Object value) {
        infoMap.put(key, value);
    }

    public static Object getInfo(String key) {
        return infoMap.get(key);
    }

    public static String getDeviceId(Context context) {
        String serial = "";
        try {
            serial = (String) Build.class.getField("SERIAL").get(null);
        } catch (IllegalAccessException e) {
        } catch (NoSuchFieldException e) {
        }
        String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        UUID deviceUuid = new UUID(androidId.hashCode(), serial.hashCode());
        return deviceUuid.toString();
    }
}
