package com.dignara.lib.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtils {
    private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String ALGORITHM      = "AES";

    public static String encrypt(String input, String key) {
        try{
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            return new String(Base64.encodeToString(cipher.doFinal(input.getBytes()), Base64.DEFAULT));
        }catch(Exception e){
            return null;
        }
    }

    public static String decrypt(String input, String key) {
        try{
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            return new String(cipher.doFinal(Base64.decode(input, Base64.DEFAULT)));
        }catch(Exception e){
            return null;
        }
    }
}
