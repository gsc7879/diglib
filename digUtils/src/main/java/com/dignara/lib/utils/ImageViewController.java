package com.dignara.lib.utils;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.widget.ImageView;

public class ImageViewController {
	private static final int MODE_NONE   = 0;
	private static final int MODE_DRAG   = 1;
	private static final int MODE_ZOOM   = 2;
	private static final int MODE_ROTATE = 3;
	
	private Matrix 		 mSavedMatrix;
	private Matrix 		 mMatrix;

	private PointF 		 mStartPoint;
	private PointF 		 mMidPoint;

	private float  		 mOldDist;
	private int 		 mMode;
	
	public ImageViewController() {
		mSavedMatrix = new Matrix();
		mMatrix 	 = new Matrix();
		mStartPoint  = new PointF();
		mMidPoint    = new PointF();
	}
	
	public void resetMatrix() {
		mSavedMatrix.reset();
		mMatrix.reset();
	}
	
	public boolean onTouch(ImageView view, MotionEvent event) {
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			mSavedMatrix.set(view.getImageMatrix());
			mStartPoint.set(event.getX(), event.getY());
			mMode = MODE_DRAG;
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			mOldDist = spacing(event);
			if (mOldDist > 10f) {
				mSavedMatrix.set(mMatrix);
				midPoint(mMidPoint, event);
				mMode = MODE_ZOOM;
			}
			break;
		case MotionEvent.ACTION_UP:
			mMode = MODE_NONE;
			break;
		case MotionEvent.ACTION_POINTER_UP:
			mSavedMatrix.set(mMatrix);
			mStartPoint.set(event.getX(), event.getY());
			mMode = MODE_ROTATE;
			break;
		case MotionEvent.ACTION_MOVE:
			if (mMode == MODE_DRAG) {
				mMatrix.set(mSavedMatrix);
				mMatrix.postTranslate(event.getX() - mStartPoint.x, event.getY() - mStartPoint.y);
			} else if (mMode == MODE_ZOOM) {
				float newDist = spacing(event);
				if (newDist > 10f) {
					mMatrix.set(mSavedMatrix);
					float scale = newDist / mOldDist;
					mMatrix.postScale(scale, scale, mMidPoint.x, mMidPoint.y);
				}
			} else if (mMode == MODE_ROTATE) {
				mMatrix.set(mSavedMatrix);
				float dist = mStartPoint.y - event.getY();
				mMatrix.postRotate(dist, view.getWidth() / 2, view.getHeight() / 2);
			}
			view.setImageMatrix(mMatrix);
			break;
		}
		return true;
	}

	/** Determine the space between the first two fingers */
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}

	/** Calculate the mid point of the first two fingers */
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
}
