package com.dignara.lib.utils;

import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import org.apache.commons.lang3.StringUtils;

public class GAManager {
    private static GAManager uniqueGAManager;

    private Tracker mTracker;

    public synchronized static GAManager getInstance() {
        if (uniqueGAManager == null) {
            uniqueGAManager = new GAManager();
        }
        return uniqueGAManager;
    }

    synchronized public void initTracker(Context context, String trackingId) {
        if (mTracker == null) {
            mTracker = GoogleAnalytics.getInstance(context).newTracker(trackingId);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(false);
            mTracker.enableExceptionReporting(true);
        }
    }

    public void sendScreen(String screenName) {
        if (mTracker == null) {
            return;
        }
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void sendEvent(String category) {
        sendEvent(category, null);
    }

    public void sendEvent(String category, String action) {
        sendEvent(category, action, null);
    }

    public void sendEvent(String category, String action, String label) {
        if (mTracker == null) {
            return;
        }
        HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder().setCategory(category);
        if (StringUtils.isNotBlank(action)) {
            eventBuilder.setAction(action);
        }
        if (StringUtils.isNotBlank(label)) {
            eventBuilder.setLabel(label);
        }
        mTracker.send(eventBuilder.build());
    }
}
