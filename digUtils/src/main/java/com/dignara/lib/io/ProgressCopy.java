package com.dignara.lib.io;

import org.apache.commons.io.IOUtils;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ProgressCopy extends FilterInputStream {
    private PropertyChangeSupport mPropertyChangeSupport;
    private long                  mMaxNumBytes;
    private OnProgress            mProgress;
    private volatile long         mTotalNumBytesRead;
    private OutputStream          mOutputStream;

    public interface OnProgress {
         void onProgress(long transSize, long totalSize);
    }

    public ProgressCopy(InputStream inputStream, OutputStream outputStream, long maxNumBytes, OnProgress progress) {
        super(inputStream);
        mPropertyChangeSupport = new PropertyChangeSupport(this);
        mMaxNumBytes           = maxNumBytes;
        mOutputStream          = outputStream;
        mProgress              = progress;
    }

    public void startCopy() throws IOException {
        IOUtils.copy(this, mOutputStream);
        IOUtils.closeQuietly(this);
        IOUtils.closeQuietly(mOutputStream);
    }

    public long getMaxNumBytes() {
        return mMaxNumBytes;
    }

    public long getTotalNumBytesRead() {
        return mTotalNumBytesRead;
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        mPropertyChangeSupport.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        mPropertyChangeSupport.removePropertyChangeListener(l);
    }

    @Override
    public int read() throws IOException {
        int b = super.read();
        updateProgress(1);
        return b;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return (int)updateProgress(super.read(b));
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return (int)updateProgress(super.read(b, off, len));
    }

    @Override
    public long skip(long n) throws IOException {
        return updateProgress(super.skip(n));
    }

    @Override
    public void mark(int readLimit) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    public boolean cancel() {
        try {
            close();
            IOUtils.closeQuietly(this);
            IOUtils.closeQuietly(mOutputStream);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private long updateProgress(long numBytesRead) {
        if (numBytesRead > 0) {
            long oldTotalNumBytesRead = mTotalNumBytesRead;
            mTotalNumBytesRead += numBytesRead;
            mPropertyChangeSupport.firePropertyChange("t", oldTotalNumBytesRead, mTotalNumBytesRead);
            mProgress.onProgress(mTotalNumBytesRead, mMaxNumBytes);
        }
        return numBytesRead;
    }
}
