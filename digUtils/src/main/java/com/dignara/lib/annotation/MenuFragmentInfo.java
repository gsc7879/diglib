package com.dignara.lib.annotation;

import android.support.annotation.LayoutRes;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MenuFragmentInfo {
    @LayoutRes int layout();

    String[] permissions() default {};

    int enterAnimation() default 0;

    int exitAnimation() default 0;

    boolean isSelectAble() default false;
}
