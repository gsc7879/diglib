package com.dignara.lib.annotation;

import android.support.annotation.LayoutRes;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PageFragmentInfo {
    @LayoutRes int layout();

    String[] permissions() default {};

    boolean isAddBackStack() default true;

    int forwardEnterAnimation() default 0;

    int forwardExitAnimation() default 0;

    int backwardEnterAnimation() default 0;

    int backwardExitAnimation() default 0;

    String screenName() default "";
}
