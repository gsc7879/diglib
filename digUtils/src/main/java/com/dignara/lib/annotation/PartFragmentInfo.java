package com.dignara.lib.annotation;

import android.support.annotation.LayoutRes;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface PartFragmentInfo {
    @LayoutRes int layout();

    String[] permissions() default {};

    boolean isUseBackPressEvent() default true;

    int attachAnimation() default 0;

    int detachAnimation() default 0;

    String screenName() default "";
}
