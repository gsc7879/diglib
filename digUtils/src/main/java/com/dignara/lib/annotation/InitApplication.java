package com.dignara.lib.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface InitApplication {
    String pagePackage();

    String partPackage() default "";

    String menuPackage() default "";

    String dataPackage() default "";

    String trackingId() default "";
}
