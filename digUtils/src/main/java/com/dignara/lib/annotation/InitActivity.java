package com.dignara.lib.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface InitActivity {
    int pageLayoutId();

    int menuLayoutId() default 0;
}
