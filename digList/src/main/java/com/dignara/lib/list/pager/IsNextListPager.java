package com.dignara.lib.list.pager;

public class IsNextListPager extends NextListPager {
    private boolean mIsNext;

    public IsNextListPager(int limit) {
        super(limit);
    }

    @Override
    public void initListCount() {
        super.initListCount();
        mIsNext = false;
    }

    public void setListCount(boolean isNext, int offset) {
        mIsNext = isNext;
        mOffset = offset;
    }

    @Override
    public boolean isNext() {
        return mIsNext;
    }
}
