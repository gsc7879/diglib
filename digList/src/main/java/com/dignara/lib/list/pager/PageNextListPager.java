package com.dignara.lib.list.pager;

public class PageNextListPager extends NextListPager {
    private int mTotal;

    public PageNextListPager(int limit) {
        super(limit);
    }

    @Override
    public void initListCount() {
        super.initListCount();
        mOffset = -1;
    }

    public void setPage(int page, int total) {
        mOffset = page;
        mTotal  = total;
    }

    public int getTotal() {
        return mTotal;
    }

    public int getNextPage() {
        return isNext() ? (mOffset + 1) : mOffset;
    }

    @Override
    public boolean isNext() {
        return mOffset < 0 || mTotal > mOffset * mLimit + mLimit;
    }
}
