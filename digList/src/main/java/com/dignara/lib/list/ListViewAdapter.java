package com.dignara.lib.list;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dignara.lib.list.layout.MeasureLinearLayoutManager;
import com.dignara.lib.list.pager.NextListPager;
import com.dignara.lib.utils.AnnotationUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class ListViewAdapter<T extends ListViewData, S extends ListViewHolder, U extends ListSetData, V> extends RecyclerView.Adapter<S> implements SwipeRefreshLayout.OnRefreshListener {
    private int                    mAdapterLayoutResId;
    private int                    mLastPosition;
    private boolean		           mIsLoading;
    private NextListPager          mNextListPager;
    private SwipeRefreshLayout     mSwipeRefreshLayout;

    protected U                    mListSetData;
    protected List<T>              mItemList;
    protected LayoutInflater       mLayoutInflater;
    protected ViewGroup		       mListParent;
    protected ViewGroup            mNoDataLayout;
    protected RecyclerView         mListView;
    protected Context 		       mContext;
    protected OnListScrollListener mOnListScrollListener;

    public ListViewAdapter(View parentView, int parentLayoutId, int listViewResId, int adapterLayoutResId, int noDataLayoutResId) {
        this(parentView, parentLayoutId, listViewResId, adapterLayoutResId, noDataLayoutResId, (U) new DefaultListSetData<V>());
    }

    public ListViewAdapter(View parentView, int parentLayoutId, int listViewResId, int adapterLayoutResId, int noDataLayoutResId, U listSetData) {
        mContext		    = parentView.getContext();
        mAdapterLayoutResId = adapterLayoutResId;
        mLayoutInflater	    = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListParent	 	    = (ViewGroup) parentView.findViewById(parentLayoutId);
        mListView	  	    = (RecyclerView) parentView.findViewById(listViewResId);
        mNoDataLayout       = (ViewGroup) mLayoutInflater.inflate(noDataLayoutResId, mListParent, false);
        mItemList           = new ArrayList<>();
        mListSetData        = listSetData;

        AnnotationUtils.setInjectView(mNoDataLayout, this);
        AnnotationUtils.setInjectView(parentView, this);
        AnnotationUtils.setClickEvent(mNoDataLayout, this);
        AnnotationUtils.setClickEvent(parentView, this);
        mListParent.addView(mNoDataLayout, 0);
        createListView();
        onSetListView(mListView);
    }

    protected void setClickView(Activity activity) {
        AnnotationUtils.setClickEvent(activity, this);
    }

    public ViewGroup getListParent() {
        return mListParent;
    }

    public interface OnListScrollListener {
        void onScrollDown();
        void onScrollUp();
    }

    public U getListData() {
        return mListSetData;
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }

    public void setOnListScrollListener(OnListScrollListener loadListener) {
        mOnListScrollListener = loadListener;
    }

    protected void setNextListController(NextListPager nextListPager) {
        mNextListPager = nextListPager;
    }

    public void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout, int[] colorResId) {
        mSwipeRefreshLayout = swipeRefreshLayout;
        mSwipeRefreshLayout.setColorSchemeResources(colorResId);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void listScrollCheck(int currentPosition) {
        if(currentPosition > mLastPosition) {
            mOnListScrollListener.onScrollDown();
        }
        if(currentPosition < mLastPosition) {
            mOnListScrollListener.onScrollUp();
        }
        mLastPosition = currentPosition;
    }

    private void startLoadList() {
        if (mIsLoading) {
            return;
        }
        setLoading(true);
        if (mListSetData.isFirst()) {
            cleanup();
            if (mNextListPager != null) {
                mNextListPager.initListCount();
            }
        }
        onStartLoad(mListSetData.isFirst());
        mListSetData.setFirst(false);
    }

    public void endLoadList(int errorCode) {
        endLoadList(null, errorCode);
    }

    public void endLoadList(V data) {
        endLoadList(data, -1);
    }

    public void endLoadList(V data, int errorCode) {
        if (!mIsLoading) {
            return;
        }
        mListSetData.setData(data);
        if (mListSetData.isSuccess()) {
            onEndLoad(data);
        }
        onAddedItems(getItemCount(), errorCode);
        setLoading(false);
        notifyDataSetChanged();
    }

    private void setLoading(boolean isLoading) {
        mIsLoading = isLoading;
        setNoListView(isLoading || (!isLoading && getItemCount() == 0));
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(isLoading);
        }
    }

    public RecyclerView getListView() {
        return mListView;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }

    public void createListView() {
        final MeasureLinearLayoutManager layoutManager = new MeasureLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mListView.setLayoutManager(layoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mListView.setAdapter(this);
        mListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (mOnListScrollListener != null) {
                    listScrollCheck(dy);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!mIsLoading && mNextListPager != null && mNextListPager.isNext()) {
                    int totalItemCount  = layoutManager.getItemCount();
                    int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (totalItemCount > 1 && lastVisibleItem >= totalItemCount - 1) {
                        startLoadList();
                    }
                }
            }
        });
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public S onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(mAdapterLayoutResId, parent, false);
        return createListViewHolder(itemView, viewType);
    }

    @Override
    public void onBindViewHolder(S holder, int position) {
        T listViewData = mItemList.get(position);
        listViewData.getViewController().setView(this, holder);
        onSetData(holder, listViewData);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    protected void cleanup() {
        onCleanup();
        for (T listViewData : mItemList) {
            listViewData.cleanup();
        }
        mItemList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        loadList();
    }

    private void setNoListView(boolean isNoList) {
        mNoDataLayout.setVisibility(isNoList ? View.VISIBLE : View.GONE);
    }

    public T getItem(int index) {
        return mItemList.get(index);
    }

    protected void addItem(T listViewData) {
        addItem(listViewData, -1);
    }

    protected void addItem(T listViewData, int position) {
        if (position > -1) {
            mItemList.add(position, listViewData);
        } else {
            mItemList.add(listViewData);
        }
        notifyItemInserted(position);
        listViewData.getViewController().setInitViewData(this);
        setNoListView(getItemCount() == 0);
    }

    public void removeItem(T listViewData) {
        int index = mItemList.indexOf(listViewData);
        if (index > -1) {
            removeItem(mItemList.indexOf(listViewData));
        }
    }

    public void removeItem(int position) {
        mItemList.remove(position);
        notifyItemRemoved(position);
        setNoListView(getItemCount() == 0);
    }

    public void loadList() {
        initListData();
        startLoadList();
    }

    private void initListData() {
        mListSetData.setInit();
    }

    protected void onSetListView(RecyclerView listView) {}

    protected abstract S createListViewHolder(View parent, int viewType);

    protected abstract void onSetData(S listViewHolder, T listViewData);

    protected abstract void onCleanup();

    protected abstract void onStartLoad(boolean isFirst);

    protected abstract void onEndLoad(V dataList);

    protected void onAddedItems(int itemCount, int errorCode) {}

    protected void onScroll(RecyclerView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {}
}
