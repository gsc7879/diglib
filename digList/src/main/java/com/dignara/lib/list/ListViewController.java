package com.dignara.lib.list;

public abstract class ListViewController<T extends ListViewData, S extends ListViewHolder> {
	protected T mViewData;
	
	public ListViewController(T listViewData) {
		mViewData = listViewData;
	}
	
	public void cleanup() {
		onCleanup();
	}
	
	public abstract void setInitViewData(ListViewAdapter listViewAdapter);
	
	public abstract void setView(ListViewAdapter listAdapter, S listViewHolder);

	protected abstract void onCleanup();
}
