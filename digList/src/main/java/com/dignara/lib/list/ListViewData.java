package com.dignara.lib.list;

public abstract class ListViewData {
	private ListViewController mListViewController;
	
	public ListViewController getViewController() {
        if (mListViewController == null) {
            mListViewController = createViewController();
        }
		return mListViewController;
	}
	
	public void cleanup() {
		onCleanup();
        if (mListViewController != null) {
		    mListViewController.cleanup();
		    mListViewController = null;
        }
	}
	
	protected abstract ListViewController createViewController();

	protected abstract void onCleanup();
}
