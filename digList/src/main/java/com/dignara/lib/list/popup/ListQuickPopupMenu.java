package com.dignara.lib.list.popup;

import android.content.Context;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewData;
import com.dignara.lib.popup.QuickPopupMenu;

public class ListQuickPopupMenu extends QuickPopupMenu {
    private ListViewAdapter         mListViewAdapter;

    public interface OnSelectedItemListener {
        public void onSelectedItem(int index, ListViewData listViewData);
    }

    public ListQuickPopupMenu(Context context, int rootLayoutResId) {
        super(context, rootLayoutResId);
    }

    public void setListAdapter(ListViewAdapter listViewAdapter) {
        mListViewAdapter = listViewAdapter;
    }
}