package com.dignara.lib.list;

import android.view.View;

import static android.support.v7.widget.RecyclerView.ViewHolder;

public abstract class ListViewHolder extends ViewHolder {
	public ListViewHolder(View itemView) {
		super(itemView);
		setListViewHolder(itemView);
	}

	protected abstract void setListViewHolder(View itemView);
}
