package com.dignara.lib.list;

public abstract class ListSetData<T> {
    private boolean isFirst;
    private T       data;

    public boolean isSuccess() {
        return data != null;
    }

    T getData() {
        return data;
    }

    boolean isFirst() {
        return isFirst;
    }

    void setFirst(boolean first) {
        isFirst = first;
    }

    void setData(T data) {
        this.data = data;
    }

    void setInit() {
        this.isFirst = true;
        this.data    = null;
        onInitData();
    }

    protected abstract void onInitData();
}
