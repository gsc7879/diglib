package com.dignara.lib.list.pager;

public abstract class NextListPager {
    protected int mLimit;
    protected int mOffset;

    public NextListPager(int limit) {
        mLimit = limit;
    }

    public void initListCount() {
        mOffset = 0;
    }

    public int getLimit() {
        return mLimit;
    }

    public int getOffset() {
        return mOffset;
    }

    public abstract boolean isNext();
}
