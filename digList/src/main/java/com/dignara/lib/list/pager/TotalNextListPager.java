package com.dignara.lib.list.pager;

public class TotalNextListPager extends NextListPager {
    private int mTotal;

    public TotalNextListPager(int limit) {
        super(limit);
    }

    @Override
    public void initListCount() {
        super.initListCount();
        mTotal = 0;
    }

    public void addListCount(int total, int addCount) {
        mTotal   = total;
        mOffset += addCount;
    }

    public int getTotal() {
        return mTotal;
    }

    public int getPage() {
        return mTotal > 0 ? mTotal / mOffset : 0;
    }

    @Override
    public boolean isNext() {
        return mTotal > mOffset;
    }
}
