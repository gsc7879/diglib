package com.dignara.lib.dialog;

import android.app.DialogFragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.dignara.lib.utils.AnnotationUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class DialogController extends DialogFragment {
    private OnDismissListener       mDismissListener;
    private int                     mLayoutResId;

    private int                     mShowAnimationResId;
    private int 		            mDismissAnimationResId;
    private boolean                 mIsPositive;
    private boolean                 mIsNotCanceled;
    private boolean                 mIsCancelable;

    protected int                   mDialogId;

    protected Map<Integer, Object>  mDataMap;
    protected OnSendDataListener    mSendDataListener;

    public interface OnSendDataListener {
        void onSendData(int dialogId, Object... data);
    }

    public interface OnDismissListener {
        void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object>  dataMap);
    }

    public void setOnSendDataListener(OnSendDataListener sendDataListener) {
        mSendDataListener = sendDataListener;
    }

    public void setOnDismissListener(OnDismissListener dismissListener) {
        mDismissListener = dismissListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutResId           = getArguments().getInt("layoutResId");
        mShowAnimationResId    = getArguments().getInt("showAnimationResId");
        mDismissAnimationResId = getArguments().getInt("dismissAnimationResId");
        mDialogId              = getArguments().getInt("dialogId");
        mIsCancelable          = getArguments().getBoolean("isCancelable", true);
        mDataMap               = new HashMap<>();
        setCancelable(mIsCancelable);

        try {
            int styleResId = getArguments().getInt("styleId");
            setStyle(DialogFragment.STYLE_NO_FRAME, styleResId > 0 ? styleResId : getActivity().getPackageManager().getActivityInfo(getActivity().getComponentName(), 0).theme);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View mainLayout = getView();
        if (mainLayout != null) {
            mainLayout.setClickable(true);
        }
        getDialog().setCanceledOnTouchOutside(mIsCancelable);
        AnnotationUtils.setInjectView(mainLayout, this);
        AnnotationUtils.setClickEvent(mainLayout, this);
        onInitDialog(mainLayout);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container,  Bundle savedInstanceState) {
        View dialogLayout = layoutInflater.inflate(mLayoutResId, container, false);
        if (mShowAnimationResId != 0) {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), mShowAnimationResId);
            dialogLayout.startAnimation(animation);
        }
        return dialogLayout;
    }

    @Override
    public void dismiss() {
        if (mDismissAnimationResId != 0) {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), mDismissAnimationResId);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    onCloseBefore();
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    closeDialog();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            getView().startAnimation(animation);
        }
    }

    private void closeDialog() {
        onCloseAfter();
        super.dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mDismissListener != null) {
            mDismissListener.onDismiss(mDialogId, mIsPositive, !mIsNotCanceled, mDataMap);
        }
    }

    public void negativeDismiss() {
        mIsNotCanceled = true;
        mIsPositive    = false;
        dismiss();
    }

    public void positiveDismiss() {
        mIsNotCanceled = true;
        mIsPositive    = true;
        dismiss();
    }

    protected void onCloseAfter() {};
    protected void onCloseBefore() {};

    protected abstract void onInitDialog(View view);
}
