package com.dignara.lib.dialog;

import android.os.Bundle;


public abstract class DialogMaker {
	public abstract DialogController getDialogFragment(int dialogId, Object... etcData);

    protected Bundle getBundle(int layoutResId, int showAnimationResId, int dismissAnimationResId, boolean isCancelable, int dialogId) {
        Bundle bundle = new Bundle();
        bundle.putInt("layoutResId"          , layoutResId);
        bundle.putInt("showAnimationResId"   , showAnimationResId);
        bundle.putInt("dismissAnimationResId", dismissAnimationResId);
        bundle.putInt("dialogId"             , dialogId);
        bundle.putBoolean("isCancelable"     , isCancelable);
        return bundle;
    }
}
