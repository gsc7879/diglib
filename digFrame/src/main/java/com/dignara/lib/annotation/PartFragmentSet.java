package com.dignara.lib.annotation;

public class PartFragmentSet extends FragmentSet {
    private int     attachAnimation;
    private int     detachAnimation;
    private boolean isUseBackPressEvent;

    public PartFragmentSet(int layoutId, String packageName, String screenName, String[] permissions, boolean isUseBackPressEvent, int attachAnimation, int detachAnimation) {
        super(layoutId, packageName, screenName, permissions, false);
        this.attachAnimation     = attachAnimation;
        this.detachAnimation     = detachAnimation;
        this.isUseBackPressEvent = isUseBackPressEvent;
    }

    public int getAttachAnimation() {
        return attachAnimation;
    }

    public int getDetachAnimation() {
        return detachAnimation;
    }

    public boolean isUseBackPressEvent() {
        return isUseBackPressEvent;
    }
}
