package com.dignara.lib.annotation;

public class FragmentSet {
    private int      layoutId;
    private boolean  isAddBackStack;
    private String   packageName;
    private String   screenName;
    private String[] permissions;

    public FragmentSet(int layoutId, String packageName, String screenName, String[] permissions, boolean isAddBackStack) {
        this.layoutId       = layoutId;
        this.packageName    = packageName;
        this.isAddBackStack = isAddBackStack;
        this.screenName     = screenName;
        this.permissions    = permissions;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getPackageName() {
        return packageName;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public boolean isAddBackStack() {
        return isAddBackStack;
    }
}
