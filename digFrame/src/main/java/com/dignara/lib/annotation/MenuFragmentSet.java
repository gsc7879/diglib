package com.dignara.lib.annotation;

public class MenuFragmentSet extends FragmentSet {
    private boolean isSelectAble;
    private int     enterAnimation;
    private int     exitAnimation;

    public MenuFragmentSet(int layoutId, String packageName, String[] permissions, boolean isSelectAble, int enterAnimation, int exitAnimation) {
        super(layoutId, packageName, null, permissions, false);
        this.isSelectAble   = isSelectAble;
        this.enterAnimation = enterAnimation;
        this.exitAnimation  = exitAnimation;
    }

    public boolean isSelectAble() {
        return isSelectAble;
    }

    public int getEnterAnimation() {
        return enterAnimation;
    }

    public int getExitAnimation() {
        return exitAnimation;
    }
}
