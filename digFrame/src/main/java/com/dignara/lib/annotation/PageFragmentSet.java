package com.dignara.lib.annotation;

public class PageFragmentSet extends FragmentSet {
    private int     forwardEnterAnimation;
    private int     forwardExitAnimation;
    private int     backwardEnterAnimation;
    private int     backwardExitAnimation;

    public PageFragmentSet(int layoutId, String packageName, String screenName, String[] permissions, boolean isAddBackStack, int forwardEnterAnimation, int forwardExitAnimation, int backwardEnterAnimation, int backwardExitAnimation) {
        super(layoutId, packageName, screenName, permissions, isAddBackStack);
        this.forwardEnterAnimation  = forwardEnterAnimation;
        this.forwardExitAnimation   = forwardExitAnimation;
        this.backwardEnterAnimation = backwardEnterAnimation;
        this.backwardExitAnimation  = backwardExitAnimation;
    }

    public int getForwardEnterAnimation() {
        return forwardEnterAnimation;
    }

    public int getForwardExitAnimation() {
        return forwardExitAnimation;
    }

    public int getBackwardEnterAnimation() {
        return backwardEnterAnimation;
    }

    public int getBackwardExitAnimation() {
        return backwardExitAnimation;
    }
}
