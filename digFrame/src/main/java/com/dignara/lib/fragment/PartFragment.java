package com.dignara.lib.fragment;

import android.os.Bundle;
import android.view.View;
import com.dignara.lib.annotation.FragmentSet;
import com.dignara.lib.annotation.PartFragmentSet;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.KeyboardUtils;
import org.apache.commons.lang3.StringUtils;

public abstract class PartFragment extends MainFragment {
    private static final String STATE_ATTACH_ANIMATION      = "attachAnimation";
    private static final String STATE_DETACH_ANIMATION      = "detachAnimation";
    private static final String STATE_ATTACH_FRAGMENT_NAME  = "attachFragmentName";
    private static final String STATE_USE_BACK_PRESS_EVENT  = "isUseBackPressEvent";

    private OnAttachedListener mAttachedListener;
    private boolean            mIsUseBackPressEvent;
    private int                mAttachAnimation;
    private int                mDetachAnimation;

    protected Class<? extends MainFragment> mParentFragmentClass;

    public interface OnAttachedListener {
        void attached();
    }

    public void initFragment(FragmentSet fragmentSet) {
        super.initFragment(fragmentSet);
        initPartFragment((PartFragmentSet) fragmentSet);
    }

    void initPartFragment(PartFragmentSet partFragmentInfo) {
        setAnimation(partFragmentInfo.getAttachAnimation(), partFragmentInfo.getDetachAnimation());
        mIsUseBackPressEvent = partFragmentInfo.isUseBackPressEvent();
    }

    void setAnimation(int attachAnimation, int detachAnimation) {
        mAttachAnimation = attachAnimation;
        mDetachAnimation = detachAnimation;
    }

    boolean isUseBackPressEvent() {
        return mIsUseBackPressEvent;
    }

    int getAttachAnimation() {
        return mAttachAnimation;
    }

    int getDetachAnimation() {
        return mDetachAnimation;
    }

    Class<? extends MainFragment> getParentFragmentClass() {
        return mParentFragmentClass;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_ATTACH_ANIMATION        , mAttachAnimation);
        outState.putInt(STATE_DETACH_ANIMATION        , mDetachAnimation);
        outState.putString(STATE_ATTACH_FRAGMENT_NAME , mParentFragmentClass == null ? "" : mParentFragmentClass.getName());
        outState.putBoolean(STATE_USE_BACK_PRESS_EVENT, mIsUseBackPressEvent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mAttachAnimation     = savedInstanceState.getInt(STATE_ATTACH_ANIMATION);
            mDetachAnimation     = savedInstanceState.getInt(STATE_DETACH_ANIMATION);
            mIsUseBackPressEvent = savedInstanceState.getBoolean(STATE_USE_BACK_PRESS_EVENT);

            try {
                mParentFragmentClass = (Class<? extends MainFragment>) Class.forName(savedInstanceState.getString(STATE_ATTACH_FRAGMENT_NAME));
            } catch (Exception e) {
                mParentFragmentClass = null;
            }
        }
    }

    @Override
    protected void startInitView() {
        super.startInitView();
        if (mAttachedListener != null) {
            mAttachedListener.attached();
        }
    }

    public void detachFragment(Object... data) {
        detachIgnoreListenerFragment(true, data);
    }

    public void detachIgnoreListenerFragment(boolean isCallListener, Object... data) {
        AppManager.getInstance().getPageManager().detachFragment(this, mParentFragmentClass, isCallListener, data);
    }

    void attachFragment(final MainFragment parentFragment, boolean isUseBackPressEvent, final Object... data) {
        KeyboardUtils.hideSoftInput(getActivity());
        mParentFragmentClass = parentFragment == null ? null : parentFragment.getClass();
        mIsUseBackPressEvent = isUseBackPressEvent;
        mAttachedListener    = new OnAttachedListener() {
            @Override
            public void attached() {
                onAttached(mParentFragmentClass, data);
                AppManager.getInstance().getPageManager().setActiveFragment(getFragmentManager(), PartFragment.this.getClass());
                if (parentFragment != null) {
                    parentFragment.attachedFragment(PartFragment.this.getClass(), data);
                }
                if (StringUtils.isNotBlank(mScreenName)) {
                    GAManager.getInstance().sendScreen(mScreenName);
                }
            }
        };
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            detachFragment();
            return false;
        }
        return true;
    }

    protected void onAttached(Class<? extends MainFragment> parentFragmentClass, Object... data) {}
}