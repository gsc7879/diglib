package com.dignara.lib.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.Log;
import com.dignara.lib.annotation.FragmentSet;

public class FragmentMaker {
    public MainFragment getAddedFragment(FragmentManager fragmentManager, Class<? extends MainFragment> fragmentClass) {
        if (fragmentClass != null) {
            Fragment fragment = fragmentManager.findFragmentByTag(fragmentClass.getName());
            if (fragment != null) {
                return (MainFragment) fragment;
            }
        }
        return null;
    }

    public MainFragment getFragment(FragmentManager fragmentManager, int layoutId, Class<? extends MainFragment> fragmentClass, FragmentSet fragmentSet) {
        MainFragment mainFragment = getAddedFragment(fragmentManager, fragmentClass);
        if (mainFragment == null) {
            mainFragment = createFragment(fragmentSet);
            addFragment(fragmentManager, layoutId, mainFragment, fragmentClass);
        }
        return mainFragment;
    }

    protected void addFragment(FragmentManager fragmentManager, int layoutId, Fragment fragment, Class<? extends MainFragment> fragmentClass) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (layoutId > 0) {
            fragmentTransaction.add(layoutId, fragment, fragmentClass.getName());
        } else {
            fragmentTransaction.add(fragment, fragmentClass.getName());
        }
        if (fragment instanceof PartFragment) {
            fragmentTransaction.setCustomAnimations(((PartFragment) fragment).getAttachAnimation(), 0);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void switchFragment(FragmentManager fragmentManager, MainFragment hideFragment, MainFragment showFragment, int enterAnimation, int exitAnimation, boolean isForward) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation);
        if (hideFragment != null) {
            fragmentTransaction.hide(hideFragment);
            if (isForward && hideFragment.isAddBackStack()) {
                fragmentTransaction.addToBackStack(hideFragment.getTag());
            } else {
                fragmentManager.popBackStack();
            }
        }
        if (showFragment != null) {
            fragmentTransaction.show(showFragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    public boolean existFragment(FragmentManager fragmentManager, Class<? extends MainFragment> fragmentClass) {
        return fragmentManager.findFragmentByTag(fragmentClass.getName()) != null;
    }

    public Class<? extends PageFragment> getPageFragmentClass(FragmentManager fragmentManager, int index) {
        int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        if (backStackEntryCount - index > -1) {
            try {
                return (Class<? extends PageFragment>) Class.forName(fragmentManager.getBackStackEntryAt(backStackEntryCount - index).getName());
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public synchronized MainFragment createFragment(FragmentSet fragmentSet) {
        try {
            MainFragment mainFragment = (MainFragment) Class.forName(fragmentSet.getPackageName()).newInstance();
            mainFragment.initFragment(fragmentSet);
            return mainFragment;
        } catch (InstantiationException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }
}
