package com.dignara.lib.fragment;

import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import com.dignara.lib.annotation.MenuFragmentSet;
import com.dignara.lib.annotation.PartFragmentSet;

import java.util.Map;

public class MenuManager {
    private static final String STATE_CURRENT_MENU_NAME = "currentMenuClass";

    private Map<Class<? extends MenuFragment>, MenuFragmentSet> mMenuFragmentInfoMap;
    private Class<? extends MenuFragment>                       mCurrentMenuClass;

    private int           mRootLayoutId;
    private FragmentMaker mFragmentMaker;

    public MenuManager(int rootLayoutId, Bundle savedInstanceState, Map<Class<? extends MenuFragment>, MenuFragmentSet> menuFragmentInfoMap, Map<Class<? extends PartFragment>, PartFragmentSet> partFragmentInfoMap) {
        mRootLayoutId        = rootLayoutId;
        mFragmentMaker       = new FragmentMaker();
        mMenuFragmentInfoMap = menuFragmentInfoMap;
        loadSavedInstanceState(savedInstanceState);
    }

    void loadSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            try {
                String currentMenuClassName = savedInstanceState.getString(STATE_CURRENT_MENU_NAME);
                mCurrentMenuClass = (Class<? extends MenuFragment>) Class.forName(currentMenuClassName);
            } catch (Exception e) {
                mCurrentMenuClass = null;
            }
        }
    }

    public Class<? extends MenuFragment> getCurrentMenuClass() {
        return mCurrentMenuClass;
    }

    void saveInstanceState(Bundle outState) {
        outState.putString(STATE_CURRENT_MENU_NAME, mCurrentMenuClass == null ? "" : mCurrentMenuClass.getName());
    }

    public void switchMenu(FragmentManager fragmentManager, Class<? extends MenuFragment> menuClass) {
        MainFragment prevFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        MainFragment nextFragment = mFragmentMaker.getFragment(fragmentManager, mRootLayoutId, menuClass, mMenuFragmentInfoMap.get(menuClass));
        if (nextFragment == null) {
            return;
        }
        mFragmentMaker.switchFragment(fragmentManager, prevFragment, nextFragment, prevFragment == null ? 0 : ((MenuFragment) prevFragment).getExitAnimation(), nextFragment == null ? 0 : ((MenuFragment) nextFragment).getEnterAnimation(), false);
        mCurrentMenuClass = menuClass;
    }

    public void selectMenu(FragmentManager fragmentManager, int index) {
        MainFragment mainFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        if (mainFragment != null && mainFragment instanceof MenuFragment) {
            ((MenuFragment) mainFragment).selectMenu(index);
        }
    }
	
	public void toggleMenu(FragmentManager fragmentManager) {
        MainFragment mainFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        if (mainFragment != null && mainFragment instanceof MenuFragment) {
            ((MenuFragment) mainFragment).toggleMenu();
        }
	}
	
	public void hideMenu(FragmentManager fragmentManager) {
        MainFragment mainFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        if (mainFragment != null && mainFragment instanceof MenuFragment) {
            ((MenuFragment) mainFragment).onHideMenu();
        }
	}
	
	public void showMenu(FragmentManager fragmentManager) {
        MainFragment mainFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        if (mainFragment != null && mainFragment instanceof MenuFragment) {
            ((MenuFragment) mainFragment).onShowMenu();
        }
	}

    public boolean sendBackPressEvent(FragmentManager fragmentManager) {
        MainFragment mainFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentMenuClass);
        if (mainFragment != null) {
            return mainFragment.onBackPressed();
        }
        return true;
    }
}
