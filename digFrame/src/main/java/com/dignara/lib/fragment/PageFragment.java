package com.dignara.lib.fragment;

import android.os.Bundle;
import com.dignara.lib.annotation.FragmentSet;
import com.dignara.lib.annotation.PageFragmentSet;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.KeyboardUtils;
import org.apache.commons.lang3.StringUtils;

public abstract class PageFragment extends MainFragment {
    private static final String STATE_FORWARD_ENTER_ANIMATION  = "forwardEnterAnimation";
    private static final String STATE_FORWARD_EXIT_ANIMATION   = "forwardExitAnimation";
    private static final String STATE_BACKWARD_ENTER_ANIMATION = "backwardEnterAnimation";
    private static final String STATE_BACKWARD_EXIT_ANIMATION  = "backwardExitAnimation";
    private static final String STATE_ACTIVE_FRAGMENT_NAME     = "activeFragmentClass";

    private Class<? extends PartFragment> mActiveFragmentClass;

    private OnSwitchedListener mSwitchedListener;

    private int   mForwardEnterAnimation;
    private int   mForwardExitAnimation;
    private int   mBackwardEnterAnimation;
    private int   mBackwardExitAnimation;

    public interface OnSwitchedListener {
        void switched();
    }

    public void initFragment(FragmentSet fragmentSet) {
        super.initFragment(fragmentSet);
        initPageFragment((PageFragmentSet) fragmentSet);
    }

    public void initPageFragment(PageFragmentSet pageFragmentInfo) {
       mForwardEnterAnimation  = pageFragmentInfo.getForwardEnterAnimation();
       mForwardExitAnimation   = pageFragmentInfo.getForwardExitAnimation();
       mBackwardEnterAnimation = pageFragmentInfo.getBackwardEnterAnimation();
       mBackwardExitAnimation  = pageFragmentInfo.getBackwardExitAnimation();
    }

    int getForwardEnterAnimation() {
        return mForwardEnterAnimation;
    }

    int getForwardExitAnimation() {
        return mForwardExitAnimation;
    }

    int getBackwardEnterAnimation() {
        return mBackwardEnterAnimation;
    }

    int getBackwardExitAnimation() {
        return mBackwardExitAnimation;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_FORWARD_ENTER_ANIMATION , mForwardEnterAnimation);
        outState.putInt(STATE_FORWARD_EXIT_ANIMATION  , mForwardExitAnimation);
        outState.putInt(STATE_BACKWARD_ENTER_ANIMATION, mBackwardEnterAnimation);
        outState.putInt(STATE_BACKWARD_EXIT_ANIMATION , mBackwardExitAnimation);
        outState.putString(STATE_ACTIVE_FRAGMENT_NAME , mActiveFragmentClass == null ? "" : mActiveFragmentClass.getName());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mForwardEnterAnimation  = savedInstanceState.getInt(STATE_FORWARD_ENTER_ANIMATION);
            mForwardExitAnimation   = savedInstanceState.getInt(STATE_FORWARD_EXIT_ANIMATION);
            mBackwardEnterAnimation = savedInstanceState.getInt(STATE_BACKWARD_ENTER_ANIMATION);
            mBackwardExitAnimation  = savedInstanceState.getInt(STATE_BACKWARD_EXIT_ANIMATION);

            try {
                mActiveFragmentClass = (Class<? extends PartFragment>) Class.forName(savedInstanceState.getString(STATE_ACTIVE_FRAGMENT_NAME));
            } catch (Exception e) {
                mActiveFragmentClass = null;
            }
        }
    }

    @Override
    protected void startInitView() {
        super.startInitView();
        if (mSwitchedListener != null) {
            mSwitchedListener.switched();
        }
    }

    public void sendData(int dataId, Object... data) {
        sendData(mActiveFragmentClass, dataId, data);
    }

    public void switchPage(Class<? extends PageFragment> switchPageClass, Object... etcData) {
        AppManager.getInstance().getPageManager().switchPage(getFragmentManager(), switchPageClass, etcData);
    }

    void switchedPage(final Class<? extends PageFragment> prevPageClass, final Object... etcData) {
        KeyboardUtils.hideSoftInput(getActivity());
        mSwitchedListener = new PageFragment.OnSwitchedListener() {
            @Override
            public void switched() {
                onSwitchedPage(prevPageClass, etcData);
                if (StringUtils.isNotBlank(mScreenName)) {
                    GAManager.getInstance().sendScreen(mScreenName);
                }
            }
        };
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            switchPrevPage();
            return false;
        }
        return true;
    }

    protected Class<? extends PartFragment> getActiveFragmentClass() {
        return mActiveFragmentClass;
    }

    void setActiveFragmentClass(Class<? extends PartFragment> fragmentClass) {
        mActiveFragmentClass = fragmentClass;
    }

    protected void onSwitchedPage(Class<? extends PageFragment> prevPageClass, Object... etcData) {}

    protected void onLeavePage(Class<? extends PageFragment> nextPageClass) {}

    protected void switchPrevPage(Object... data) {
        AppManager.getInstance().getPageManager().switchPrevPage(getFragmentManager(), data);
    }

    protected void detachAllSubPart() {
        AppManager.getInstance().getPageManager().detachAllSubPart(getFragmentManager(), this);
    }


}