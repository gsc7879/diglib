package com.dignara.lib.fragment;

import android.support.multidex.MultiDexApplication;
import com.dignara.lib.utils.DeviceInfo;

public abstract class DignaraApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        DeviceInfo.setDeviceInfo(this);
        AppManager.getInstance().init(this);
    }
}
