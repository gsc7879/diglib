package com.dignara.lib.fragment;

import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.dignara.lib.annotation.*;
import com.dignara.lib.bank.ImageBank;
import com.dignara.lib.utils.GAManager;
import dalvik.system.DexFile;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

public class AppManager {
    private static AppManager uniqueAppManager;

    private List<DataBank>  mDataBankList;
    private FragmentManager mFragmentManager;
    private PageManager     mPageManager;
    private MenuManager     mMenuManager;

    private Map<Class<? extends MenuFragment>, MenuFragmentSet> mMenuFragmentInfoMap;
    private Map<Class<? extends PageFragment>, PageFragmentSet> mPageFragmentInfoMap;
    private Map<Class<? extends PartFragment>, PartFragmentSet> mPartFragmentInfoMap;

    public synchronized static AppManager getInstance() {
        if (uniqueAppManager == null) {
            uniqueAppManager = new AppManager();
        }
        return uniqueAppManager;
    }

    private AppManager() {
        mDataBankList        = new ArrayList<>();
        mMenuFragmentInfoMap = new Hashtable<>();
        mPageFragmentInfoMap = new Hashtable<>();
        mPartFragmentInfoMap = new Hashtable<>();
    }

    public void saveInstanceState(Bundle instanceState) {
        if (mPageManager != null) {
            mPageManager.saveInstanceState(instanceState);
        }

        if (mMenuManager != null) {
            mMenuManager.saveInstanceState(instanceState);
        }

        if (mDataBankList != null) {
            for (DataBank dataBank : mDataBankList) {
                dataBank.onSaveInstanceState(instanceState);
            }
        }
    }

    void loadInstanceState(Bundle instanceState) {
        if (mDataBankList != null) {
            for (DataBank dataBank : mDataBankList) {
                dataBank.onLoadInstanceState(instanceState);
            }
        }
    }

    void init(Application application) {
        initDataBank(application.getApplicationContext());
        ImageBank.getInstance().init(application.getApplicationContext());
        initApplication(application);
    }

    private void initDataBank(Context context) {
        if (mDataBankList == null) {
            return;
        }

        for (DataBank dataBank : mDataBankList) {
            dataBank.initData(context);
        }
    }

    private void initApplication(Application application) {
        for (Annotation annotation : application.getClass().getAnnotations()) {
            InitApplication initApplication = (InitApplication) annotation;
            String menuPackage = initApplication.menuPackage();
            String pagePackage = initApplication.pagePackage();
            String partPackage = initApplication.partPackage();
            String dataPackage = initApplication.dataPackage();
            String trackingId  = initApplication.trackingId();

            setMenuFragmentMap(application, menuPackage);
            setPageFragmentMap(application, pagePackage);
            setPartFragmentMap(application, partPackage);
            setDataBank(application, dataPackage);

            if (StringUtils.isNotBlank(trackingId)) {
                GAManager.getInstance().initTracker(application.getApplicationContext(), trackingId);
            }
        }
    }

    public void initActivity(Activity activity, Bundle savedInstanceState, Class<? extends MenuFragment> initMenuClass, Class<? extends PageFragment> initPageClass) {
        for (Annotation annotation : activity.getClass().getAnnotations()) {
            InitActivity initActivity = (InitActivity) annotation;
            int menuLayoutId = initActivity.menuLayoutId();
            int pageLayoutId = initActivity.pageLayoutId();

            if (initMenuClass != null) {
                initMenu(activity.getFragmentManager(), savedInstanceState, menuLayoutId, initMenuClass);
            }

            if (initPageClass != null) {
                initPage(activity.getFragmentManager(), savedInstanceState, pageLayoutId, initPageClass);
            }
        }
        if (savedInstanceState != null) {
            loadInstanceState(savedInstanceState);
        }
    }

    private void initMenu(FragmentManager fragmentManager, Bundle savedInstanceState, int menuLayoutId, Class<? extends MenuFragment> initMenuClass) {
        mFragmentManager = fragmentManager;
        mMenuManager     = new MenuManager(menuLayoutId, savedInstanceState, mMenuFragmentInfoMap, mPartFragmentInfoMap);
        if (savedInstanceState == null && mMenuManager.getCurrentMenuClass() == null) {
            mMenuManager.switchMenu(fragmentManager, initMenuClass);
        }
    }

    private void initPage(FragmentManager fragmentManager, Bundle savedInstanceState, int pageLayoutId, Class<? extends PageFragment> initPageClass) {
        mFragmentManager = fragmentManager;
        mPageManager     = new PageManager(pageLayoutId, savedInstanceState, mPageFragmentInfoMap, mPartFragmentInfoMap);
        if (savedInstanceState == null && mPageManager.getCurrentPageClass() == null) {
            mPageManager.switchPage(fragmentManager, initPageClass);
        }
    }

    private void setMenuFragmentMap(Context context, String rootPackage) {
        for (Class clazz : getPathClasses(context, rootPackage)) {
            if (clazz.isAnnotationPresent(MenuFragmentInfo.class)) {
                Annotation[] annotations = clazz.getAnnotations();
                for (Annotation annotation : annotations) {
                    MenuFragmentInfo menuInfo = (MenuFragmentInfo) annotation;
                    int layoutId              = menuInfo.layout();
                    int enterAnimation        = menuInfo.enterAnimation();
                    int exitAnimation         = menuInfo.exitAnimation();
                    boolean isSelectAble      = menuInfo.isSelectAble();
                    String[] permissions      = menuInfo.permissions();
                    mMenuFragmentInfoMap.put(clazz, new MenuFragmentSet(layoutId, clazz.getName(), permissions, isSelectAble, enterAnimation, exitAnimation));
                }
            }
        }
    }

    private void setDataBank(Context context, String rootPackage) {
        for (Class clazz : getPathClasses(context, rootPackage)) {
            if (clazz.isAnnotationPresent(com.dignara.lib.annotation.DataBank.class)) {
                try {
                    Class c = Class.forName(clazz.getName());
                    Method factoryMethod = c.getDeclaredMethod("getInstance");
                    DataBank dataBank = (DataBank) factoryMethod.invoke(null, null);
                    mDataBankList.add(dataBank);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setPageFragmentMap(Context context, String rootPackage) {
        for (Class clazz : getPathClasses(context, rootPackage)) {
            if (clazz.isAnnotationPresent(PageFragmentInfo.class)) {
                Annotation[] annotations = clazz.getAnnotations();
                for (Annotation annotation : annotations) {
                    PageFragmentInfo pageInfo  = (PageFragmentInfo) annotation;
                    String screenName          = pageInfo.screenName();
                    int layoutId               = pageInfo.layout();
                    int forwardEnterAnimation  = pageInfo.forwardEnterAnimation();
                    int forwardExitAnimation   = pageInfo.forwardExitAnimation();
                    int backwardEnterAnimation = pageInfo.backwardEnterAnimation();
                    int backwardExitAnimation  = pageInfo.backwardExitAnimation();
                    boolean isAddBackStack     = pageInfo.isAddBackStack();
                    String[] permissions       = pageInfo.permissions();
                    mPageFragmentInfoMap.put(clazz, new PageFragmentSet(layoutId, clazz.getName(), screenName, permissions, isAddBackStack, forwardEnterAnimation, forwardExitAnimation, backwardEnterAnimation, backwardExitAnimation));
                }
            }
        }
    }

    private void setPartFragmentMap(Context context, String rootPackage) {
        for (Class clazz : getPathClasses(context, rootPackage)) {
            if (clazz.isAnnotationPresent(PartFragmentInfo.class)) {
                Annotation[] annotations = clazz.getAnnotations();
                for (Annotation annotation : annotations) {
                    PartFragmentInfo partInfo   = (PartFragmentInfo) annotation;
                    String screenName           = partInfo.screenName();
                    int layoutId                = partInfo.layout();
                    int attachAnimation         = partInfo.attachAnimation();
                    int detachAnimation         = partInfo.detachAnimation();
                    boolean isUseBackPressEvent = partInfo.isUseBackPressEvent();
                    String[] permissions        = partInfo.permissions();
                    mPartFragmentInfoMap.put(clazz, new PartFragmentSet(layoutId, clazz.getName(), screenName, permissions, isUseBackPressEvent, attachAnimation, detachAnimation));
                }
            }
        }
    }

    public Set<Class<?>> getPathClasses(Context context, String packageName) {
        Set<Class<?>> classes = new HashSet<>();
        DexFile dex = null;
        try {
            dex = new DexFile(context.getApplicationInfo().sourceDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<String> entries = dex.entries();
        while (entries.hasMoreElements()) {
            String entry = entries.nextElement();
            if (entry.toLowerCase().startsWith(packageName.toLowerCase()))
                try {
                    classes.add(classLoader.loadClass(entry));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
        }
        return classes;
    }

    public FragmentManager getFragmentManager() { return mFragmentManager; }

    public MenuManager getMenuManager() {
        return mMenuManager;
    }

    public PageManager getPageManager() {
        return mPageManager;
    }

    public void detachAllSubPart() {
        mPageManager.detachAllSubPart(mFragmentManager);
    }

    public boolean sendBackPressEvent() {
        return mPageManager.sendBackPressEvent(mFragmentManager);
    }

    public void sendActivityResult(int requestCode, int resultCode, Intent data) {
        mPageManager.sendActivityResult(mFragmentManager, requestCode, resultCode, data);
    }

    public void requestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mPageManager.requestPermissionsResult(mFragmentManager, requestCode, permissions, grantResults);
    }

    public void attachFragment(Class<? extends PartFragment> attachingFragmentClass, Object... data) {
        mPageManager.attachFragment(mFragmentManager, attachingFragmentClass, data);
    }

    public void sendData(Class<? extends MainFragment>[] receiveFragmentClasses, int dataType, Object... data) {
        mPageManager.sendData(mFragmentManager, null, receiveFragmentClasses, dataType, data);
    }

    public void sendData(Class<? extends MainFragment> receiveFragmentClass, int dataType, Object... data) {
        mPageManager.sendData(mFragmentManager, null, receiveFragmentClass, dataType, data);
    }

    public void sendData(int dataType, Object... data) {
        mPageManager.sendData(mFragmentManager, null, dataType, data);
    }
}