package com.dignara.lib.fragment;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dignara.lib.annotation.FragmentSet;
import com.dignara.lib.utils.AnnotationUtils;
import rx.subscriptions.CompositeSubscription;

import java.util.ArrayList;
import java.util.List;

public abstract class MainFragment extends Fragment {
    private static final String STATE_LAYOUT_ID     = "layoutId";
    private static final String NAME_FRAGMENT_CHILD = "childFragmentClassName";
    private static final String NAME_SCREEN         = "screenName";
    private static final String PERMISSIONS         = "permissions";

    protected int                           mLayoutId;
    protected OnReadyViewListener           mReadyViewListener;
    protected OnCreatedListener             mCreatedListener;
    protected OnDetachListener              mDetachListener;
    protected CompositeSubscription         mSubscriptions;
    protected Class<? extends PartFragment> mChildFragmentClass;
    protected String                        mScreenName;

    private String[]                        mPermissions;
    private boolean                         mAddBackStack;
    private Bundle                          mSavedInstanceState;

    public void initFragment(FragmentSet fragmentSet) {
        mScreenName   = fragmentSet.getScreenName();
        mAddBackStack = fragmentSet.isAddBackStack();
        mLayoutId     = fragmentSet.getLayoutId();
        mPermissions  = fragmentSet.getPermissions();
    }

    public CompositeSubscription getSubscriptions() {
        return mSubscriptions;
    }

    boolean isAddBackStack() {
        return mAddBackStack;
    }

    void setOnCreatedListener(OnCreatedListener createdListener) {
        mCreatedListener = createdListener;
    }

    void setOnDetachListener(OnDetachListener detachListener) {
        mDetachListener = detachListener;
    }

    void setReadyFragmentListener(OnReadyViewListener readyViewListener) {
        mReadyViewListener = readyViewListener;
    }

    interface OnCreatedListener {
        void created(Class<? extends MainFragment> fragmentClass);
    }

    interface OnDetachListener {
        void onDetach();
    }

    interface OnReadyViewListener {
        void onReady();
    }

    Class<? extends PartFragment> getChildFragmentClass() {
        return mChildFragmentClass;
    }

    void receiveData(Class<? extends MainFragment> sendFragmentClass, int dataType, Object... data) {
        onDataReceived(sendFragmentClass, dataType, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_LAYOUT_ID, mLayoutId);
        outState.putString(NAME_FRAGMENT_CHILD, mChildFragmentClass == null ? "" : mChildFragmentClass.getName());
        outState.putString(NAME_SCREEN, mScreenName);
        outState.putStringArray(PERMISSIONS, mPermissions);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(layoutInflater, container, savedInstanceState);
        return layoutInflater.inflate(mLayoutId, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;
        mSubscriptions      = new CompositeSubscription();

        if (savedInstanceState != null) {
            mLayoutId    = savedInstanceState.getInt(STATE_LAYOUT_ID);
            mScreenName  = savedInstanceState.getString(NAME_SCREEN);
            mPermissions = savedInstanceState.getStringArray(PERMISSIONS);

            try {
                mChildFragmentClass = (Class<? extends PartFragment>) Class.forName(savedInstanceState.getString(NAME_FRAGMENT_CHILD));
            } catch (Exception e) {
                mChildFragmentClass = null;
            }
        }
        if (mCreatedListener != null) {
            mCreatedListener.created(getClass());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mDetachListener != null) {
            mDetachListener.onDetach();
        }
    }

    @Override
    public void onDestroyView() {
        if (mSubscriptions != null && mSubscriptions.hasSubscriptions()) {
            mSubscriptions.unsubscribe();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View mainLayout = getView();
        if (mainLayout != null) {
            mainLayout.setClickable(true);
        }

        AnnotationUtils.setInjectView(getActivity(), this);
        AnnotationUtils.setClickEvent(getActivity(), this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            startInitView();
        } else {
            checkPermissions();
        }
    }

    public void attachFragment(int attachLayoutResId, Class<? extends PartFragment> attachFragmentClass, Object... data) {
        AppManager.getInstance().getPageManager().attachFragment(getFragmentManager(), attachLayoutResId, attachFragmentClass, this, data);
    }

    public boolean detachChildFragment() {
        if (mChildFragmentClass != null) {
            AppManager.getInstance().getPageManager().detachFragment(mChildFragmentClass, this, true);
            return true;
        }
        return false;
    }

    public void sendData(Class<? extends MainFragment> receiveFragmentClass, int dataType, Object... data) {
        AppManager.getInstance().getPageManager().sendData(getFragmentManager(), getClass(), receiveFragmentClass, dataType, data);
    }

    protected Class<? extends PageFragment> getPrevPageClass() {
        return AppManager.getInstance().getPageManager().getPrevPageClass(getFragmentManager());
    }

    void attachedFragment(Class<? extends PartFragment> attachedFragmentClass, Object... data) {
        mChildFragmentClass = attachedFragmentClass;
        onAttachedFragment(attachedFragmentClass, data);
    }

    void detachedFragment(Class<? extends PartFragment> detachedFragmentClass, Class<? extends MainFragment> parentFragmentClass, boolean isCallListener, Object... data) {
        mChildFragmentClass = null;
        AppManager.getInstance().getPageManager().setActiveFragment(getFragmentManager(), PartFragment.class.isAssignableFrom(parentFragmentClass) ? (Class<? extends PartFragment>) parentFragmentClass : null);
        if (isCallListener) {
            onDetachedFragment(detachedFragmentClass, data);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    void checkPermissions() {
        List<String> deniedPermissions = new ArrayList<>();
        for (String permission : mPermissions) {
            if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_DENIED) {
                deniedPermissions.add(permission);
            }
        }
        if (!deniedPermissions.isEmpty()) {
            requestPermissions(deniedPermissions.toArray(new String[deniedPermissions.size()]), 0);
        } else {
            startInitView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        String permission;
        for (int i = 0; i < permissions.length; i++) {
            permission = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                deniedPermissions.add(permission);
            }
        }

        if (onDeniedPermissions(deniedPermissions.toArray(new String[deniedPermissions.size()]))) {
            startInitView();
        }
    }

    protected void startInitView() {
        onInitView(getView());

        if (mSavedInstanceState != null) {
            onLoadInstanceState(mSavedInstanceState);
        }

        if (mReadyViewListener != null) {
            mReadyViewListener.onReady();
        }
    }

    protected abstract void onInitView(View view);

    protected abstract boolean onDeniedPermissions(String[] permissions);

    protected void onDetachedFragment(Class<? extends PartFragment> detachedFragmentClass, Object... data) {}

    protected void onAttachedFragment(Class<? extends PartFragment> attachedFragmentClass, Object... data) {}

    protected void onLoadInstanceState(Bundle savedInstanceState) {}

    protected void onDataReceived(Class<? extends MainFragment> sendFragmentClass, int dataType, Object... data) {}

    protected boolean onBackPressed() {
        return false;
    }
}

