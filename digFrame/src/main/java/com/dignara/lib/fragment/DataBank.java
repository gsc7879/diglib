package com.dignara.lib.fragment;

import android.content.Context;
import android.os.Bundle;

public interface DataBank {
    void initData(Context context);

    void onSaveInstanceState(Bundle instanceState);

    void onLoadInstanceState(Bundle instanceState);
}
