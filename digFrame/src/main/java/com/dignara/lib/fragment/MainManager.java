package com.dignara.lib.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.dignara.lib.annotation.PartFragmentSet;

import java.util.Map;

public abstract class MainManager {
    private static final String STATE_CURRENT_PAGE_NAME = "currentPageTag";
    private static final String STATE_ROOT_LAYOUT_ID   = "rootLayoutId";

    private Map<Class<? extends PartFragment>, PartFragmentSet> mPartFragmentInfoMap;

    int                           mRootLayoutId;
    Class<? extends PageFragment> mCurrentPageClass;
    FragmentMaker                 mFragmentMaker;

    MainManager(int rootLayoutId, Bundle savedInstanceState, Map<Class<? extends PartFragment>, PartFragmentSet> partFragmentInfoMap) {
        mRootLayoutId        = rootLayoutId;
        mFragmentMaker       = new FragmentMaker();
        mPartFragmentInfoMap = partFragmentInfoMap;
        loadSavedInstanceState(savedInstanceState);
    }

    void loadSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mRootLayoutId = savedInstanceState.getInt(STATE_ROOT_LAYOUT_ID);
            try {
                mCurrentPageClass = (Class<? extends PageFragment>) Class.forName(savedInstanceState.getString(STATE_CURRENT_PAGE_NAME));
            } catch (Exception e) {
                mCurrentPageClass = null;
            }
        }
    }

    void saveInstanceState(Bundle outState) {
        outState.putInt(STATE_ROOT_LAYOUT_ID, mRootLayoutId);
        outState.putString(STATE_CURRENT_PAGE_NAME, mCurrentPageClass == null ? "" : mCurrentPageClass.getName());
    }

    void attachFragment(FragmentManager fragmentManager, int attachLayoutResId, Class<? extends PartFragment> attachingFragmentClass, MainFragment parentFragment, Object... data) {
        PartFragment attachingFragment = (PartFragment) mFragmentMaker.getFragment(fragmentManager, attachLayoutResId, attachingFragmentClass, mPartFragmentInfoMap.get(attachingFragmentClass));
        attachingFragment.attachFragment(parentFragment, attachingFragment.isUseBackPressEvent(), data);
    }

    void detachFragment(Class<? extends PartFragment> detachFragmentClass, MainFragment parentFragment, boolean isCallListener, Object... data) {
        FragmentManager fragmentManager = parentFragment.getFragmentManager();
        MainFragment detachFragment = mFragmentMaker.getAddedFragment(fragmentManager, detachFragmentClass);
        if (detachFragment != null && detachFragment instanceof PartFragment) {
            detachFragment(fragmentManager, (PartFragment) detachFragment, parentFragment, isCallListener, data);
        }
    }

    void detachFragment(PartFragment detachFragment, Class<? extends MainFragment> parentFragmentClass, boolean isCallListener, Object... data) {
        FragmentManager fragmentManager = detachFragment.getFragmentManager();
        MainFragment parentFragment = mFragmentMaker.getAddedFragment(fragmentManager, parentFragmentClass);
        if (parentFragment != null) {
            detachFragment(fragmentManager, detachFragment, parentFragment, isCallListener, data);
        }
    }

    private void detachFragment(FragmentManager fragmentManager, final PartFragment detachFragment, final MainFragment parentFragment, final boolean isCallListener, final Object... data) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(detachFragment);
        fragmentTransaction.commitAllowingStateLoss();

        if (parentFragment != null && detachFragment != null) {
            detachFragment.setOnDetachListener(new MainFragment.OnDetachListener() {
                @Override
                public void onDetach() {
                    parentFragment.detachedFragment(detachFragment.getClass(), detachFragment.getParentFragmentClass(), isCallListener, data);
                }
            });
        }
    }

    void setActiveFragment(FragmentManager fragmentManager, Class<? extends PartFragment> activeFragmentClass) {
        MainFragment attachFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        if (attachFragment != null && attachFragment instanceof PageFragment) {
            ((PageFragment) attachFragment).setActiveFragmentClass(activeFragmentClass);
        }
    }
}
