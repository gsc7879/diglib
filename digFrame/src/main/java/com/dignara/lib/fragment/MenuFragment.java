package com.dignara.lib.fragment;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.dignara.lib.annotation.FragmentSet;
import com.dignara.lib.annotation.MenuFragmentSet;

import java.util.HashMap;
import java.util.Map;

public abstract class MenuFragment extends MainFragment implements OnClickListener {
    private static final String STATE_SELECT_ABLE     = "isSelectAble";
    private static final String STATE_ENTER_ANIMATION = "enterAnimation";
    private static final String STATE_EXIT_ANIMATION  = "exitAnimation";

    protected Map<Integer, View> mMenuButton;
    private OnMenuClickListener  mMenuClickListener;
    private boolean              mIsSelectAble;
    private int     			 mSelectedIndex;
    private int                  mEnterAnimation;
    private int                  mExitAnimation;

    public interface OnMenuClickListener {
        boolean onMenuClick(int menuId, int menuIndex, View menuView);
    }

    public void initFragment(FragmentSet fragmentSet) {
        super.initFragment(fragmentSet);
        initMenuFragment((MenuFragmentSet) fragmentSet);
    }

    void initMenuFragment(MenuFragmentSet menuFragmentInfo) {
        mIsSelectAble   = menuFragmentInfo.isSelectAble();
        mEnterAnimation = menuFragmentInfo.getEnterAnimation();
        mExitAnimation  = menuFragmentInfo.getExitAnimation();
    }

    int getExitAnimation() {
        return mExitAnimation;
    }

    int getEnterAnimation() {
        return mEnterAnimation;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_SELECT_ABLE, mIsSelectAble);
        outState.putInt(STATE_ENTER_ANIMATION, mEnterAnimation);
        outState.putInt(STATE_EXIT_ANIMATION, mExitAnimation);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMenuButton = new HashMap<>();
        if (savedInstanceState != null) {
            mIsSelectAble   = savedInstanceState.getBoolean(STATE_SELECT_ABLE);
            mEnterAnimation = savedInstanceState.getInt(STATE_ENTER_ANIMATION);
            mExitAnimation  = savedInstanceState.getInt(STATE_EXIT_ANIMATION);
        }
    }

    public void setOnMenuClickListener(OnMenuClickListener onMenuClickListener) {
        mMenuClickListener = onMenuClickListener;
    }

    void selectMenu(int selectIndex) {
        if (!mMenuButton.containsKey(selectIndex)) {
            return;
        }
        changeButton(selectIndex);
        onChangeMenu(selectIndex);
    }

    protected void addButton(int id, int buttonResId) {
        View buttonView = getView().findViewById(buttonResId);
        buttonView.setTag(id);
        buttonView.setOnClickListener(this);
        mMenuButton.put(id, buttonView);
    }

    private void changeButton(int selectIndex) {
        if (!mIsSelectAble) {
            return;
        }

        for (Integer buttonId : mMenuButton.keySet()) {
            mMenuButton.get(buttonId).setSelected(buttonId == selectIndex);
        }
        mSelectedIndex = selectIndex;
    }

    Map<Integer, View> getButtonMap() {
        return mMenuButton;
    }

    View getButton(int index) {
        return mMenuButton.get(index);
    }

    @Override
    public void onClick(View menuButton) {
        int id = (int) menuButton.getTag();
        if (mMenuClickListener != null && mMenuClickListener.onMenuClick(getId(), id, menuButton)) {
            return;
        }

        if (!onClickMenuItem(id, menuButton)) {
            changeButton(id);
        }
    }

    int getSelectedIndex() {
        return mSelectedIndex;
    }

    void toggleMenu() {
        if (getView().getVisibility() == View.VISIBLE) {
            onHideMenu();
        } else {
            onShowMenu();
        }
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        return permissions.length == 0;
    }

    protected abstract boolean onClickMenuItem(int id, View menuButton);

    protected abstract void onHideMenu();

    protected abstract void onShowMenu();

    protected abstract void onChangeMenu(int menuIndex);
}
