package com.dignara.lib.fragment;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.dignara.lib.annotation.PageFragmentSet;
import com.dignara.lib.annotation.PartFragmentSet;

import java.util.Map;

public class PageManager extends MainManager {
    private Map<Class<? extends PageFragment>, PageFragmentSet> mPageFragmentInfoMap;

    private OnPageChangeListener mPageChangeListener;

    public interface OnPageChangeListener {
        void onPageChanged(Class beforePageClass, Class nextPageClass);
    }

    public PageManager(int pageRootLayoutId, Bundle savedInstanceState, Map<Class<? extends PageFragment>, PageFragmentSet> pageFragmentInfoMap, Map<Class<? extends PartFragment>, PartFragmentSet> partFragmentInfoMap) {
        super(pageRootLayoutId, savedInstanceState, partFragmentInfoMap);
        mPageFragmentInfoMap = pageFragmentInfoMap;
    }

    void setOnPageChangeListener(OnPageChangeListener pageChangeListener) {
        mPageChangeListener = pageChangeListener;
    }

    void attachFragment(FragmentManager fragmentManager, Class<? extends PartFragment> attachingFragmentClass, Object... data) {
        MainFragment currentFragment = mFragmentMaker.getFragment(fragmentManager, mRootLayoutId, mCurrentPageClass, mPageFragmentInfoMap.get(mCurrentPageClass));
        if (currentFragment != null) {
            Class<? extends PartFragment> activeFragmentClass = ((PageFragment) currentFragment).getActiveFragmentClass();
            if (activeFragmentClass != null) {
                currentFragment = mFragmentMaker.getAddedFragment(fragmentManager, activeFragmentClass);
            }
        }
        attachFragment(fragmentManager, mRootLayoutId, attachingFragmentClass, currentFragment, data);
    }

    Class<? extends PageFragment> getCurrentPageClass() {
        return mCurrentPageClass;
    }

    boolean switchPrevPage(FragmentManager fragmentManager, Object... etcData) {
        Class<? extends PageFragment> prevPageFragmentClass = mFragmentMaker.getPageFragmentClass(fragmentManager, 1);
        if (prevPageFragmentClass == null) {
            return false;
        }
        switchPage(fragmentManager, false, prevPageFragmentClass, etcData);
        return true;
    }

    void switchPage(FragmentManager fragmentManager, Class<? extends PageFragment> pageFragmentClass, Object... etcData) {
        switchPage(fragmentManager, true, pageFragmentClass, etcData);
    }

    void switchPage(FragmentManager fragmentManager, boolean isForward, Class<? extends PageFragment> pageFragmentClass, final Object... etcData) {
        PageFragment prevPageFragment = (PageFragment) mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        MainFragment nextFragment;

        if (isForward) {
            nextFragment = mFragmentMaker.getFragment(fragmentManager, mRootLayoutId, pageFragmentClass, mPageFragmentInfoMap.get(pageFragmentClass));
        } else {
            nextFragment = mFragmentMaker.getAddedFragment(fragmentManager, pageFragmentClass);
        }

        if (nextFragment == null) {
            return;
        }

        final PageFragment nextPageFragment = (PageFragment) nextFragment;
        switchFragment(fragmentManager, isForward, prevPageFragment, nextPageFragment);

        if (prevPageFragment != null) {
            prevPageFragment.onLeavePage(pageFragmentClass);
        }

        doSwitchPage(prevPageFragment, nextPageFragment, etcData);
        mCurrentPageClass = pageFragmentClass;
    }

    private void doSwitchPage(PageFragment prevPageFragment, PageFragment nextPageFragment, Object... etcData) {
        if (prevPageFragment != null) {
            Class<? extends PageFragment> prevPageClass = prevPageFragment.getClass();
            if (mPageChangeListener != null) {
                mPageChangeListener.onPageChanged(prevPageClass, nextPageFragment.getClass());
            }
            nextPageFragment.switchedPage(prevPageClass, etcData);
        } else {
            nextPageFragment.switchedPage(null, etcData);
        }
    }

    private void switchFragment(final FragmentManager fragmentManager, boolean isForward, PageFragment prevFragment, PageFragment nextFragment) {
        mFragmentMaker.switchFragment(fragmentManager, prevFragment, nextFragment, prevFragment == null ? 0 : isForward ? prevFragment.getForwardEnterAnimation() : prevFragment.getBackwardEnterAnimation(), nextFragment == null ? 0 : isForward ? nextFragment.getForwardExitAnimation() : nextFragment.getBackwardExitAnimation(), isForward);
    }

    void sendActivityResult(FragmentManager fragmentManager, int requestCode, int resultCode, Intent data) {
        MainFragment pageFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        if (pageFragment != null) {
            Class<? extends PartFragment> childFragmentClass = pageFragment.getChildFragmentClass();
            if (childFragmentClass == null) {
                pageFragment.onActivityResult(requestCode, resultCode, data);
            } else {
                MainFragment partFragment = mFragmentMaker.getAddedFragment(fragmentManager, childFragmentClass);
                if (partFragment != null) {
                    partFragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    void requestPermissionsResult(FragmentManager fragmentManager, int requestCode, String[] permissions, int[] grantResults) {
        MainFragment pageFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        if (pageFragment != null) {
            Class<? extends PartFragment> childFragmentClass = pageFragment.getChildFragmentClass();
            if (childFragmentClass == null) {
                pageFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            } else {
                MainFragment partFragment = mFragmentMaker.getAddedFragment(fragmentManager, childFragmentClass);
                if (partFragment != null) {
                    partFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }

    void detachAllSubPart(FragmentManager fragmentManager) {
        MainFragment pageFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        if (pageFragment != null && pageFragment instanceof PageFragment) {
            detachAllSubPart(fragmentManager, (PageFragment) pageFragment);
        }
    }

    void detachAllSubPart(FragmentManager fragmentManager, PageFragment pageFragment) {
        Class<? extends PartFragment> activeFragmentClass = pageFragment.getActiveFragmentClass();
        MainFragment activeFragment = mFragmentMaker.getAddedFragment(fragmentManager, activeFragmentClass);
        if (activeFragmentClass != null && activeFragment instanceof PartFragment) {
            deleteAllChildFragment(fragmentManager, (PartFragment) activeFragment);
        }
    }

    private void deleteAllChildFragment(FragmentManager fragmentManager, PartFragment partFragment) {
        partFragment.detachIgnoreListenerFragment(false);
        Class<? extends MainFragment> parentFragmentClass = partFragment.getParentFragmentClass();
        MainFragment parentFragment = mFragmentMaker.getAddedFragment(fragmentManager, parentFragmentClass);
        if (parentFragment != null && parentFragment instanceof PartFragment) {
            deleteAllChildFragment(fragmentManager, (PartFragment) parentFragment);
        }
    }

    void sendData(FragmentManager fragmentManager, Class<? extends MainFragment> sendFragmentClass, Class<? extends MainFragment>[] receiveFragmentClasses, int dataType, Object... data) {
        for (Class<? extends MainFragment> targetFragmentClass : receiveFragmentClasses) {
            sendData(fragmentManager, sendFragmentClass, targetFragmentClass, dataType, data);
        }
    }

    void sendData(FragmentManager fragmentManager, Class<? extends MainFragment> sendFragmentClass, Class<? extends MainFragment> receiveFragmentClass, int dataType, Object... data) {
        MainFragment fragment = mFragmentMaker.getAddedFragment(fragmentManager, receiveFragmentClass);
        if (fragment != null) {
            fragment.receiveData(sendFragmentClass, dataType, data);
        }
    }

    void sendData(FragmentManager fragmentManager, Class<? extends MainFragment> sendFragmentClass, int dataType, Object... data) {
        sendData(fragmentManager, sendFragmentClass, mCurrentPageClass, dataType, data);
    }

    Class<? extends PageFragment> getPrevPageClass(FragmentManager fragmentManager) {
        return mFragmentMaker.getPageFragmentClass(fragmentManager, 1);
    }

    boolean sendBackPressEvent(FragmentManager fragmentManager) {
        MenuManager menuManager = AppManager.getInstance().getMenuManager();
        if (menuManager != null && menuManager.sendBackPressEvent(fragmentManager)) {
            return true;
        }

        MainFragment pageFragment = mFragmentMaker.getAddedFragment(fragmentManager, mCurrentPageClass);
        if (pageFragment != null && pageFragment instanceof PageFragment) {
            Class<? extends PartFragment> backPressEventClass = ((PageFragment) pageFragment).getActiveFragmentClass();
            if (backPressEventClass != null) {
                MainFragment fragment = mFragmentMaker.getAddedFragment(fragmentManager, backPressEventClass);
                if (fragment != null && fragment.isAdded() && fragment.onBackPressed()) {
                    return true;
                }
            }
            return pageFragment.onBackPressed() || switchPrevPage(fragmentManager);
        }
        return false;
    }
}
